import logging
from typing import Tuple, List

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
from aprilgrid import AprilGrid
from apriltags_eth import AprilTagDetector, AprilTagDetection
from evo.core.trajectory import PoseTrajectory3D
from scipy.spatial.transform import Rotation
from tqdm import tqdm

from utils.render_3d import render_cube


class AprilTagPose(object):
    tag_cols: int
    tag_rows: int
    tag_size: float
    tag_spacing_ratio: float
    tag_detector: AprilTagDetector
    grid_detector: AprilGrid
    K: np.ndarray
    dist_coeffs: np.ndarray
    min_detected_tags: int
    projection_threshold: float

    def __init__(self,
                 K: np.ndarray,
                 dist_coeffs: np.ndarray,
                 tag_cols: int = 6,
                 tag_rows: int = 6,
                 tag_size: float = 0.14,
                 tag_spacing_ratio: float = 0.021 / 0.14,
                 min_detected_tags: int = 36,
                 projection_threshold: float = 0.9):
        """

        :param K:
        :param dist_coeffs:
        :param tag_cols:
        :param tag_rows:
        :param tag_size:
        :param tag_spacing_ratio:
        """
        self.tag_cols = tag_cols
        self.tag_rows = tag_rows
        self.tag_size = tag_size
        self.tag_spacing_ratio = tag_spacing_ratio
        self.K = K
        self.dist_coeffs = dist_coeffs
        self.min_detected_tags = min_detected_tags
        self.grid_detector = AprilGrid(self.tag_rows, self.tag_cols, self.tag_size, self.tag_spacing_ratio)
        self.tag_detector = self.grid_detector.detector
        self.projection_threshold = projection_threshold
        self.log = logging.getLogger(__name__)

    def detect_tags(self, img: np.ndarray) -> List[AprilTagDetection]:
        """

        :param img:
        :return:
        """
        results = self.tag_detector.extract_tags(img)
        return results

    def detect_grid(self, img: np.ndarray, debug: bool = False) -> Tuple[np.ndarray, np.ndarray]:
        """

        :param debug: For
        :param img: gray scale image
        :return:
        """

        res = self.grid_detector.compute_observation(img)

        image_points = np.array(res.image_points)
        object_points = np.array(res.target_points)
        npoints = len(object_points)
        if npoints <= self.min_detected_tags * 4:
            return None, None
        object_points = np.hstack((object_points, np.zeros((npoints, 1))))

        if debug:
            plt.figure()
            plt.imshow(img, cmap="gray")
            plt.plot(image_points[:, 0], image_points[:, 1], marker=".")
            plt.show()
        return image_points, object_points

    def calculating_pose_errors(self,
                                image_points: np.ndarray,
                                object_points: np.ndarray,
                                rvec: np.ndarray,
                                tvec: np.ndarray):
        """
        Projection error of april grid pose into image
        :param image_points:
        :param object_points:
        :param rvec:
        :param tvec:
        :return:
        """
        projected_grid_points, _ = cv.projectPoints(object_points,
                                                    rvec,
                                                    tvec,
                                                    self.K,
                                                    self.dist_coeffs)
        point_errors = np.linalg.norm(image_points - projected_grid_points.squeeze(), axis=1)
        return point_errors

    def calculate_grid_pose(self,
                            image_points: np.ndarray,
                            object_points: np.ndarray):
        """

        :param image_points:
        :param object_points:
        :param K:
        :param dist_coeffs:
        :return: t, R - camera pose w_T_c. camera -> world
        """
        # TODO undistort points first for support equidistant distortion model
        # undistorted = cv.fisheye.undistortPoints(distorted, K, D)
        # https://stackoverflow.com/a/59063100/3481173
        success, rvec, tvec = cv.solvePnP(object_points, image_points, self.K, self.dist_coeffs,
                                          flags=cv.SOLVEPNP_EPNP)  # SOLVEPNP_SQPNP, SOLVEPNP_IPPE

        # Calculate projection errors
        point_errors_epnp = self.calculating_pose_errors(image_points, object_points, rvec, tvec)
        if np.percentile(point_errors_epnp, 99) > self.projection_threshold:
            self.log.warning("EPNP failed. Using IPPE instead!!")
            # This is a slower and more accurate version of PnP. Only use if EPnP fails
            success2, rvec, tvec = cv.solvePnP(object_points, image_points, self.K, self.dist_coeffs,
                                               flags=cv.SOLVEPNP_IPPE)  # SOLVEPNP_SQPNP, SOLVEPNP_IPPE
            # # Check for outliers and filter them out
            # if check_outliers:
            #     success2, rvec, tvec, inliers = \
            #         cv.solvePnPRansac(object_points, image_points, self.K, self.dist_coeffs,
            #                           rvec=rvec, tvec=tvec, useExtrinsicGuess=True, iterationsCount=10)
            point_errors_ippe = self.calculating_pose_errors(image_points, object_points, rvec, tvec)

            if not success2 or np.percentile(point_errors_ippe, 95) > self.projection_threshold:
                self.log.warning("IPPE failed. Using SQPNP instead!!")
                success3, rvec, tvec = cv.solvePnP(object_points, image_points, self.K, self.dist_coeffs,
                                                   flags=cv.SOLVEPNP_SQPNP)
                point_errors_sqpnp = self.calculating_pose_errors(image_points, object_points, rvec, tvec)
                if not success3 or np.percentile(point_errors_sqpnp, 90) > self.projection_threshold:
                    return None, None

        R, jac_R = cv.Rodrigues(rvec)
        Rt = R.T
        pos = -Rt @ tvec

        return pos.ravel(), Rt

    def estimate_poses(self, image_path_lists: List[Tuple[np.int64, str]],
                       failed_detection_as_null: bool = False) -> List[
        Tuple[np.int64, np.ndarray, np.ndarray]]:
        """

        :param image_path_lists: List of tuples with timestamp based in acquisition time and image path
        :return: pose list a_T_c. Camera to april grid
        """
        pose_list = []
        for ts_clk, image_path in tqdm(image_path_lists, desc='Estimating poses from April Tag Grid'):
            pos_nan = np.empty((3,), dtype=np.float64)
            R_nan = np.empty((3, 3), dtype=np.float64)

            img = cv.imread(image_path, cv.IMREAD_GRAYSCALE)
            if img is None:
                if failed_detection_as_null:
                    pose_list.append((ts_clk, pos_nan, R_nan))
                continue
            image_points, object_points = self.detect_grid(img)
            if image_points is None:
                if failed_detection_as_null:
                    pose_list.append((ts_clk, pos_nan, R_nan))
                continue  # Failed detection
            # Camera pose w_T_c
            pos, R = self.calculate_grid_pose(image_points, object_points)

            if pos is None:
                if failed_detection_as_null:
                    pose_list.append((ts_clk, pos_nan, R_nan))
                continue  # failed pose estimation

            pose_list.append((ts_clk, pos, R))

        return pose_list

    @staticmethod
    def write_pose_list(pose_list: List[Tuple[np.int64, np.ndarray, np.ndarray]], filename: str) -> None:
        with open(filename, "w") as f:
            f.write("#timestamp, p_WC_x [m], p_WC_y [m], p_WC_z [m], q_WC_w [], q_WC_x [], q_WC_y [], q_WC_z []\n")
            for ts_clk, W_t_WC, R_WC in pose_list:
                r = Rotation.from_matrix(R_WC)
                q_WC = r.as_quat()  # xyzw format
                line = f"{ts_clk},{W_t_WC[0]},{W_t_WC[1]},{W_t_WC[2]},{q_WC[3]},{q_WC[0]},{q_WC[1]},{q_WC[2]}\n"
                f.write(line)

    @staticmethod
    def show_tags(results: List[AprilTagDetection], img: np.ndarray, fig: plt.Figure) -> None:
        """
        Display tag detections
        :param results:
        :param img:
        """

        ax = fig.gca()
        ax.imshow(img, cmap='gray')
        for tag in results:

            id = tag.id
            center = tag.cxy
            ax.text(center[0], center[1], f"{id}",
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontsize=8,
                    color='b')

            for cnr in tag.corners:
                circle = plt.Circle((cnr[0], cnr[1]), 3, color='r', fill=False)
                ax.add_patch(circle)

    @staticmethod
    def plot_poses_2d(pose_list: List[Tuple[np.int64, np.ndarray, np.ndarray]]):
        """

        :param pose_list:
        :return:
        """

        positions = []
        euler_angles = []
        time_stamps = []
        for ts_clk, W_t_WC, R_WC in pose_list:
            r = Rotation.from_matrix(R_WC)
            euler_angles.append(r.as_euler('ZYX'))  # intrinsics Yaw * pitch * roll
            positions.append(W_t_WC)
            time_stamps.append(ts_clk / 1e9)

        positions = np.array(positions)
        euler_angles = np.array(euler_angles)
        fig, axes = plt.subplots(2, 1, sharex=True)
        axes[0].plot(time_stamps, positions)
        axes[0].set_title("Camera positions [m]")
        axes[0].legend(['X', 'Y', 'Z'])
        axes[1].plot(time_stamps, euler_angles)
        axes[1].set_title("Camera attitude (Euler) [rad]")
        axes[1].legend(['Yaw', 'Pitch', 'Roll'])

    @staticmethod
    def convert_pose_list_to_trajectory(pose_list: List[Tuple[np.int64, np.ndarray, np.ndarray]]) -> PoseTrajectory3D:
        xyz = []
        quat = []
        stamps = []
        for ts_clk, W_t_WC, R_WC in pose_list:
            r = Rotation.from_matrix(R_WC)
            q_WC = r.as_quat()  # xyzw format
            xyz.append(W_t_WC)
            quat.append([q_WC[3], q_WC[0], q_WC[1], q_WC[2]])  # wxyz format
            stamps.append(ts_clk)

        stamps = np.array(stamps) / 1e9
        xyz = np.array(xyz)
        quat = np.array(quat)

        pose_trajectory = PoseTrajectory3D(xyz, quat, stamps)

        return pose_trajectory

    def display_video(self, image_list: List[Tuple[np.int64, np.int64, float, str]]) -> None:
        cube_size = self.tag_size
        cube_offset = -(3 * cube_size + 2 * self.tag_spacing_ratio * cube_size) * np.array([0., 0., 0])
        cube = cube_size * np.float32([[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 0, 0],
                                       [0, 0, 1], [0, 1, 1], [1, 1, 1], [1, 0, 1]]) + cube_offset

        for ts, frame_idx, time_sec,  image_filename in tqdm(image_list[::2], "Grid pose"):

            image = cv.imread(image_filename, 0)
            if image is None:
                continue
            rgb = cv.cvtColor(image, cv.COLOR_GRAY2BGR)
            image_points, object_points = self.detect_grid(image)
            # Camera pose w_T_c
            if image_points is not None:
                pos, R = self.calculate_grid_pose(image_points, object_points)
            else:
                pos, R = None, None

            if pos is not None:
                Rt = R.T
                rvec, jac_R = cv.Rodrigues(Rt)

                tvec = -Rt @ pos

                # project 3D cube corners to image plane
                imgpts, jac = cv.projectPoints(cube, rvec, tvec, self.K, self.dist_coeffs)

                rgb = render_cube(rgb, imgpts)

            cv.imshow('frame', rgb)
            cv.waitKey(1)
        cv.destroyAllWindows()
