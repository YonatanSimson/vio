import cv2 as cv
import numpy as np
from typing import Tuple


class GlobalShift(object):
    warp_mode: int
    criteria: Tuple[int, int, float]

    def __init__(self) -> None:
        # Define the motion model
        self.warp_mode = cv.MOTION_TRANSLATION

        # Define 2x3 or 3x3 matrices and initialize the matrix to identity
        self.warp_matrix = np.eye(2, 3, dtype=np.float32)

        # Specify the number of iterations.
        number_of_iterations = 5000

        # Specify the threshold of the increment
        # in the correlation coefficient between two iterations
        termination_eps = 1e-10

        # Define termination criteria
        self.criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)

    def calculate(self, im1: np.ndarray, im2: np.ndarray) -> Tuple[np.ndarray, float]:
        """

        :param im1:
        :param im2:
        :return:
        """
        warp_matrix = np.eye(2, 3, dtype=np.float32)

        (cc, warp_matrix) = cv.findTransformECC(im1, im2, warp_matrix, self.warp_mode, self.criteria)

        shift_xy = warp_matrix[:, -1]
        return shift_xy, cc
