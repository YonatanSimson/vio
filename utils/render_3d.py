import cv2 as cv
import numpy as np


def render_cube(img: np.ndarray, imgpts):
    imgpts = np.int32(imgpts).reshape(-1, 2)
    # draw ground floor in green
    img_green = cv.drawContours(img.copy(), [imgpts[:4]], -1, (0, 255, 0), -3)
    img = (0.5 * (img_green.astype(np.int16) + img.astype(np.int16))).astype(np.uint8)
    # draw pillars in blue color
    for i, j in zip(range(4), range(4, 8)):
        img = cv.line(img, tuple(imgpts[i]), tuple(imgpts[j]), (255, 0, 0), 2)
    # draw top layer in red color
    img = cv.drawContours(img, [imgpts[4:]], -1, (0, 0, 255), 2)
    return img


def draw(img, imgpts):
    corner = tuple(imgpts[0].ravel().astype(int))
    img = cv.line(img, corner, tuple(imgpts[1].ravel().astype(int)), (255, 0, 0), 5)
    img = cv.line(img, corner, tuple(imgpts[2].ravel().astype(int)), (0, 255, 0), 5)
    img = cv.line(img, corner, tuple(imgpts[3].ravel().astype(int)), (0, 0, 255), 5)
    return img