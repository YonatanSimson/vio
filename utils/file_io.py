from typing import List


def read_list(filename: str) -> List[str]:
    """

    :param filename:
    :return: list of test names
    """
    with open(filename, "r") as ifile:
        lines = ifile.readlines()
        lines = [line.strip() for line in lines]

        return lines