import numpy as np

"""
 RALIGN - Rigid alignment of two sets of points in k-dimensional
          Euclidean space.  Given two sets of points in
          correspondence, this function computes the scaling,
          rotation, and translation that define the transform TR
          that minimizes the sum of squared errors between TR(X)
          and its corresponding points in Y.  This routine takes
          O(n k^3)-time.

 Inputs:
   X - a k x n matrix whose columns are points
   Y - a k x n matrix whose columns are points that correspond to
       the points in X
 Outputs:
   c, R, t - the scaling, rotation matrix, and translation vector
             defining the linear map TR as

                       TR(x) = c * R * x + t

             such that the average norm of TR(X(:, i) - Y(:, i))
             is minimized.
"""

"""
Copyright: Carlo Nicolini, 2013
Code adapted from the Mark Paskin Matlab version
from http://openslam.informatik.uni-freiburg.de/data/svn/tjtf/trunk/matlab/ralign.m 
"""


def ralign(X, Y):
    m, n = X.shape

    # Center both point clouds
    mx = X.mean(1)
    my = Y.mean(1)
    Xc = X - mx[:, np.newaxis]
    Yc = Y - my[:, np.newaxis]

    sx = np.mean(np.sum(Xc * Xc, 0))
    sy = np.mean(np.sum(Yc * Yc, 0))

    Sxy = np.dot(Yc, Xc.T) / n

    # U, D, V^T = SVD(Sxy)
    U, D, V = np.linalg.svd(Sxy, full_matrices=True, compute_uv=True)
    V = V.T
    # print U,"\n\n",D,"\n\n",V
    r = np.linalg.matrix_rank(Sxy)
    d = np.linalg.det(Sxy)
    S = np.eye(m)
    if d < 0:
        S[-1, -1] = -1

    R = np.dot(np.dot(U, S), V.T)

    c = np.trace(np.dot(np.diag(D), S)) / sx
    t = my - c * np.dot(R, mx)

    return R, c, t
