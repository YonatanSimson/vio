from typing import List, Tuple
import numpy as np
import cv2 as cv
from pyquaternion import Quaternion
import sophus as sp
from rig.camera import Camera


def interp_se3(se3_0: sp.SE3, se3_1: sp.SE3, t: float) -> sp.SE3:
    """
    interpolate between two 6DOF poses
    """

    p0 = se3_0.translation()
    r0 = se3_0.rotationMatrix()
    q0 = Quaternion(matrix=r0)

    p1 = se3_1.translation()
    r1 = se3_1.rotationMatrix()
    q1 = Quaternion(matrix=r1)

    p = (1 - t) * p0 + t * p1
    q = Quaternion.slerp(q0, q1, t)

    return sp.SE3(R=q.rotation_matrix, t=p)


def midpoint_triangulate(x: np.ndarray, cam: List[Camera]):
    """
    Triangulate a single 3D point from multiple observations using the midpoint algorithm
    Sources: https://stackoverflow.com/a/40862403/3481173
    Args:
        x:   Set of 2D points in homogeneous coords, (3 x n) matrix
        cam: Collection of n objects, each containing member variables
                 cam.P - 3x4 camera matrix
                 cam.R - 3x3 rotation matrix
                 cam.trans - 3x1 translation matrix
    Returns:
        midpoint: 3D point in homogeneous coords, (4 x 1) matrix
    """

    n = len(cam)                                         # No. of cameras

    I = np.eye(3)                                        # 3x3 identity matrix
    A = np.zeros((3, n))
    B = np.zeros((3, n))
    sigma2 = np.zeros((3, 1))

    for i in range(n):
        a = -np.transpose(cam[i].R).dot(cam[i].trans)        # ith camera position
        A[:, i] = a

        b = np.linalg.pinv(cam[i].P).dot(x[:, i])              # Directional vector
        b = b / b[3]
        b = b[:3] - a
        b = b / np.linalg.norm(b)
        B[:, i] = b

        sigma2 = sigma2 + b.dot(b.T.dot(a))

    C = (n * I) - B.dot(B.T)
    Cinv = np.linalg.inv(C)
    sigma1 = np.sum(A, axis=1)[:, None]
    m1 = I + B.dot(np.transpose(B).dot(Cinv))
    m2 = Cinv.dot(sigma2)

    midpoint = (1/n) * m1.dot(sigma1) - m2
    return np.vstack((midpoint, 1))


def mid_point_triangulation_single(undistorted_p0: np.ndarray,
                                   undistorted_p1: np.ndarray,
                                   pose0: np.array,
                                   pose1: np.array) -> np.ndarray:
    """
    Triangulate a 3D point with two rays coming out of two origins
    Kanatani and Kanawazi method.
    Sources: https://stackoverflow.com/a/40862403/3481173
             https://imkaywu.github.io/blog/2017/07/triangulation/
             https://openaccess.thecvf.com/content_ICCV_2019/papers/Lee_Closed-Form_Optimal_Two-View_Triangulation_Based_on_Angular_Errors_ICCV_2019_paper.pdf

    """

    ray0 = np.concatenate((undistorted_p0, [1]))
    ray0 = ray0 / np.linalg.norm(ray0)
    ray0_world = pose0[:3, :3] @ ray0
    center0 = pose0[:3, 3]

    ray1 = np.concatenate((undistorted_p1, [1]))
    ray1 = ray1 / np.linalg.norm(ray1)
    ray1_world = pose1[:3, :3] @ ray1
    center1 = pose1[:3, 3]

    t = center0 - center1
    q = np.cross(ray0_world, ray1_world)

    q_sqrd = np.linalg.norm(q) ** 2
    q_norm = q / q_sqrd

    r0 = center0 + q_norm.dot(np.cross(ray1_world, t)) * ray0_world
    r1 = center1 + q_norm.dot(np.cross(ray0_world, t)) * ray1_world

    mid_point = 0.5 * (r0 + r1)

    return mid_point, r0, r1


def stereo_triangulation_midpoint(points0: np.ndarray, points1: np.ndarray, cameras: List[Camera]) -> np.ndarray:
    """
    Kanatani and Kanawazi midpoint triangulation method for multiple points
    points0: Nx2 numpy array of distorted points (xy)
    points1: Nx2 numpy array of distorted points (xy)
    cameras: List of 2 Cameras, each containing member variables
             cam.P - 3x4 camera matrix
             cam.R - 3x3 rotation matrix
             cam.trans - 3x1 translation matrix
    Returns:
        midpoint: 3D point in homogeneous coords, (N x 3) matrix
        residual: L2 error for the projection of each point into pixels
    """

    undistorted_p0 = cv.undistortPoints(np.expand_dims(points0, axis=1), cameras[0].K, cameras[0].dist_coeffs)
    undistorted_p1 = cv.undistortPoints(np.expand_dims(points1, axis=1), cameras[1].K, cameras[1].dist_coeffs)

    points_3d = []
    for p0, p1 in zip(undistorted_p0, undistorted_p1):
        p3_mp, r0, r1 = \
            mid_point_triangulation_single(p0.squeeze(),
                                           p1.squeeze(),
                                           cameras)
        points_3d.append(p3_mp)

    points_3d = np.array(points_3d)

    # Calculate residual
    _, residuals0 = cameras[0].project_and_calculate_residual(points_3d, points0)
    _, residuals1 = cameras[1].project_and_calculate_residual(points_3d, points1)
    residual = 0.5 * (residuals0 + residuals1)

    return np.array(points_3d), residual


def transform_points(pose: np.ndarray, points: np.ndarray) -> np.ndarray:
    """

    :param pose: 4x4 numpy array for transforming the 3D points from one frame to another
    :param points: Nx3 numpy array for N 3D points
    :return:
    """
    return (pose[:3, :3] @ points.T).T + pose[:3, -1]


def pose_to_vectors(pose: np.ndarray, invert: bool = True) -> Tuple[np.ndarray, np.ndarray]:
    """
    Convert pose to rvec tvec and invert the pose
    :return:
    """
    R = pose[:3, :3]
    if invert:
        Rt = R.T
        rvec, jac_R = cv.Rodrigues(Rt)
        tvec = -Rt @ pose[:3, -1]
    else:
        rvec, jac_R = cv.Rodrigues(R)
        tvec = pose[:3, -1]

    return rvec, tvec


def skew_matrix(a: np.ndarray) -> np.ndarray:
    skew = np.array([[0,    -a[2],  a[1]],
                     [a[2],     0, -a[0]],
                     [-a[1], a[0],    0]])
    return skew


def midpoint_triangulate(x: np.ndarray, cam: List[Camera]):
    """
    Triangulate a single 3D point from multiple observations using the midpoint algorithm
    Sources: https://stackoverflow.com/a/40862403/3481173
    Args:
        x:   Set of 2D points in homogeneous coords, (3 x n) matrix
        cam: Collection of n objects, each containing member variables
                 cam.P - 3x4 camera matrix world to cam
                 cam.R - 3x3 rotation matrix world to cam
                 cam.trans - 3x1 translation matrix world to cam
    Returns:
        midpoint: 3D point in homogeneous coords, (4 x 1) matrix
    """

    n = len(cam)                                         # No. of cameras

    I = np.eye(3)                                        # 3x3 identity matrix
    A = np.zeros((3, n))
    B = np.zeros((3, n))
    sigma2 = np.zeros((3, 1))

    for i in range(n):
        a = -np.transpose(cam[i].R).dot(cam[i].trans)        # ith camera position
        A[:, i] = a

        b = np.linalg.pinv(cam[i].P).dot(x[:, i])              # Directional vector
        b = b / b[3]
        b = b[:3] - a
        b = b / np.linalg.norm(b)
        B[:, i] = b

        sigma2 = sigma2 + b.dot(b.T.dot(a))

    C = (n * I) - B.dot(B.T)
    Cinv = np.linalg.inv(C)
    sigma1 = np.sum(A, axis=1)[:, None]
    m1 = I + B.dot(np.transpose(B).dot(Cinv))
    m2 = Cinv.dot(sigma2)

    midpoint = (1/n) * m1.dot(sigma1) - m2
    return np.vstack((midpoint, 1))


def mid_point_triangulation_single(undistorted_p0: np.ndarray,
                                   undistorted_p1: np.ndarray,
                                   pose0: np.ndarray,
                                   pose1: np.ndarray) -> np.ndarray:
    """
    Triangulate a 3D point with two rays coming out of two origins
    Kanatani and Kanawazi method.
    Sources: https://stackoverflow.com/a/40862403/3481173
             https://imkaywu.github.io/blog/2017/07/triangulation/
             https://openaccess.thecvf.com/content_ICCV_2019/papers/Lee_Closed-Form_Optimal_Two-View_Triangulation_Based_on_Angular_Errors_ICCV_2019_paper.pdf

    """

    ray0 = np.concatenate((undistorted_p0, [1]))
    ray0 = ray0 / np.linalg.norm(ray0)
    ray0_world = pose0[:3, :3] @ ray0
    center0 = pose0[:3, 3]

    ray1 = np.concatenate((undistorted_p1, [1]))
    ray1 = ray1 / np.linalg.norm(ray1)
    ray1_world = pose1[:3, :3] @ ray1
    center1 = pose1[:3, 3]

    t = center0 - center1
    q = np.cross(ray0_world, ray1_world)

    q_sqrd = np.linalg.norm(q) ** 2
    q_norm = q / q_sqrd

    r0 = center0 + q_norm.dot(np.cross(ray1_world, t)) * ray0_world
    r1 = center1 + q_norm.dot(np.cross(ray0_world, t)) * ray1_world

    mid_point = 0.5 * (r0 + r1)

    return mid_point, r0, r1


def stereo_triangulation_midpoint(points0: np.ndarray,
                                  points1: np.ndarray,
                                  pose0: np.ndarray,
                                  pose1: np.ndarray,
                                  K: np.ndarray,
                                  dist_coeffs: np.ndarray) -> np.ndarray:
    """
    Kanatani and Kanawazi midpoint triangulation method for multiple points
    :param points0: Nx2 numpy array of distorted points (xy)
    :param points1: Nx2 numpy array of distorted points (xy)
    :param pose0: 4x4 numpy array world_T_cam0
    :param pose1: 4x4 numpy array world_T_cam1
    :param K: numpy array 3x3 camera projection matrix
    :param dist_coeffs: Numpy array 5x1 k1,k2,p1,p2 distortion coefficients (RadTan)
    Returns:
        midpoint: 3D point in homogeneous coords, (N x 3) matrix
        residual: L2 error for the projection of each point into pixels
    """

    undistorted_p0 = cv.undistortPoints(np.expand_dims(points0, axis=1), K, dist_coeffs)
    undistorted_p1 = cv.undistortPoints(np.expand_dims(points1, axis=1), K, dist_coeffs)

    points_3d = []
    for p0, p1 in zip(undistorted_p0, undistorted_p1):
        p3_mp, _, __ = \
            mid_point_triangulation_single(p0.squeeze(),
                                           p1.squeeze(),
                                           pose0,
                                           pose1)
        points_3d.append(p3_mp)

    points_3d = np.array(points_3d)

    # # Calculate residual
    points0_2d_tag = project_points(points_3d, pose0, K, dist_coeffs)
    points1_2d_tag = project_points(points_3d, pose1, K, dist_coeffs)
    residuals0 = np.linalg.norm(points0_2d_tag - points0, axis=0)
    residuals1 = np.linalg.norm(points1_2d_tag - points1, axis=0)
    residual = 0.5 * (residuals0 + residuals1)

    return np.array(points_3d), residual


def project_points(points: np.ndarray,
                   pose: np.ndarray,
                   K: np.ndarray,
                   dist_coeffs: np.ndarray) -> np.ndarray:
    """

    :param points: Nx3 Numpy array points in 3D world frame
    :param pose: 4x4 numpy array world_T_cam
    :param K: numpy array 3x3 camera projection matrix
    :param dist_coeffs: Numpy array 5x1 k1,k2,p1,p2 distortion coefficients (RadTan)
    :return:
    """

    R = pose[:3, :3].T
    t = -pose[:3, :3].T @ pose[:3, 3]
    rvec, _ = cv.Rodrigues(R)
    if len(points.shape) == 1:
        projected_pt2d, _ = cv.projectPoints(points[np.newaxis, :], rvec, t, K, dist_coeffs)
    elif len(points.shape) == 2:
        projected_pt2d, _ = cv.projectPoints(points, rvec, t, K, dist_coeffs)

    return projected_pt2d.squeeze()

