import numpy as np
import os
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import yaml
import warnings
import cv2 as cv
import logging
import argparse
from functools import partial
from tqdm import tqdm
from typing import Dict, Tuple
import configparser

from matplotlib.animation import FFMpegWriter
from scipy.spatial.transform import Rotation
from pyquaternion import Quaternion

from readers.sightec_data_reader import DataReader
from utils.apriltag_pose import AprilTagPose
from evo.tools.file_interface import read_euroc_csv_trajectory
from orientation_estimation.complementary_filter import ComplementaryFilter
from orientation_estimation.madgwick_filter import MadgwickFilter


def rotate_to_orthophoto(points_target):
    alpha = -np.pi / 2   # 90 degrees rotation
    target_center = points_target.mean(axis=0)
    R_ortho = np.array([[np.cos(alpha), -np.sin(alpha)],
                        [np.sin(alpha),  np.cos(alpha)]])

    points_ortho_target = points_target.copy()
    points_ortho_target -= target_center
    points_ortho_target = (R_ortho @ points_ortho_target.T).T
    points_ortho_target += R_ortho @ target_center

    return points_ortho_target


def warp_points(points: np.ndarray, H: np.ndarray) -> np.ndarray:
    """
    Warp points from a 2d image to another 2d image
    :param points: Nx2 Numpy array. pixel coordinates in xy
    :param H: 3x3 Numpy array. Homography matrix
    :return:
    """
    number_of_points = len(points)
    points_homogenous = np.vstack((points.T, np.ones((1, number_of_points))))

    points_target = H @ points_homogenous
    points_target = points_target / points_target[-1, :]

    return points_target[:2, :].T


def warp_single_image(args: Tuple[np.ndarray, np.ndarray, float], K: np.ndarray, cam_R_imu: np.ndarray):
    img, imu_R_ned, t_img = args

    orthophot_R_cam = Rotation.from_euler('XYZ', [0., 0., 0.], degrees=True).as_matrix()

    cam_R_ned = cam_R_imu @ imu_R_ned
    orthophot_R_ned = orthophot_R_cam @ cam_R_ned
    euler_angles = Rotation.from_matrix(orthophot_R_ned).as_euler('XYZ', degrees=True)
    H = K @ orthophot_R_ned.T @ np.linalg.inv(K)
    H /= H[2, 2]

    h, w = img.shape

    # Calculate where the pixels will land on target
    tl_source = [0, 0]
    tr_source = [w - 1, 0]
    bl_source = [0, h - 1]
    br_source = [w - 1, h - 1]
    test_labels = ["top left", "top right", "bottom right", "bottom left"]
    points_source = np.vstack((tl_source, tr_source, br_source, bl_source))
    points_target = warp_points(points_source, H)

    # Rotate to align with orthophoto
    points_target = rotate_to_orthophoto(points_target)

    max_width = int(points_target[:, 0].max() - points_target[:, 0].min() + 1)  # round up
    max_height = int(points_target[:, 1].max() - points_target[:, 1].min() + 1)  # round up

    dsize = (max_width, max_height)
    min_x, min_y = points_target.min(axis=0)
    max_x, max_y = points_target.max(axis=0)
    if min_x < 0:
        offset_x = int(-min_x + 1)
        points_target[:, 0] += offset_x
    if min_y < 0:
        offset_y = int(-min_y + 1)
        points_target[:, 1] += offset_y
    if max_x > max_width - 1:
        offset_x = int(max_x - max_width + 1 + 0.5)
        points_target[:, 0] -= offset_x
    if max_y > max_height - 1:
        offset_y = int(max_y - max_height + 1 + 0.5)
        points_target[:, 1] -= offset_y

    # Rotate for orthophoto

    # Warp to BEV (Birds Eye View)
    # Using target to source because of WARP_INVERSE_MAP
    H_image, _ = cv.findHomography(points_target, points_source)
    if np.prod(dsize) / np.prod(img.shape) > 100:
        logging.warning(f"Image warp likely to fail due to sharp roll {euler_angles[0]:.02f}, pitch {euler_angles[1]:.02f}")
        img_warped = None
    else:
        try:
            img_warped = cv.warpPerspective(img, H_image, dsize, flags=cv.WARP_INVERSE_MAP)
        except Exception as e:
            logging.warning(e)
            img_warped = None

    return img_warped, euler_angles


def calibrate_magnometer(mag: np.ndarray, calibrate_magnometer_file: str) -> np.ndarray:
    with open(calibrate_magnometer_file) as f:
        lines = [line.rstrip("\n") for line in f.readlines() if "#" not in line and "=" in line]

    imu_config = {}
    for line in lines:
        key, value = line.split("=")
        if "Valid" in key or "Is" in key:
            imu_config[key] = True if value is "true" else False
        else:
            imu_config[key] = float(value)

    compass_max = np.array([imu_config["CompassCalMaxX"],
                            imu_config["CompassCalMaxY"],
                            imu_config["CompassCalMaxZ"]])

    compass_min = np.array([imu_config["CompassCalMinX"],
                            imu_config["CompassCalMinY"],
                            imu_config["CompassCalMinZ"]])

    compass_delta = 0.5 * (compass_max - compass_min)
    max_delta = compass_delta.max()

    compass_scale = max_delta / compass_delta
    compass_offset = 0.5 * (compass_max + compass_min)

    compass_ellipsoid_offset = np.array([imu_config["compassCalOffsetX"],
                                         imu_config["compassCalOffsetY"],
                                         imu_config["compassCalOffsetZ"]])

    compass_cor = np.array(
        [[imu_config["compassCalCorr11"], imu_config["compassCalCorr12"], imu_config["compassCalCorr13"]],
         [imu_config["compassCalCorr21"], imu_config["compassCalCorr22"], imu_config["compassCalCorr23"]],
         [imu_config["compassCalCorr31"], imu_config["compassCalCorr32"], imu_config["compassCalCorr33"]]])

    mag = compass_scale * (mag - compass_offset)
    mag = (mag - compass_ellipsoid_offset) @ compass_cor.T

    return mag


def run_and_calculate_metrics(args: Dict) -> pd.DataFrame:

    longitude = 34.884360
    latitude = 32.132910
    altitude = 100.0
    # Filter coefficient
    alpha = 0.99  # Complementary filter
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = False
    has_april_tag = True
    record_video = False
    calibrate_magnometer_file = None  # "/home/yonatan/data/madgwick_filter/RTIMULib.ini"

    input_folder = args.input_folder
    metric_subfolder = args.metric_subfolder

    if args.output_folder is None:
        output_folder = input_folder
    else:
        output_folder = args.output_folder

    output_gt_pose = os.path.join(input_folder, "gt", 'April_WC_gt.csv')
    if not os.path.isdir(os.path.join(input_folder, "gt")):
        os.makedirs(os.path.join(input_folder, "gt"))

    has_april_tag_gt = os.path.isfile(output_gt_pose)

    metrics_folder = os.path.join(output_folder, metric_subfolder)
    if not os.path.isdir(metrics_folder):
        os.makedirs(metrics_folder)

    try:
        cam_imu_extrinsics_calibration = os.path.join(input_folder, "imu_camera_extrinsics.yaml")
        with open(cam_imu_extrinsics_calibration) as f:
            imu_cam_calibration = yaml.load(f, Loader=yaml.FullLoader)
        print(imu_cam_calibration)

        imu_calibration_cam0 = imu_cam_calibration['cam0']

        T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])
    except Exception as e:
        print(e)
        logging.warning("Has no Extrinsic calibration!! Rough Estimation instead")
        R_cam_imu = np.array([[0.0, -1.0, 0.0],
                              [-1.0, 0.0, 0.0],
                              [0.0, 0.0, -1.0]])
        T_cam_imu = np.eye(4)
        T_cam_imu[:3, :3] = R_cam_imu

    reader = DataReader(input_folder)
    if calibrate_magnometer_file is not None:
        mag = reader.imu_data[['MagField x', ' MagField y', ' MagField z [mgauss]']].values
        mag = calibrate_magnometer(mag, calibrate_magnometer_file)
        reader.imu_data[['MagField x', ' MagField y', ' MagField z [mgauss]']] = mag

    if has_april_tag:
        april_detector = AprilTagPose(K=reader.camera.K,
                                      dist_coeffs=reader.camera.dist_coeffs,
                                      tag_cols=6,
                                      tag_rows=6,
                                      tag_size=tag_size,
                                      tag_spacing_ratio=tag_spacing_ratio,
                                      min_detected_tags=18,
                                      projection_threshold=3.)

    if show_video:
        image_list = reader.get_image_list()
        warnings.simplefilter("ignore")
        april_detector.display_video(image_list)

    if has_april_tag:
        if not has_april_tag_gt:
            image_path_list = reader.get_image_list(use_time=True)
            # a_T_c. Camera to april grid
            pose_list = april_detector.estimate_poses(image_path_list, failed_detection_as_null=False)
            april_detector.plot_poses_2d(pose_list)
            april_detector.write_pose_list(pose_list, output_gt_pose)
            pose_trajectory = april_detector.convert_pose_list_to_trajectory(pose_list)
        else:
            pose_trajectory = read_euroc_csv_trajectory(output_gt_pose)

        # Convert from Cam->April Grid to IMU->April Grid. a_T_imu
        pose_trajectory.transform(T_cam_imu, right_mul=True)
        # April Grid is positioned at NWU
        # We would like to get the transform relative to NED by rolling 180deg over x axis
        w_R_a = Rotation.from_euler('ZYX', [0, 0, -180], degrees=True).as_matrix()
        T_NED_April = np.eye(4)
        T_NED_April[:3, :3] = w_R_a
        pose_trajectory.transform(T_NED_April)

        # IMU to (NED) world
        ts_world_cam = pose_trajectory.timestamps
        xyz_imu = pose_trajectory.positions_xyz
        euler_imu = pose_trajectory.get_orientations_euler(axes="szyx")

        fig, axes = plt.subplots(2, 2, sharex=True)
        axes[0, 0].plot(ts_world_cam, xyz_imu)
        axes[0, 0].set_title("IMU to world (NED) XYZ [m]")
        axes[0, 0].legend(['x', 'y', 'z'])
        axes[1, 0].plot(ts_world_cam, np.rad2deg(euler_imu))
        axes[1, 0].set_title("IMU to world (NED) orientation (euler) [deg]")
        axes[1, 0].legend(['Yaw', 'Pitch', 'Roll'])

        # World to CAM
        pose_trajectory_imu_world = pose_trajectory.invert()
        ts_world = pose_trajectory_imu_world.timestamps
        xyz_world = pose_trajectory_imu_world.positions_xyz
        euler_world = pose_trajectory_imu_world.get_orientations_euler(axes="szyx")
        axes[0, 1].plot(ts_world, xyz_world)
        axes[0, 1].set_title("World to IMU XYZ [m]")
        axes[0, 1].legend(['x', 'y', 'z'])
        axes[1, 1].plot(ts_world, euler_world)
        axes[1, 1].set_title("World to IMU orientation (euler) [rad]")
        axes[1, 1].legend(['Yaw', 'Pitch', 'Roll'])
        plt.tight_layout()
        df_gt = pd.DataFrame(index=ts_world, data=euler_imu)
    else:
        pose_trajectory = None

    # Deploy complementary filter
    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    mag = reader.imu_data[['MagField x', ' MagField y', ' MagField z [mgauss]']]
    ts_sec = reader.imu_data['nav-time [sec]']  # reader.convert_system_clk_to_sec(ts_system_clk)

    if record_video:
        fourcc = cv.VideoWriter_fourcc(*'AVC1')  # H264
        video_out = cv.VideoWriter(os.path.join(input_folder, "yaw_debug.avi"), fourcc, 20.0, tuple(reader.camera.size))

    if not os.path.exists(os.path.join(input_folder, "complementary_filter.csv")):
        comp_filter = ComplementaryFilter(alpha=alpha)
        time_stamps, pitch_roll_ = comp_filter.run(reader)
        df_cf = pd.DataFrame({"time": time_stamps, "roll": pitch_roll_[:, 0], "pitch": pitch_roll_[:, 1]})
        df_cf.set_index('time', inplace=True)
        df_cf.to_csv(os.path.join(input_folder, "complementary_filter.csv"))
    else:
        df_cf = pd.read_csv(os.path.join(input_folder, "complementary_filter.csv"), index_col=[0])

    # Madgwick filter
    madgwick_data_csv = os.path.join(metrics_folder, "madgwick_filter.csv")
    if not os.path.exists(madgwick_data_csv) or True:
        mf = MadgwickFilter(
            longitude=longitude,
            latitude=latitude,
            altitude=altitude,
            use_magnometer=True)

        m0 = mag.iloc[0].values
        a0 = acc.iloc[0].values
        mf.init(a0=a0, m0=m0)
        gyb = np.zeros((3,))
        ts_array, q_array = mf.run(reader, gyb=gyb)
        gyro_bias = mf.gyro_bias
        print(f"Madgwick Gyro bias: {gyro_bias}")
        print(f"Gyro bias ref: {gyb}")
        # q = wxyz
        df_madgwick = pd.DataFrame(
            {"time": ts_array,
             "w": q_array[:, 0],
             "x": q_array[:, 1],
             "y": q_array[:, 2],
             "z": q_array[:, 3]
             }
        )
        df_madgwick.set_index('time', inplace=True)
        df_madgwick.to_csv(madgwick_data_csv)
    else:
        df_madgwick = pd.read_csv(madgwick_data_csv, index_col=[0])
        ts_array = df_madgwick.index.values
        q_array = df_madgwick.values

    imu_T_cam = reader.camera.cam_to_rig
    cam_R_imu = imu_T_cam.rotationMatrix().T
    warp_single_image_partial = partial(warp_single_image,
                                        K=reader.camera.K,
                                        cam_R_imu=cam_R_imu)

    image_ts = reader.image_ts
    dt_imu = np.mean(np.diff(ts_sec))

    if record_video:
        metadata = dict(title='Madgwick Filter', artist='Matplotlib',
                        comment='Yaw, Pitch, Roll correction')
        writer = FFMpegWriter(fps=10, metadata=metadata)
        matplotlib.use("Agg")
        fig, axes = plt.subplots(1, 2, figsize=(16, 8))
        writer.setup(fig, os.path.join(input_folder, "debug_test.mp4"), 300)

    madgwick_euler_ned_imu_list = []
    madgwick_q_ned_imu_list = []
    ts_camera = []
    poses_se3 = pose_trajectory.poses_se3
    quaternion_ned_imu_gt = pose_trajectory.orientations_quat_wxyz
    ned_R_imu_gt_prev = None
    q_ned_imu_relative_list = []
    ts_relative_list = []
    for image_ts_series in tqdm(image_ts.iterrows(), "Visualize Madgwick filter", total=len(image_ts)):  #idx in range(1, num_samples):
        img_ts_us = image_ts_series[0]
        img_ts_sec = image_ts_series[1].ts_sec

        ind_imu = np.searchsorted(ts_sec, img_ts_sec, side='left')

        m = mag.iloc[ind_imu].values
        m = m / np.linalg.norm(m)
        if ind_imu == 0 and abs(ts_sec.iloc[ind_imu] - img_ts_sec) > dt_imu:
            continue

        ts_imu, q_ned_imu = ts_array[ind_imu], Quaternion(q_array[ind_imu])

        # Relative pose
        try:
            index_image = ts_world_cam.tolist().index(img_ts_sec)
            ned_T_imu_gt = poses_se3[index_image]
            ned_R_imu_gt = ned_T_imu_gt[:3, :3]
        except ValueError:
            ned_R_imu_gt = np.empty((3, 3), dtype=np.float64)
            ned_R_imu_gt.fill(np.nan)

        if ned_R_imu_gt_prev is not None:
            ned_R_cam_gt_rel = ned_R_imu_gt_prev.T @ ned_R_imu_gt
            isnan = np.any(np.isnan(ned_R_cam_gt_rel))

            ned_q_cam_gt_rel = Quaternion(np.array([np.nan, np.nan, np.nan, np.nan])) if isnan else Quaternion(matrix=ned_R_cam_gt_rel)
            q_ned_imu_relative_list.append(ned_q_cam_gt_rel)
            ts_relative_list.append(img_ts_sec)
            dt = img_ts_sec - ts_prev

        ned_R_imu_gt_prev = ned_R_imu_gt
        ts_prev = img_ts_sec

        img = reader.read_images(img_ts_us)[0]
        rgb = cv.cvtColor(img, cv.COLOR_GRAY2BGR)

        ned_R_imu = q_ned_imu.rotation_matrix
        euler_ned_imu = Rotation.from_matrix(ned_R_imu).as_euler('zyx', degrees=True)
        madgwick_euler_ned_imu_list.append(euler_ned_imu)
        madgwick_q_ned_imu_list.append(q_ned_imu)
        ts_camera.append(img_ts_sec)

        imu_R_ned = q_ned_imu.conjugate.rotation_matrix
        args = img, imu_R_ned, img_ts_sec
        img_warped, euler_angles = warp_single_image_partial(args)

        # Export to disk for analyzing
        roll, pitch, yaw = euler_angles
        if record_video:
            axes[0].imshow(img, cmap='gray')
            axes[0].set_title("img")
            axes[1].imshow(img_warped, cmap='gray')
            axes[1].set_title(f"Warped: roll, pitch, yaw - ({roll:0.1f}, {pitch:0.1f}, {yaw:0.1f}) ")
            plt.tight_layout()
            writer.grab_frame()

            try:
                ypr_gt_deg = np.rad2deg(df_gt.loc[img_ts_sec, :].values)
                rgb = cv.putText(img=rgb,
                                 text=f"ypr GT: {ypr_gt_deg[0]:.01f}, {ypr_gt_deg[1]:.01f}, {ypr_gt_deg[2]:.01f}[deg]",
                                 org=(15, 125),
                                 fontFace=cv.FONT_HERSHEY_TRIPLEX,
                                 fontScale=0.7,
                                 color=(255, 255, 0),
                                 thickness=1)
            except KeyError:
                print(f"Key error in GT: {img_ts_sec}")

            ypr = q_ned_imu.yaw_pitch_roll
            rgb = cv.putText(img=rgb,
                             text=f"ypr: {np.rad2deg(ypr[0]):.01f}, {np.rad2deg(ypr[1]):.01f}, {np.rad2deg(ypr[2]):.01f}[deg]",
                             org=(15, 35),
                             fontFace=cv.FONT_HERSHEY_TRIPLEX,
                             fontScale=0.7,
                             color=(0, 255, 0),
                             thickness=1)

            rgb = cv.putText(img=rgb, text=f"m: {m[0]:.01f}, {m[1]:.01f}, {m[2]:.01f}",
                             org=(15, 65),
                             fontFace=cv.FONT_HERSHEY_TRIPLEX,
                             fontScale=0.7,
                             color=(255, 0, 255),
                             thickness=1)

            yaw_direct = np.arctan2(-m[1], m[0])
            rgb = cv.putText(img=rgb, text=f"yaw(direct): {np.rad2deg(yaw_direct):.01f}[deg]",
                             org=(15, 95),
                             fontFace=cv.FONT_HERSHEY_TRIPLEX,
                             fontScale=0.7,
                             color=(0, 255, 255),
                             thickness=1)

            roll_cf, pitch_cf = np.rad2deg(df_cf.loc[ts_imu, :])
            rgb = cv.putText(img=rgb, text=f"pitch, roll (cf): {pitch_cf:.01f}, {roll_cf:.01f}[deg]",
                             org=(15, 155),
                             fontFace=cv.FONT_HERSHEY_TRIPLEX,
                             fontScale=0.7,
                             color=(0, 0, 255),
                             thickness=1)

            video_out.write(rgb)

        cv.imshow('yaw', rgb)
        cv.waitKey(1)

    cv.destroyAllWindows()
    if record_video:
        video_out.release()
        writer.finish()

    matplotlib.use("TkAgg")

    madgwick_q_ned_imu_relative_error = [madgwick_q_ned_imu_list[i - 1].conjugate * madgwick_q_ned_imu_list[i] for i in range(1, len(madgwick_euler_ned_imu_list))]
    madgwick_euler_ned_imu = np.array(madgwick_euler_ned_imu_list)
    ts_camera = np.array(ts_camera)

    common_ts, comm_alg, comm_gt = np.intersect1d(ts_camera, ts_world_cam, return_indices=True)
    madgwick_euler_ned_imu_ = madgwick_euler_ned_imu[comm_alg, :]
    euler_imu_ = np.rad2deg(euler_imu[comm_gt, :])

    # Calculate absolute rotation errors
    # We don't subtract in euler angles. First find rotation difference then extract the euler angles from that
    # In this way we avoid wrap around issues common with subtracting one set of euler angles from another
    err_angles_list = []
    for ind_alg, ind_gt in zip(comm_alg, comm_gt):
        q_gt = Quaternion(quaternion_ned_imu_gt[ind_gt])
        q_alg = madgwick_q_ned_imu_list[ind_alg]
        q_err = q_gt.conjugate * q_alg  # q_alg.conjugate * q_gt
        euler_e = np.rad2deg(q_err.yaw_pitch_roll)
        err_angles_list.append(euler_e)

    err_angles = np.array(err_angles_list)

    absolute_error = np.empty(madgwick_euler_ned_imu.shape, dtype=np.float64)
    absolute_error.fill(np.nan)
    absolute_error[comm_alg, :] = err_angles

    # Display errors
    fig, axes = plt.subplots(3, 2, sharex=True, figsize=(16, 8))
    axes[0, 0].set_title("IMU to World(NED) Yaw [deg]")
    axes[0, 0].scatter(common_ts, madgwick_euler_ned_imu_[:, 0], label="Madgwick", marker=".")
    axes[0, 0].scatter(common_ts, euler_imu_[:, 0], label="April Tag GT", marker=".")
    axes[0, 0].legend()
    axes[0, 0].grid()
    axes[1, 0].set_title("IMU to World(NED) Pitch [deg]")
    axes[1, 0].scatter(common_ts, madgwick_euler_ned_imu_[:, 1], label="Madgwick", marker=".")
    axes[1, 0].scatter(common_ts, euler_imu_[:, 1], label="April Tag GT", marker=".")
    axes[1, 0].legend()
    axes[1, 0].grid()
    axes[2, 0].set_title("IMU to World(NED) Roll [deg]")
    axes[2, 0].scatter(common_ts, madgwick_euler_ned_imu_[:, 2], label="Madgwick", marker=".")
    axes[2, 0].scatter(common_ts, euler_imu_[:, 2], label="April Tag GT", marker=".")
    axes[2, 0].legend()
    axes[2, 0].grid()

    axes[0, 1].set_title("Error Yaw [deg]")
    axes[0, 1].plot(ts_camera, absolute_error[:, 0], marker=".")
    axes[0, 1].grid()
    axes[1, 1].set_title("Error Pitch [deg]")
    axes[1, 1].plot(ts_camera, absolute_error[:, 1], marker=".")
    axes[1, 1].grid()
    axes[2, 1].set_title("Error Roll [deg]")
    axes[2, 1].plot(ts_camera, absolute_error[:, 2], marker=".")
    axes[2, 1].grid()
    plt.tight_layout()
    plt.savefig(os.path.join(metrics_folder, "absolute_error.png"))

    absolute_error_rmse = np.sqrt(np.nanmean(absolute_error ** 2, axis=0))
    absolute_error_q95 = np.nanpercentile(np.abs(absolute_error), 95, axis=0)

    # Relative errors
    ts_relative = np.array(ts_relative_list)
    common_relative_ts, comm_alg, comm_gt = np.intersect1d(ts_camera[1:], ts_relative, return_indices=True)

    q_err_relative = []
    for idx_alg, idx_gt, ts_ in list(zip(comm_alg, comm_gt, common_relative_ts)):
        q_err_relative.append(q_ned_imu_relative_list[idx_gt] * madgwick_q_ned_imu_relative_error[idx_alg])

    euler_err_relative = np.rad2deg(np.array([q.yaw_pitch_roll for q in q_err_relative]))

    relative_error_rmse = np.sqrt(np.nanmean(euler_err_relative ** 2, axis=0))
    relative_error_q95 = np.nanpercentile(np.abs(euler_err_relative), 95, axis=0)

    fig, axes = plt.subplots(3, 1, sharex=True, figsize=(16, 12))
    axes[0].set_title("Relative error Yaw [deg]")
    axes[0].plot(common_relative_ts, euler_err_relative[:, 0], marker=".")
    axes[0].grid()
    axes[1].set_title("Relative error Pitch [deg]")
    axes[1].plot(common_relative_ts, euler_err_relative[:, 1], marker=".")
    axes[1].grid()
    axes[2].set_title("Relative error Roll [deg]")
    axes[2].plot(common_relative_ts, euler_err_relative[:, 2], marker=".")
    axes[2].grid()
    plt.tight_layout()
    plt.savefig(os.path.join(metrics_folder, "relative_error.png"))

    df_errors = pd.DataFrame(columns=["Yaw[deg]", "Pitch[deg]", "Roll[deg]"],
                             index="absolute_error_rmse,absolute_error_q95,relative_error_rmse,relative_error_q95".split(","),
                             data=np.row_stack((absolute_error_rmse, absolute_error_q95, relative_error_rmse, relative_error_q95)))

    error_csv = os.path.join(metrics_folder, "error_stats.csv")
    error_html = os.path.join(metrics_folder, "error_stats.html")
    df_errors.to_csv(error_csv)
    with open(error_html, 'w') as fo:
        df_errors.to_html(fo)

    return df_errors


if __name__ == "__main__":
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    """
    To encode with h264 make sure to set environment variable:
    OPENCV_FFMPEG_WRITER_OPTIONS="vcodec\;x264|vprofile\;high|vlevel\;4.0"
    """
    parser = argparse.ArgumentParser(description='Arguments for running Madgwick filter')
    parser.add_argument(
        "-i",
        "--input_folder",
        help="Input folder",
        type=str)

    parser.add_argument(
        "-o",
        "--output_folder",
        help="Output folder",
        type=str,
        default=None)

    parser.add_argument(
        "-m",
        "--metric_subfolder",
        help="Subfolder for metrics",
        type=str,
        default="metrics")

    args = parser.parse_args()

    df_errors = run_and_calculate_metrics(args)
    print("Done")
