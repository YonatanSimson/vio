"""
Origin of this code is the following a matlab script
https://github.com/CanCanZeng/hand_eye_calib/blob/master/handEye.m
% handEye - performs hand/eye calibration
%
%     gHc = handEye(bHg, wHc)
%
%     bHg - pose of gripper relative to the robot base..
%           (Gripper center is at: g0 = Hbg * [0;0;0;1] )
%           Matrix dimensions are 4x4xM, where M is ..
%           .. number of camera positions.
%           Algorithm gives a non-singular solution when ..
%           .. at least 3 positions are given
%           Hbg(:,:,i) is i-th homogeneous transformation matrix
%     wHc - pose of camera relative to the world ..
%           (relative to the calibration block)
%           Dimension: size(Hwc) = size(Hbg)
%     gHc - 4x4 homogeneous transformation from gripper to camera
%           , that is the camera position relative to the gripper.
%           Focal point of the camera is positioned, ..
%           .. relative to the gripper, at
%                 f = gHc*[0;0;0;1];
%
% References: R.Tsai, R.K.Lenz "A new Technique for Fully Autonomous
%           and Efficient 3D Robotics Hand/Eye calibration", IEEE
%           trans. on robotics and Automation, Vol.5, No.3, June 1989
%
% Notation: wHc - pose of camera frame (c) in the world (w) coordinate system
%                 .. If a point coordinates in camera frame (cP) are known
%                 ..     wP = wHc * cP
%                 .. we get the point coordinates (wP) in world coord.sys.
%                 .. Also referred to as transformation from camera to world
%
"""

from copy import deepcopy
from typing import List
from warnings import warn

import numpy as np
import pandas as pd
import scipy
from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation
from sklearn.linear_model import LinearRegression

from imu_integration.gyro_integration import quat_integration_step_rk4
from utils.common import skew_matrix


def angle_of_rotation_matrix(R: np.ndarray) -> float:
    """

    :param R:
    :return: angle of the axis-angle representation
    """
    theta = np.arccos(0.5 * (R.trace() - 1))
    return theta


def rot2quat(R: np.ndarray) -> np.ndarray:
    """

    :param R:
    :return:
    """
    w4 = np.sqrt(1 + R.trace())

    scale = 1 / w4
    q = scale * np.array([R[2, 1] - R[1, 2],
                          R[0, 2] - R[2, 0],
                          R[1, 0] - R[0, 1]])
    return q


def quat2rot(q_: np.ndarray) -> np.ndarray:
    """
    R - 3x3 rotation matrix
        q = sin(theta/2) * v
        theta - rotation angle
        v    - unit rotation axis, |v| = 1
    :param q_: 3x1 unit quaternion
    :return: R
    """
    q = q_[:, np.newaxis]
    p = (q.T @ q).ravel()
    if p > 4:
        warn('Warning: quat2rot: quaternion greater than 1')

    alpha = np.sqrt(4 - p)  # w = cos(theta/2)

    R = (1 - 0.5 * p) * np.eye(3) + 0.5 * (q.T * q + alpha * skew_matrix(q.squeeze()))

    return R


def hand_eye_calibration(delta_R_I_imu: List[np.ndarray],
                         delta_R_world_cam: List[np.ndarray],
                         weighted_ls: bool = False) -> np.ndarray:
    """
    :param delta_R_I_imu: List of relative IMU poses
    :param delta_R_world_cam: List of relative camera poses w_R_c(i)^T * w_R_c(j)
    :param weighted_ls: Use weighted least squares
    :return: cam_R_imu rotation from IMU to Camera
    """

    n = len(delta_R_I_imu)  # number of relative poses
    A = []
    B = []
    angle_list = []
    for idx in range(n):
        p_imu = rot2quat(delta_R_I_imu[idx])
        p_cam = rot2quat(delta_R_world_cam[idx])
        angle_list.append(angle_of_rotation_matrix(delta_R_I_imu[idx]))

        A.append(skew_matrix(p_imu + p_cam))
        B.append(p_cam - p_imu)

    # Rotation from camera to gripper is obtained from the set of equations:
    #    skew(Pgij+Pcij) * Pcg_ = Pcij - Pgij
    # Gripper with camera is first moved to M different poses, then the gripper
    # .. and camera poses are obtained for all poses. The above equation uses
    # .. invariances present between each pair of i-th and j-th pose.

    A_ = np.vstack(A)
    B_ = np.concatenate(B)
    # Solve the equation A*p_imu_cam_tag = B
    if not weighted_ls:
        p_cam_imu_tag, residuals, rank, s = np.linalg.lstsq(A_, B_, rcond=None)
        # x = scipy.linalg.solve(A_.T @ A_, A_.T @ B_, assume_a='pos')
        # TODO try from scipy.linalg import cho_factor, cho_solve
        # c, low = cho_factor(A_.T @ A_)
        # x = cho_solve((c, low), A_.T @ B_)
        # Or using QR:  https://boostedml.com/2020/04/solving-full-rank-linear-least-squares-without-matrix-inversion-in-python-and-numpy.html
        # Q,R=np.linalg.qr(A)
        # x_householder=linalg.solve_triangular(R,Q.T.dot(b))
        # r_norm_householder = np.linalg.norm(np.dot(gram,x_householder)-np.dot(A.T,b))
    else:
        angles = np.array(angle_list)
        sample_weight = (angles - min(angles)) / (max(angles) - min(angles))
        sample_weight[sample_weight < 0.1] = 0.1
        sample_weight = np.kron(sample_weight, np.ones((3,)))  # expand to three weights per sample
        linear_regressor = LinearRegression(copy_X=True, fit_intercept=False).fit(A_, B_, sample_weight)
        p_cam_imu_tag = linear_regressor.coef_

    # Obtained non-unit quaternion is scaled back to unit value that
    # .. designates camera-gripper rotation
    p_cam_imu = 2 * p_cam_imu_tag / np.sqrt(1 + (p_cam_imu_tag * p_cam_imu_tag).sum())
    cam_R_imu = quat2rot(p_cam_imu)

    return cam_R_imu


def robust_hand_eye_calibration(delta_R_I_imu: List[np.ndarray],
                                delta_R_world_cam: List[np.ndarray],
                                weighted_ls: bool = False) -> np.ndarray:
    """
    :param delta_R_I_imu: List of relative IMU poses
    :param delta_R_world_cam: List of relative camera poses w_R_c(i)^T * w_R_c(j)
    :param weighted_ls: Use weighted least squares
    :return: cam_R_imu rotation from IMU to Camera
    """

    cam_R_imu = hand_eye_calibration(delta_R_I_imu, delta_R_world_cam,
                                     weighted_ls=weighted_ls)

    p_cam_imu = rot2quat(cam_R_imu)
    n = len(delta_R_I_imu)  # number of relative poses
    lemma3_list = []
    lemma4_list = []
    trace_error_list = []
    angle_error_list = []
    outliers2_idx = []
    for idx in range(n):

        p_imu = rot2quat(delta_R_I_imu[idx])
        p_cam = rot2quat(delta_R_world_cam[idx])

        lemma3 = np.abs(np.dot(p_cam - p_imu, p_cam_imu))
        lemma4 = np.abs(np.cross(p_cam - p_imu, np.cross(p_cam + p_imu, p_cam_imu))).mean()  # L1 norm

        lemma3_list.append(lemma3)
        lemma4_list.append(lemma4)

        AX = delta_R_world_cam[idx] @ cam_R_imu
        XB = cam_R_imu @ delta_R_I_imu[idx]

        trace_error = (AX @ XB.T).trace()
        angle_error = np.linalg.norm(Rotation.from_matrix(AX @ XB.T).as_euler('ZYX', degrees=True))
        trace_error_list.append(trace_error)
        angle_error_list.append(angle_error)
        if trace_error < 2.95 or angle_error >= 10:
            outliers2_idx.append(idx)

    percentileLemma3 = np.percentile(lemma3_list, 95, interpolation='higher')
    percentileLemma4 = np.percentile(lemma4_list, 95, interpolation='higher')
    outliers_idx = [idx for idx, (lemma3, lemma4) in enumerate(zip(lemma3_list, lemma4_list)) if
                    lemma3 > percentileLemma3 or
                    lemma4 > percentileLemma4]

    # recalculate with outliers filtered
    delta_R_I_imu_filtered = []
    delta_R_world_cam_filtered = []
    for idx in range(n):
        if idx not in outliers_idx:
            delta_R_I_imu_filtered.append(delta_R_I_imu[idx])
            delta_R_world_cam_filtered.append(delta_R_world_cam[idx])

    cam_R_imu_robust = hand_eye_calibration(delta_R_I_imu_filtered, delta_R_world_cam_filtered,
                                            weighted_ls=weighted_ls)

    return cam_R_imu_robust


def estimate_camera_imu_orientation(pose_trajectory,
                                    gyro: pd.DataFrame,
                                    gyro_bias: np.ndarray,
                                    t_imu_shift: float = 0.,
                                    imu_rotation_min_thresh: float = 3.,
                                    t_start=0.,
                                    t_end=np.inf) -> np.ndarray:
    """
    Calculates the relative orientation between the IMU to the Camera
    :param pose_trajectory: Camera poses world_T_cam
    :param gyro: Gyro measurements
    :param gyro_bias: vector of gyro biases
    :param t_imu_shift: time gap between IMU and camera timestamps
    :param imu_rotation_min_thresh: Thresh in degrees for minimal rotation required for IMU rotation
    :return: 3x3 rotation matrix of IMU to camera
    """
    t_prev = None
    prev_a_T_c = None
    delta_R_I_imu = []
    delta_R_world_cam = []

    delta_R_imu_list = []
    delta_R_cam_list = []
    t_mid_sample = []
    index_list = []
    angle_sum = 0.
    for idx, (t_cam, a_T_c) in enumerate(zip(pose_trajectory.timestamps, pose_trajectory.poses_se3)):
        if (t_cam < t_start) or (t_cam > t_end):
            continue
        if idx % 2 != 0:
            continue

        if prev_a_T_c is None:
            prev_a_T_c = a_T_c
            t_prev = t_cam
            continue

        delta_R_cam = prev_a_T_c[:3, :3].T @ a_T_c[:3, :3]

        range_idx = (gyro['nav-time [sec]'] >= t_prev + t_imu_shift) & (gyro['nav-time [sec]'] <= t_cam + t_imu_shift)
        if sum(range_idx) == 0:
            continue

        gyro_section = gyro.loc[range_idx]
        idx_prev = max(gyro_section.index.values[0] - 1, 0)
        w_prev = gyro.iloc[idx_prev][['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values
        t_g_prev = gyro.iloc[idx_prev]['nav-time [sec]']
        q0 = Quaternion()  # Identity quaternion. wxyz format
        q = deepcopy(q0)
        for index, data in gyro_section.iterrows():
            w = data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values
            t_imu = data['nav-time [sec]']
            dt = t_imu - t_g_prev
            q = quat_integration_step_rk4(q, w_prev - gyro_bias, w - gyro_bias, dt)
            t_g_prev = t_imu
            w_prev = w

        q_imu = q

        aa_imu_integrated = Rotation.from_matrix(q_imu.rotation_matrix).as_rotvec()
        angle_imu_integrated = np.rad2deg(np.linalg.norm(aa_imu_integrated))
        if angle_imu_integrated > imu_rotation_min_thresh:
            delta_R_I_imu.append(delta_R_cam)
            delta_R_world_cam.append(q_imu.rotation_matrix)
            angle_sum += angle_imu_integrated
            index_list.append(idx)

        delta_R_imu_list.append(delta_R_cam)
        delta_R_cam_list.append(q_imu.rotation_matrix)
        t_mid_sample.append(0.5 * (t_cam + t_prev))

        prev_a_T_c = a_T_c
        t_prev = t_cam
        idx += 1

    #######################
    # Estimate rotation   #
    #######################
    cam_R_imu_est = robust_hand_eye_calibration(delta_R_I_imu, delta_R_world_cam)

    return cam_R_imu_est, delta_R_imu_list, delta_R_cam_list, t_mid_sample
