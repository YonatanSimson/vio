import cv2 as cv
import numpy as np
import os
import sophus as sp
import matplotlib.pyplot as plt

from evo.core.trajectory import PoseTrajectory3D
from readers.sightec_data_reader import DataReader
from readers.slam_poses_reader import read_slam_poses
from utils.common import stereo_triangulation_midpoint, midpoint_triangulate

"""
Use tracker
    tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'GOTURN', 'MOSSE', 'CSRT']
    tracker_type = tracker_types[2]

    if int(minor_ver) < 3:
        tracker = cv2.Tracker_create(tracker_type)
    else:
        if tracker_type == 'BOOSTING':
            tracker = cv2.TrackerBoosting_create()
        if tracker_type == 'MIL':
            tracker = cv2.TrackerMIL_create()
        if tracker_type == 'KCF':
            tracker = cv2.TrackerKCF_create()
        if tracker_type == 'TLD':
            tracker = cv2.TrackerTLD_create()
        if tracker_type == 'MEDIANFLOW':
            tracker = cv2.TrackerMedianFlow_create()
        if tracker_type == 'GOTURN':
            tracker = cv2.TrackerGOTURN_create()
        if tracker_type == 'MOSSE':
            tracker = cv2.TrackerMOSSE_create()
        if tracker_type == "CSRT":
            tracker = cv2.TrackerCSRT_create()
"""
if __name__ == "__main__":

    start_idx = 1355
    num_of_frames = 200

    run_folder = 'run_#19'
    input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#21")

    reader = DataReader(input_folder)

    image_filename = f'{input_folder}/REC/{start_idx:06d}.jpeg'
    indexes, poses = read_slam_poses(os.path.join(output_folder, "r_t_slam.csv"))
    reader.image_ts.set_index(['index']).loc[indexes, 'ts_sec']
    timestamps = reader.image_ts.set_index(['index']).loc[indexes, 'ts_sec']
    pose_trajectory = PoseTrajectory3D(poses_se3=poses, timestamps=timestamps)

    img0 = cv.imread(image_filename, 0)
    rgb = cv.cvtColor(img0, cv.COLOR_GRAY2BGR)

    # p0 = np.array([316, 532])
    # p1 = np.array([314, 554])
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(rgb)

    coords = np.array([(578.0619162119671, 499.72327621513784),
                       (581.2327026991319, 489.41822013185185)])
    if len(coords) != 2:
        coords = []

        def onclick(event):
            global ix, iy
            ix, iy = event.xdata, event.ydata
            print('x = %d, y = %d' % (ix, iy))

            global coords
            coords.append((ix, iy))

            if len(coords) == 2:
                fig.canvas.mpl_disconnect(cid)
                plt.close(1)
            return coords
        cid = fig.canvas.mpl_connect('button_press_event', onclick)
        plt.show()

    p0 = np.array(coords[0])
    p1 = np.array(coords[1])

    points = np.vstack([p0, p1])
    rgb = cv.line(rgb, p0.astype(np.int), p1.astype(np.int), (0, 0, 255), thickness=1, lineType=cv.LINE_AA)

    plt.figure()
    plt.imshow(rgb)

    # cv.imshow("tracking", rgb)
    # cv.waitKey(1)

    cv.namedWindow("tracking")
    lines = []
    idx0 = np.searchsorted(indexes, start_idx)
    pose0 = pose_trajectory.poses_se3[idx0]
    for idx in range(start_idx + 1, start_idx + num_of_frames):
        image_filename = f'/home/yonatan/data/Hod-Hasharon/07_12_21/run_#19/REC/{idx:06d}.jpeg'
        img1 = cv.imread(image_filename, 0)

        idx1 = np.searchsorted(indexes, idx)
        pose1 = pose_trajectory.poses_se3[idx1]

        # Now do LK between rig_frames  #######
        # Parameters for lucas kanade optical flow
        lk_params = dict(winSize=(11, 11),
                         maxLevel=5,
                         criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 20, 0.03))

        # Create some random colors
        color = np.random.randint(0, 255, (100, 3))
        # Create a mask image for drawing purposes
        h, w = img0.shape
        mask = np.zeros((h, w, 3), dtype=np.uint8)

        # Calculate optical flow
        points0 = points.reshape(-1, 1, 2).astype(np.float32)
        points1, st, err = cv.calcOpticalFlowPyrLK(img0, img1, points0, None, **lk_params)
        points0r, _st, _err = cv.calcOpticalFlowPyrLK(img1, img0, points1, None, **lk_params)

        points1 = points1.squeeze()

        mid_points = stereo_triangulation_midpoint(points0.squeeze(),
                                                   points1,
                                                   pose0,
                                                   pose1,
                                                   reader.camera.K,
                                                   reader.camera.dist_coeffs)
        p0 = points1[0, :].astype(np.int)
        p1 = points1[1, :].astype(np.int)
        rgb = cv.cvtColor(img1, cv.COLOR_GRAY2BGR)
        rgb = cv.line(rgb, p0, p1, (0, 0, 255), thickness=1, lineType=cv.LINE_AA)

        d = np.linalg.norm(points0r.squeeze() - points0.squeeze(), axis=0)
        good = d < 1.5
        if ~good.all():
            break

        lines.append(points.squeeze())
        points = np.copy(points1)
        img0 = img1.copy()

        cv.imshow("tracking", rgb)
        cv.waitKey(100)

    lines_unprojected = []
    # unprojectpoints
    for line in lines:
        line_unprojected = reader.camera.unproject(line)
        lines_unprojected.append(line_unprojected)

    poses = pose_trajectory.poses_se3
    world_T_cam = poses[0]
    R = world_T_cam[:3, :3]
    U, D, Vt = np.linalg.svd(R)
    world_T_cam[:3, :3] = U @ Vt
    reader.camera.set_pose_world2rig(sp.SE3(world_T_cam))

    # Select good points
    p2d_good_new = p1[good]
    p2d_good_old = p0[good]
    # draw the tracks
    for i, (new, old) in enumerate(zip(p2d_good_new, p2d_good_old)):
        a, b = new.astype(np.int32).ravel()
        c, d = old.astype(np.int32).ravel()
        mask = cv.line(mask, (a, b), (c, d), color[i].tolist(), 2, cv.LINE_AA)
        rgb = cv.circle(rgb, (a, b), 5, color[i].tolist(), -1, cv.LINE_AA)

    img = cv.add(rgb, mask)

    # undistorted_p0 = cv.undistortPoints(np.expand_dims(points0, axis=1), reader.rig.cameras[0].K, reader.rig.cameras[0].dist_coeffs)
    # undistorted_p1 = cv.undistortPoints(np.expand_dims(points1, axis=1), reader.rig.cameras[1].K, reader.rig.cameras[1].dist_coeffs)
    print("Done")
