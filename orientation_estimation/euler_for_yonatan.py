import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd

from scipy.spatial.transform import Rotation

from evo.core.trajectory import PoseTrajectory3D
from readers.sightec_data_reader import DataReader
from readers.slam_poses_reader import read_slam_poses

from hand_eye import estimate_camera_imu_orientation


if __name__ == "__main__":
    # Filter coefficient
    alpha = 0.99  #  0.98
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = False

    results_R = {
        'run_#19': os.path.join('/home/yonatan/output/complemetary_filter', "run_#21"),
        'run_#15': os.path.join('/home/yonatan/output/complemetary_filter', "run_#22"),
        'run_#16': os.path.join('/home/yonatan/output/complemetary_filter', "run_#23"),
        'run_#17': os.path.join('/home/yonatan/output/complemetary_filter', "run_#24"),
        'run_#14': os.path.join('/home/yonatan/output/complemetary_filter', "run_#26")
    }

    df = pd.DataFrame(columns=["yaw", "pitch", "roll"])
    for run_name, output_folder in results_R.items():
        cam_R_imu = np.loadtxt(os.path.join(output_folder, 'cam_R_imu.txt'))
        euler = Rotation.from_matrix(cam_R_imu).as_euler('zyx', degrees=True)
        df.loc[run_name] = euler

    df.to_csv(os.path.join('/home/yonatan/output/complemetary_filter', "imu_to_cam.csv"))
    print("Done")
