import os
from functools import partial
from multiprocessing import Pool, cpu_count

import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json
from warnings import warn
from scipy.spatial.transform import Rotation
from tqdm import tqdm

from utils.apriltag_pose import AprilTagPose
from orientation_estimation.complementary_filter import ComplementaryFilter
from orientation_estimation.complementary_filter_quaternion import ComplementaryFilterQ
from readers.sightec_data_reader import DataReader


def warp_points(points: np.ndarray, H: np.ndarray) -> np.ndarray:
    """
    Warp points from a 2d image to another 2d image
    :param points: Nx2 Numpy array. pixel coordinates in xy
    :param H: 3x3 Numpy array. Homography matrix
    :return:
    """
    number_of_points = len(points)
    points_homogenous = np.vstack((points.T, np.ones((1, number_of_points))))

    points_target = H @ points_homogenous
    points_target = points_target / points_target[-1, :]

    return points_target[:2, :].T


def warp_single_image(input, output_images_folder, debug, time_imu_stamps, K, cam_R_imu, april_detector):
    ts_clk, frame_index, image_ts_sec, image_filename = input

    img = cv.imread(image_filename, 0)
    euler_angles_gt = np.array([np.nan, np.nan, np.nan])
    if img is None:
        warn(f"Failed to read image: {image_filename}")
        return image_ts_sec, np.array([np.nan, np.nan, np.nan]), euler_angles_gt

    comp_idx = np.searchsorted(time_imu_stamps, image_ts_sec, side='left')

    if not (time_imu_stamps.min() <= image_ts_sec <= time_imu_stamps.max()):
        return image_ts_sec, np.array([np.nan, np.nan, np.nan]), euler_angles_gt

    closest_idx = min(comp_idx + 1, len(time_imu_stamps) - 1)
    if image_ts_sec - time_imu_stamps[comp_idx] < time_imu_stamps[closest_idx] - image_ts_sec:
        closest_idx = comp_idx

    phi, theta = roll_pitch[closest_idx]  # roll, pitch
    imu_R_w = comp_filter.world_to_body(-phi, -theta)

    cam_R_w = cam_R_imu @ imu_R_w
    euler_angles = Rotation.from_matrix(cam_R_w).as_euler('XYZ', degrees=True)
    # R_yaw0 = Rotation.from_euler('z', euler_angles[0], degrees=True).as_matrix()  # Cancel Yaw
    # cam_R_I0 = cam_R_I @ R_yaw0
    # euler_angles_wo_yaw = Rotation.from_matrix(cam_R_I0).as_euler('ZYX', degrees=True)
    # euler_angles[0] = -euler_angles[0]
    # euler_angles[1] = -euler_angles[1]
    rpy_camera = euler_angles.copy()
    euler_angles[-1] = 0
    euler_angles = np.clip(euler_angles, -25, 25)
    cam_R_I_ = Rotation.from_euler('XYZ', euler_angles, degrees=True).as_matrix()
    roll, pitch, yaw = euler_angles
    # t = flight_height * np.array([np.tan(alpha), 0, -(1 - np.cos(alpha)) / np.cos(alpha)])
    # t = flight_height * np.array([np.tan(alpha), 0, 0])
    # n = np.array([0, 0, 1])
    if debug:
        image_name = os.path.basename(image_filename).split(".")[0]
        image_path = os.path.join(output_images_folder, f"{image_name}.jpeg")
        # if os.path.isfile(image_path):
        #     return image_ts_sec, rpy_camera
        if april_detector is not None:
            rgb = cv.cvtColor(img, cv.COLOR_GRAY2BGR)
            image_points, object_points = april_detector.detect_grid(img)
            # Camera pose w_T_c
            if image_points is not None:
                pos, a_R_cam = april_detector.calculate_grid_pose(image_points, object_points)
            else:
                pos, a_R_cam = None, None
            # April tag rotate to world (NED)
            a_R_w = Rotation.from_euler('XYZ', [180, 0, -90], degrees=True).as_matrix().T
            if a_R_cam is not None:
                cam_R_world_gt = a_R_cam.T @ a_R_w
                euler_angles_gt = Rotation.from_matrix(cam_R_world_gt).as_euler('XYZ', degrees=True)

            # for point in image_points:
            #     a, b = np.round(point).astype(np.int32)
            #     rgb = cv.circle(rgb, (a, b), 5, (255, 0, 0), 1, cv.LINE_AA)
            # Tilt correction
            # plt.figure()
            # plt.imshow(rgb)

        H = K @ cam_R_I_.T @ np.linalg.inv(K)
        # H = np.linalg.inv(H)
        H /= H[2, 2]

        h, w = img.shape

        # Calculate where the pixels will land on target
        tl_source = [0, 0]
        tr_source = [w - 1, 0]
        bl_source = [0, h - 1]
        br_source = [w - 1, h - 1]
        test_labels = ["top left", "top right", "bottom right", "bottom left"]
        points_source = np.vstack((tl_source, tr_source, br_source, bl_source))
        points_target = warp_points(points_source, H)

        # points_target_augmented = np.vstack((points_target, points_target[0, :]))
        # plt.figure()
        # plt.plot(points_target_augmented[:, 0], points_target_augmented[:, 1])
        # plt.gca().invert_yaxis()
        # for label, pix_coordinates in zip(test_labels, points_target):
        #     plt.text(pix_coordinates[0], pix_coordinates[1], label, fontsize=6)

        max_width = int(points_target[:, 0].max() - points_target[:, 0].min() + 1)  # round up
        max_height = int(points_target[:, 1].max() - points_target[:, 1].min() + 1)  # round up

        dsize = (max_width, max_height)
        min_x, min_y = points_target.min(axis=0)
        max_x, max_y = points_target.max(axis=0)
        if min_x < 0:
            offset_x = int(-min_x + 1)
            points_target[:, 0] += offset_x
        if min_y < 0:
            offset_y = int(-min_y + 1)
            points_target[:, 1] += offset_y
        if max_x > max_width - 1:
            offset_x = int(max_x - max_width + 1 + 0.5)
            points_target[:, 0] -= offset_x
        if max_y > max_height - 1:
            offset_y = int(max_y - max_height + 1 + 0.5)
            points_target[:, 1] -= offset_y

        # Warp to BEV (Birds Eye View)
        # Using target to source because of WARP_INVERSE_MAP
        H_image, _ = cv.findHomography(points_target, points_source)
        img_warped = cv.warpPerspective(img, H_image, dsize, flags=cv.WARP_INVERSE_MAP)

        # export to disk for analyzing
        fig, axes = plt.subplots(1, 2, figsize=(16, 8))
        axes[0].imshow(img, cmap='gray')
        axes[0].set_title(image_name)
        axes[1].imshow(img_warped, cmap='gray')
        axes[1].set_title(f"Warped: roll, pitch - ({roll:0.2f}, {pitch:0.2f}) ")
        plt.tight_layout()
        plt.savefig(image_path)
        plt.close(fig)

    return image_ts_sec, rpy_camera, euler_angles_gt


if __name__ == "__main__":
    alpha = 0.999
    fov = 100
    parallel = False
    debug = True
    skipped_frames = 2
    flight_height = 100
    tag_cols = 6
    tag_rows = 6
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    has_april_tag = True
    # run_folder = 'run_#19'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#21")
    # run_folder = 'run_#33'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/04_02_2022', run_folder)

    input_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#38"
    output_folder = input_folder

    default_intrinsics = os.path.join(input_folder, 'basler_daa1920-160um_kalibr.json')
    extrinsics_file_name = os.path.join(input_folder, 'cam_imu_config.json')
    reader = DataReader(input_folder,
                        camera_intrinsics_config=default_intrinsics,
                        rig_extrinsics_config='cam_imu_config.yaml')
    K = reader.camera.K
    try:
        # cam_R_imu = np.loadtxt(os.path.join(output_folder, 'cam_R_imu.txt'))

        with open(extrinsics_file_name) as f:
            data = json.load(f)
        cam_T_imu = np.array(data["T_cam_imu"]).reshape((4, 4))
        cam_R_imu = cam_T_imu[:3, :3]
    except Exception as e:
        print(e)
        cam_R_imu = np.array([[0.0, -1.0, 0.0],
                              [1.0,  0.0, 0.0],
                              [0.0,  0.0, 1.0]])

    image_list = reader.get_image_list()

    roll_pitch_imu_csv = os.path.join(output_folder, f"roll_pitch_imu.csv")
    comp_filter = ComplementaryFilter(alpha=alpha, cam_R_imu=cam_R_imu)
    comp_filter_q = ComplementaryFilterQ(alpha=alpha)
    # time_imu_stamps_, roll_pitch_ = comp_filter_q.run(reader, gyb=np.zeros((3,)), acb=np.zeros((3,)))
    if not os.path.isfile(roll_pitch_imu_csv):
        time_imu_stamps, roll_pitch = comp_filter.run(reader, gyb=np.zeros((3,)), acb=np.zeros((3,)))
        df = pd.DataFrame({"time": time_imu_stamps, "roll": roll_pitch[:, 0], "pitch": roll_pitch[:, 1]})
        df.to_csv(roll_pitch_imu_csv)
    else:
        df = pd.read_csv(roll_pitch_imu_csv, index_col=[0])
        time_imu_stamps = df["time"]
        roll_pitch = df.loc[:, ["roll", "pitch"]].values

    plt.figure()
    plt.plot(time_imu_stamps, np.rad2deg(np.unwrap(roll_pitch[:, 0])), label='roll')
    plt.plot(time_imu_stamps, np.rad2deg(np.unwrap(roll_pitch[:, 1])), label='pitch')
    plt.ylabel("[deg]")
    plt.xlabel("[sec]")
    plt.ylim(-2 * 180, 2 * 180)
    plt.legend()
    plt.title("Complementary filter output")

    plt.switch_backend('Agg')  # comment out if you wish to debug problems with plots

    output_images_folder = os.path.join(output_folder, "images_v1")

    if not os.path.isdir(output_images_folder):
        os.makedirs(output_images_folder)

    if has_april_tag:
        april_detector = AprilTagPose(K=reader.camera.K,
                                      dist_coeffs=reader.camera.dist_coeffs,
                                      tag_cols=tag_cols,
                                      tag_rows=tag_rows,
                                      tag_size=tag_size,
                                      tag_spacing_ratio=tag_spacing_ratio,
                                      min_detected_tags=18,
                                      projection_threshold=3.)
    else:
        april_detector = None

    warp_single_image_partial = partial(warp_single_image,
                                        output_images_folder=output_images_folder,
                                        debug=debug,
                                        time_imu_stamps=time_imu_stamps,
                                        K=K,
                                        cam_R_imu=cam_R_imu,
                                        april_detector=april_detector)

    if parallel:
        pool = Pool(cpu_count() - 1)
        results = pool.map(warp_single_image_partial, image_list[::skipped_frames])
    else:
        fig, axes = plt.subplots(1, 2, figsize=(16, 8))
        results = []
        for ts_clk, frame_index, ts_sec, image_filename in tqdm(image_list[::skipped_frames], "Warping images using IMU",
                                                                total=len(image_list[::skipped_frames])):
            # if frame_index % 5 != 0:
            #     continue
            res = warp_single_image_partial((ts_clk, frame_index, ts_sec, image_filename))
            results.append(res)
        plt.close(fig)

    # image_ts_sec, rpy_camera, euler_angles_gt
    time_cam_stamps, roll_pitch_yaw, euler_angles_gt = zip(*results)
    roll_pitch_yaw = np.array(roll_pitch_yaw)
    euler_angles_gt = np.array(euler_angles_gt)
    df_cam = pd.DataFrame({"camera_time[sec]": time_cam_stamps,
                           "roll": roll_pitch_yaw[:, 0],
                           "pitch": roll_pitch_yaw[:, 1],
                           "yaw": roll_pitch_yaw[:, 2],
                           "roll_gt": euler_angles_gt[:, 0],
                           "pitch_gt": euler_angles_gt[:, 1]})
    # df_cam.dropna(axis=0, inplace=True)
    roll_pitch_cam_csv = os.path.join(output_folder, f"roll_pitch_cam.csv")
    df_cam.to_csv(roll_pitch_cam_csv, index=False)
