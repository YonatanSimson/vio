import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == "__main__":
    input_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#38"
    output_folder = input_folder
    roll_pitch_cam_csv = os.path.join(output_folder, f"roll_pitch_cam.csv")

    df = pd.read_csv(roll_pitch_cam_csv)

    ts = df['camera_time[sec]']
    roll = df['roll']
    pitch = df['pitch']
    roll_gt = df['roll_gt']
    pitch_gt = df['pitch_gt']
    fig, axes = plt.subplots(2, 1)
    axes[0].plot(ts, roll, label="roll")
    axes[0].plot(ts, roll_gt, label="roll_gt")
    axes[0].set_xlabel("[sec]")
    axes[0].set_ylabel("[deg]")
    axes[0].legend()

    axes[1].plot(ts, pitch_gt, label="pitch_gt")
    axes[1].plot(ts, pitch, label="pitch")
    axes[1].set_xlabel("[sec]")
    axes[1].set_ylabel("[deg]")
    axes[1].legend()

    df['roll_err'] = np.abs(roll - roll_gt)
    df['pitch_err'] = np.abs(pitch - pitch_gt)
    df.describe(percentiles=[0.05, 0.5, 0.95])

    fig, axes = plt.subplots(2, 1)
    axes[0].plot(ts, np.abs(roll - roll_gt), label="roll_err")
    axes[0].set_xlabel("[sec]")
    axes[0].set_ylabel("[deg]")
    axes[0].legend()

    axes[1].plot(ts, np.abs(pitch - pitch_gt), label="pitch_err")
    axes[1].set_xlabel("[sec]")
    axes[1].set_ylabel("[deg]")
    axes[1].legend()
    plt.tight_layout()

