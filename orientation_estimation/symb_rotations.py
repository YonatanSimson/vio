import numpy as np
from pyquaternion import Quaternion
from functools import partial
from typing import Tuple, List

from scipy.spatial.transform import Rotation
import sympy as sym


def q_conjagate(q: np.ndarray) -> np.ndarray:
    q[1] = -q[1]
    q[2] = -q[2]
    q[3] = -q[3]

    return q


def q_multiplication(q: np.ndarray, p: np.ndarray) -> np.ndarray:
    return sym.Array([q[0] * p[0] - q[1] * p[1] - q[2] * p[2] - q[3] * p[3],
                      q[1] * p[0] + q[0] * p[1] + q[2] * p[3] - q[3] * p[2],
                      q[2] * p[0] + q[0] * p[2] + q[3] * p[1] - q[1] * p[3],
                      q[3] * p[0] + q[0] * p[3] + q[1] * p[2] - q[2] * p[1]])


def calculate_cost(q_: np.ndarray, q_t: np.ndarray, q_s: np.ndarray) -> float:
    """

    :param q_: Rotation from source to target frame
    :param q_t: Relative rotation of target frame
    :param q_s: Relative rotation of source frame
    :return: MSE
    """
    #
    err = Quaternion() - calculate_func(q_, q_t, q_s)
    return 0.5 * err.elements * err.elements


def calculate_euler_error(q_: Quaternion, q_t: Quaternion, q_s: Quaternion) -> float:
    """

    :param q_: Rotation from source to target frame
    :param q_t: Relative rotation of target frame
    :param q_s: Relative rotation of source frame
    :return: MSE error of euler angles in degrees
    """
    p_tag = q_ * q_s * q_.conjugate
    euler_error = Rotation.from_matrix(q_t.rotation_matrix.T @ p_tag.rotation_matrix).as_euler('zyx', degrees=True)

    return np.sqrt((euler_error * euler_error).mean())


def calculate_func(q_: np.ndarray, q_t: np.ndarray, q_s: np.ndarray):
    """
    :param q_: Rotation from source to target frame
    :param q_t: Relative rotation of target frame
    :param q_s: Relative rotation of source frame
    :return:
    """
    q = Quaternion(q_)
    p = Quaternion(q_s)
    G = Quaternion(q_t).conjugate * q * p * q.conjugate
    # G = Quaternion(q_t) - q * p * q.conjugate

    return G.elements


def jacobian(q_: np.ndarray, q_t: Quaternion, q_s: np.ndarray):
    """
    J_G = d/dq(q_t^-1 * q * p * q.conjugate)
    :param q_: Rotation quaternion
    :param q_t: Relative rotation of target frame
    :param q_s: Source rotation
    :return:
    """
    p = Quaternion(q_s)
    q = Quaternion(q_)

    # See: https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Differentiation_with_respect_to_the_rotation_quaternion
    # Also see: https://math.stackexchange.com/a/2713077/351327
    i = Quaternion([0, 1, 0, 0])
    j = Quaternion([0, 0, 1, 0])
    k = Quaternion([0, 0, 0, 1])

    pq = p * q
    pqi = pq * i
    pqj = pq * j
    pqk = pq * k
    dp_tag_dq0 = pq - pq.conjugate
    dp_tag_dqi = pqi.conjugate - pqi
    dp_tag_dqj = pqj.conjugate - pqj
    dp_tag_dqk = pqk.conjugate - pqk

    dR_dq = np.column_stack([dp_tag_dq0.elements, dp_tag_dqi.elements, dp_tag_dqj.elements, dp_tag_dqk.elements])

    J_g = Quaternion(q_t).conjugate * dR_dq

    return J_g


def gradient_descent(q_cam_imu0: Quaternion,
                     q_cam: Quaternion,
                     q_imu: Quaternion,
                     mu: float = 0.001,
                     der_tol: float = 1e-3) -> Tuple[Quaternion, float]:
    """
    :param q_cam_imu0: Initial guess of imu to camera rotation
    :param q_cam: Relative rotation of Cam (from differentiating the rotation of vision_world_R_cam)
    :param q_imu: Relative rotation of IMU (from integrating the gyro)
    :param mu: Descent rate
    :return: q_cam_imu estimate and cost in euler angles
    """
    x0 = q_cam_imu0.elements  # xyzw
    func = partial(calculate_func, q_t=q_cam, q_s=q_imu)
    cost = partial(calculate_cost, q_t=q_cam, q_s=q_imu)
    jac = partial(jacobian, q_s=q_imu)

    v = x0
    v = v / np.linalg.norm(v)
    prev_err = np.finfo(float).max
    for i in range(10000):
        der = jac(v).T @ func(v)  # -Gradient of 0.5 * G^T * G

        # TODO: replace mu with Armijo rule: https://en.wikipedia.org/wiki/Wolfe_conditions
        v_temp = v - mu * der
        v_temp /= np.linalg.norm(v)
        err_temp = cost(v_temp)
        if prev_err < err_temp:
            # in cases of overshoot
            mu /= 2.
            if mu < 1e-6:
                break

        v = v_temp
        prev_err = err_temp

        if np.linalg.norm(der) <= der_tol:
            break

    return Quaternion(v), calculate_euler_error(Quaternion(v), q_cam, q_imu)


if __name__ == "__main__":

    q0, q1, q2, q3 = sym.symbols('q0 q1 q2 q3')  # unknown rotation
    s0, s1, s2, s3 = sym.symbols('s0 s1 s2 s3')  # source rotation
    t0, t1, t2, t3 = sym.symbols('t0 t1 t2 t3')  # target rotation

    q = sym.symarray('q', 4)
    s = sym.symarray('s', 4)
    t = sym.symarray('t', 4)

    q_star = q_conjagate(q)

    print(q_star)


    print("Done")



