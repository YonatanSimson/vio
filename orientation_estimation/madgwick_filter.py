from tqdm import tqdm
from typing import Tuple, Optional
import numpy as np
import copy

from scipy.spatial.transform import Rotation
from pyquaternion import Quaternion
from ahrs.utils.wmm import WMM

"""
# Equations from internal report: https://x-io.co.uk/downloads/madgwick_internal_report.pdf
# Reference code: https://github.com/Mayitzin/ahrs/blob/master/ahrs/filters/madgwick.py
"""


class MadgwickFilter(object):
    def __init__(self,
                 longitude: float = 34.884360,
                 latitude: float = 32.132910,
                 altitude: float = 100.0,
                 use_magnometer: bool = True,
                 fix_to_true_north: bool = True) -> None:
        self.q = Quaternion()  # q_ned_imu rotation from local to global. i.e. from IMU to NED world frame
        self.wmm = None
        self.longitude = longitude
        self.latitude = latitude
        self.altitude = altitude
        self.wmm = WMM()
        self.wmm.magnetic_field(longitude, latitude, height=altitude)
        self.declination_rad = np.deg2rad(self.wmm.D)  # in radians
        if fix_to_true_north:
            self.q_declination = Quaternion(axis=np.array([0, 0, 1]),
                                            angle=-self.declination_rad)
        else:
            self.q_declination = Quaternion()
        self.use_magnometer = use_magnometer
        self.gain = 0.041 if use_magnometer else 0.033
        gyro_meas_drift = np.pi * 0.2 / 180.0  # Implementation
        self.zeta = np.sqrt(3.0 / 4.0) * gyro_meas_drift
        self.gyro_bias = np.zeros((3,), dtype=np.float64)

    def init(self, a0: np.ndarray, m0: np.ndarray):
        # Gravity
        ax, ay, az = a0
        pitch_init = -np.arctan2(-ax, np.sqrt(ay * ay + az * az))
        roll_init = -np.arctan2(ay, -az)

        # Get initial yaw from magnometer
        m = m0 / np.linalg.norm(m0)
        R_world_imu = Rotation.from_euler('ZYX', [0, pitch_init, roll_init]).as_matrix()

        # Transform magnetic field to world. Correct tilt
        h = R_world_imu @ m

        # Magnetic
        yaw_init = -np.arctan2(h[1], h[0])

        q = Rotation.from_euler('ZYX', [yaw_init, pitch_init, roll_init]).as_quat()  # xyzw
        q0 = Quaternion(real=q[3], imaginary=q[:3])

        self.q = copy.deepcopy(q0)

    def update(self, accel: np.ndarray, gyro: np.ndarray, mag: np.ndarray, dt: float):

        a_norm = np.linalg.norm(accel)

        a = accel / a_norm
        m = mag / np.linalg.norm(mag)
        # Rotate normalized magnetometer measurements
        q_mag = Quaternion(imaginary=m)
        # Transform magnetic field to world
        h = self.q * q_mag * self.q.conjugate  # (eq. 45)
        bx = np.linalg.norm([h[1], h[2]])  # (eq. 46)
        bz = h[3]

        qw, qx, qy, qz = self.q.elements / self.q.norm  # wxyz
        # gravity = [0, 0, -1] instead of [0, 0, 1] as in paper and reference code
        if self.use_magnometer:
            # Objective function (eq. 31) - Using NED world as reference unlike in the paper
            f = np.array([-2.0 * (qx * qz - qw * qy) - a[0],
                          -2.0 * (qw * qx + qy * qz) - a[1],
                          -2.0 * (0.5 - qx ** 2 - qy ** 2) - a[2],
                          2.0 * bx * (0.5 - qy ** 2 - qz ** 2) + 2.0 * bz * (qx * qz - qw * qy) - m[0],
                          2.0 * bx * (qx * qy - qw * qz) + 2.0 * bz * (qw * qx + qy * qz) - m[1],
                          2.0 * bx * (qw * qy + qx * qz) + 2.0 * bz * (0.5 - qx ** 2 - qy ** 2) - m[2]])
            # Jacobian (eq. 32)
            J = np.array([[2.0 * qy, -2.0 * qz, 2.0 * qw, -2.0 * qx],
                          [-2.0 * qx, -2.0 * qw, -2.0 * qz, -2.0 * qy],
                          [0.0, 4.0 * qx, 4.0 * qy, 0.0],
                          [-2.0 * bz * qy, 2.0 * bz * qz, -4.0 * bx * qy - 2.0 * bz * qw,
                           -4.0 * bx * qz + 2.0 * bz * qx],
                          [-2.0 * bx * qz + 2.0 * bz * qx, 2.0 * bx * qy + 2.0 * bz * qw, 2.0 * bx * qx + 2.0 * bz * qz,
                           -2.0 * bx * qw + 2.0 * bz * qy],
                          [2.0 * bx * qy, 2.0 * bx * qz - 4.0 * bz * qx, 2.0 * bx * qw - 4.0 * bz * qy, 2.0 * bx * qx]])
        else:
            # Objective function (eq. 25)
            f = np.array([-2.0 * (qx * qz - qw * qy) - a[0],
                          -2.0 * (qw * qx + qy * qz) - a[1],
                          -2.0 * (0.5 - qx ** 2 - qy ** 2) - a[2]])
            # Jacobian (eq. 26)
            J = np.array([[2.0 * qy, -2.0 * qz, 2.0 * qw, -2.0 * qx],
                          [-2.0 * qx, -2.0 * qw, -2.0 * qz, -2.0 * qy],
                          [0.0, 4.0 * qx, 4.0 * qy, 0.0]])

        # Compute the gradient
        gradient = J.T @ f  # (eq. 34)
        gradient /= np.linalg.norm(gradient)  # wxyz

        # Calculate gyro bias
        # if self.use_magnometer:
        #     # The direction of the error of q_dot (eq. 44)
        #     q_err_dot = Quaternion(gradient)
        #
        #     # Compute angular estimated direction of the gyroscope error
        #     q_omega_err = 2 * (self.q * q_err_dot).elements
        #     w_err = q_omega_err[1:]
        #
        #     # Compute and remove the gyroscope baises (eq. 47)
        #     self.gyro_bias += dt * self.zeta * w_err
        #     gyro -= self.gyro_bias

        q_gyro = Quaternion(imaginary=gyro)
        # Orientation derivative based on gyro
        qDot = 0.5 * self.q * q_gyro  # (eq. 12)
        qDot_dt = Quaternion(dt * (qDot.elements - self.gain * gradient))  # (eq. 33) + (eq 13)

        self.q += qDot_dt  # (eq. 13)
        self.q = self.q.normalised

    def run(self, reader,
            acb: Optional[np.ndarray] = None,
            gyb: Optional[np.ndarray] = None) -> Tuple[np.ndarray, np.array]:
        """

        :param reader:
        :param acb: Accelerometer bias
        :param gyb: Gyro bias
        :return:
        """
        if gyb is None:
            gyb = reader.get_gyro_bias(200)
        if acb is None:
            acb = np.zeros((3,))
        print(f"gyb: {gyb}")
        print(f"acb: {acb}")

        # Get MARG data from reader
        acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
        gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
        mag = reader.imu_data[['MagField x', ' MagField y', ' MagField z [mgauss]']]
        num_samples = len(gyro)

        ts_sec = reader.imu_data['nav-time [sec]']  # IMU timestamps
        dt_imu = np.mean(np.diff(ts_sec))

        t_prev = ts_sec.iloc[0]
        q_list = [self.body_to_world_rotation.elements]
        ts_list = [t_prev]
        for idx in tqdm(range(1, num_samples), "Madgwick filter", total=len(reader.imu_data)):
            ts: float = ts_sec.iloc[idx]
            gyr = gyro.iloc[idx].values
            a = acc.iloc[idx].values
            m = mag.iloc[idx].values

            self.update(a - acb, gyr - gyb, m, dt_imu)

            ts_list.append(ts)
            q_list.append(self.body_to_world_rotation.elements)  # wxyz
            t_prev = ts

        return np.array(ts_list), np.array(q_list)

    @property
    def body_to_world_rotation(self):
        """
        IMU to NED world orientation
        Includes a fix from magnetic north to true north
        :return: Hamilton quaternion in wxyz format
        """
        return self.q_declination * self.q
