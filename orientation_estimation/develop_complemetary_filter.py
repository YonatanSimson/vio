import numpy as np
import os
import matplotlib.pyplot as plt
import yaml
import pandas as pd
from warnings import warn
import scipy

from evo.tools.file_interface import read_euroc_csv_trajectory
from orientation_estimation.complementary_filter import ComplementaryFilter
from readers.sightec_data_reader import DataReader
from utils.apriltag_pose import AprilTagPose

if __name__ == "__main__":
    # Filter coefficient
    alpha = 0.99  #  0.98
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = True
    has_april_tag = True
    show_imu = True

    # run_folder = 'run_#5'
    # input_folder = os.path.join('/home/yonatan/data/imu_basler_calib/26_09_2021', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/imu_basler_calib_26_09_2021', run_folder + '_v14')
    run_folder = 'run_#20'
    input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/22_10_2021', run_folder)
    output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_22_10_2021', run_folder + '_v0')

    # run_folder = 'run_#17'
    # input_folder = os.path.join('/home/yonatan/data/complemetary_filter', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_22_10_2021', run_folder + '_v0')

    # run_folder = 'run_#19'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_07_12_2021', run_folder + '_v0')

    # run_folder = 'run_#15'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = '/home/yonatan/output/complemetary_filter/run_#22/CSV'

    output_gt_pose = os.path.join(input_folder, 'April_WC_gt_.csv')
    has_april_tag_gt = os.path.isfile(output_gt_pose)

    try:
        cam_imu_extrinsics_calibration = os.path.join(input_folder, "imu_camera_extrinsics.yaml")
        with open(cam_imu_extrinsics_calibration) as f:
            imu_cam_calibration = yaml.load(f, Loader=yaml.FullLoader)
        print(imu_cam_calibration)

        imu_calibration_cam0 = imu_cam_calibration['cam0']

        T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])
        R_cam_imu = T_cam_imu[:3, :3]
    except Exception as e:
        print(e)
        warn("Has no Extrinsic calibration!! Rough Estimation instead")
        R_cam_imu = np.array([[0.0, -1.0, 0.0],
                              [-1.0, 0.0, 0.0],
                              [0.0, 0.0, -1.0]])
        T_cam_imu = np.eye(4)
        T_cam_imu[:3, :3] = R_cam_imu

    # world_T_cam poses
    vio_ref_results_path = os.path.join(output_folder, 'WC.csv')
    try:
        rovio_results = read_euroc_csv_trajectory(vio_ref_results_path)
    except Exception as e:
        warn(e.message)
        rovio_results = None
    reader = DataReader(input_folder)

    if has_april_tag:
        april_detector = AprilTagPose(K=reader.camera.K,
                                      dist_coeffs=reader.camera.dist_coeffs,
                                      tag_cols=6,
                                      tag_rows=6,
                                      tag_size=tag_size,
                                      tag_spacing_ratio=tag_spacing_ratio,
                                      min_detected_tags=18,
                                      projection_threshold=3.)

    if show_video:
        image_list = reader.get_image_list()
        april_detector.display_video(image_list)

    if has_april_tag:
        if not has_april_tag_gt:
            image_path_list = reader.get_image_list(use_time=True)
            pose_list = april_detector.estimate_poses(image_path_list)
            april_detector.plot_poses_2d(pose_list)
            april_detector.write_pose_list(pose_list, output_gt_pose)
            pose_trajectory = april_detector.convert_pose_list_to_trajectory(pose_list)
        else:
            pose_trajectory = read_euroc_csv_trajectory(output_gt_pose)

        # Convert from Cam->world to IMU->world
        pose_trajectory.transform(T_cam_imu, right_mul=True)
        cam_fps = 1 / np.median(np.diff(pose_trajectory.timestamps))

        # World to IMU
        pose_trajectory_imu_world = pose_trajectory.invert()

        ts_imu = pose_trajectory.timestamps
        xyz_imu = pose_trajectory.positions_xyz
        euler_imu = pose_trajectory.get_orientations_euler(axes="rzyx")

        fig, axes = plt.subplots(2, 2, sharex=True)
        axes[0, 0].plot(ts_imu, xyz_imu)
        axes[0, 0].set_title("IMU to world XYZ [m]")
        axes[0, 0].legend(['x', 'y', 'z'])
        axes[1, 0].plot(ts_imu, euler_imu)
        axes[1, 0].set_title("IMU to world orientation (euler) [rad]")
        axes[1, 0].legend(['Yaw', 'Pitch', 'Roll'])

        ts_world = pose_trajectory_imu_world.timestamps
        xyz_world = pose_trajectory_imu_world.positions_xyz
        euler_world = pose_trajectory_imu_world.get_orientations_euler(axes="szyx")
        axes[0, 1].plot(ts_world, xyz_world)
        axes[0, 1].set_title("World to IMU XYZ [m]")
        axes[0, 1].legend(['x', 'y', 'z'])
        axes[1, 1].plot(ts_world, euler_world)
        axes[1, 1].set_title("World to IMU orientation (euler) [rad]")
        axes[1, 1].legend(['Yaw', 'Pitch', 'Roll'])
        plt.tight_layout()
    else:
        pose_trajectory = None

    # Deploy complementary filter
    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    # ts_system_clk = acc.index.values
    ts_sec = reader.imu_data['nav-time [sec]']  # reader.convert_system_clk_to_sec(ts_system_clk)

    range_idx = (ts_sec >= 40) & (ts_sec < 50)
    gyro_sub = gyro.loc[range_idx]
    ts_sub = ts_sec[range_idx]

    plt.figure()
    plt.plot(ts_sub, np.rad2deg(gyro_sub))

    plt.figure()
    plt.plot(ts_sub, np.rad2deg(gyro_sub.cumsum()) / 200)
    plt.legend(['roll', 'pitch', 'yaw'])
    # IMU-Camera extrinsics results

    try:
        cam_imu_results = read_euroc_csv_trajectory(os.path.join(output_folder, 'MC.csv'))

        # Visualize IMU
        fig, axes = plt.subplots(2, 3 if show_imu else 2, sharex=False, figsize=(16, 8))

        ts_vio = rovio_results.timestamps
        xyz_rovio = rovio_results.positions_xyz
        euler_rovio = rovio_results.get_orientations_euler(axes='szyx')

        vio_body_results_path = os.path.join(output_folder, 'WB.csv')
        rovio_body_results = read_euroc_csv_trajectory(vio_body_results_path)
        ts_imu = rovio_body_results.timestamps
        xyz_imu = rovio_body_results.positions_xyz
        euler_imu = rovio_body_results.get_orientations_euler(axes='szyx')

        axes[0, 0].plot(ts_vio, xyz_rovio)
        axes[0, 0].set_title("ROVIO CAM XYZ [m]")
        axes[0, 0].legend(['x', 'y', 'z'])
        axes[1, 0].plot(ts_vio, np.unwrap(euler_rovio, axis=0))
        axes[1, 0].set_title("ROVIO CAM orientation (euler) [deg]")
        axes[1, 0].legend(['yaw', 'pitch', 'roll'])

        axes[0, 1].plot(ts_imu, xyz_imu)
        axes[0, 1].set_title("IMU XYZ [m]")
        axes[0, 1].legend(['x', 'y', 'z'])
        axes[1, 1].plot(ts_imu, np.unwrap(euler_imu, axis=0))
        axes[1, 1].set_title("IMU orientation (euler) [deg]")
        axes[1, 1].legend(['yaw', 'pitch', 'roll'])

        if show_imu:
            axes[0, 2].plot(ts_sec, acc.values)
            axes[0, 2].set_title("IMU Accel [m/s2]")
            axes[0, 2].set_ylim(-20., 20.)
            axes[1, 2].plot(ts_sec, gyro.values)
            axes[1, 2].set_title("IMU Angvel [rad/s]")
            axes[1, 2].set_ylim(-np.pi / 2, np.pi / 2)

        plt.tight_layout()

        # Read IMU and run Complementary filter
        df_update_state = pd.read_csv(os.path.join(output_folder, 'state_update_log.csv'), header=0)
        df_update_state.reset_index(inplace=True)
        acc_bias = df_update_state[['acb_x[m/s2]', 'acb_y[m/s2]', 'acb_z[m/s2]']]
        acb = acc_bias.median().values
    except Exception as e:
        warn(e.message)
        acb = np.zeros((3, ))

    comp_filter = ComplementaryFilter(alpha=alpha)
    time_stamps, pitch_roll_ = comp_filter.run(reader)

    n = pitch_roll_.shape[0]

    df = pd.DataFrame({"time": time_stamps, "roll": pitch_roll_[:, 0], "pitch": pitch_roll_[:, 1]})
    df.to_csv(os.path.join(output_folder, f"roll_pitch_{run_folder}.csv"))
    try:
        yaw_gt = euler_imu[:, 0]
        pitch_gt = euler_imu[:, 1] + np.pi
        roll_gt = euler_imu[:, 2] - np.pi

        plt.figure()
        plt.plot(time_stamps, np.unwrap(pitch_roll_[:, 0]), label='roll')
        plt.plot(ts_imu, np.unwrap(roll_gt), label='roll_gt')
        plt.plot(time_stamps, np.unwrap(pitch_roll_[:, 1]), label='pitch')
        plt.plot(ts_imu, np.unwrap(pitch_gt), label='pitch_gt')
        plt.ylabel("[rad]")
        plt.xlabel("[sec]")
        plt.ylim(-2 * np.pi, 2 * np.pi)
        plt.legend()
        plt.title("Complementary filter output")
    except Exception as e:
        print(e)
        warn("Has no GT")

        plt.figure()
        plt.plot(time_stamps, np.unwrap(pitch_roll_[:, 0]), label='roll')
        plt.plot(time_stamps, np.unwrap(pitch_roll_[:, 1]), label='pitch')
        plt.ylabel("[rad]")
        plt.xlabel("[sec]")
        plt.ylim(-2 * np.pi, 2 * np.pi)
        plt.legend()
        plt.title("Complementary filter output")

    # pitch_gt = np.unwrap(euler_imu[:, 1] + np.pi)
    # roll_gt = np.unwrap(euler_imu[:, 2] - np.pi)
    f_pitch = scipy.interpolate.interp1d(time_stamps,  np.unwrap(euler_imu[:, 1] + np.pi),
                                         kind='cubic', fill_value=np.nan, bounds_error=False)
    f_roll = scipy.interpolate.interp1d(time_stamps, np.unwrap(euler_imu[:, 2] - np.pi),
                                        kind='cubic', fill_value=np.nan, bounds_error=False)
    pitch_gt_interp = f_pitch(time_stamps)
    roll_gt_interp = f_roll(time_stamps)
    plt.figure()
    plt.plot(time_stamps, np.rad2deg(roll_gt_interp - np.unwrap(pitch_roll_[:, 0])), label='roll error')
    plt.plot(time_stamps, np.rad2deg(pitch_gt_interp - np.unwrap(pitch_roll_[:, 1])), label='pitch error')
    plt.legend()
    plt.ylabel("[deg]")
    plt.xlabel("[sec]")
    plt.title("Complementary filter error")
    # ax = acc.values[:, 0] - acb[0]
    # ay = acc.values[:, 1] - acb[1]
    # az = acc.values[:, 2] - acb[2]

    # plt.figure()
    # plt.plot(reader.imu_data['nav-time [sec]'].values, np.rad2deg(np.unwrap(-np.arctan2(-ay, az), axis=0)), label="roll")
    # plt.plot(reader.imu_data['nav-time [sec]'].values, np.rad2deg(np.unwrap(-np.arctan2(ax, np.sign(az) * np.sqrt(az * az + ay * ay)), axis=0)), label="pitch")
    # plt.title("Angle from accelerometer")
    df = pd.DataFrame({"roll_error": np.abs(np.rad2deg(roll_gt_interp - np.unwrap(pitch_roll_[:, 0]))),
                       "pitch_error": np.abs(np.rad2deg(pitch_gt_interp - np.unwrap(pitch_roll_[:, 1])))})
    df.dropna(inplace=True)
    plt.show()

