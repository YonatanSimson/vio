from tqdm import tqdm
from typing import Tuple, Optional
import numpy as np
import math
from pyquaternion import Quaternion

from imu_integration.gyro_integration import compute_omega_skew

"""
Source: https://github.com/Mayitzin/ahrs/blob/master/ahrs/filters/complementary.py
Alternative for the future: https://github.com/Mayitzin/ahrs/blob/831d3cbb81aec76bf26f79ede68c84f804889394/ahrs/filters/aqua.py
"""

# map axes strings to/from tuples of inner axis, parity, repetition, frame
_AXES2TUPLE = {
    'sxyz': (0, 0, 0, 0), 'sxyx': (0, 0, 1, 0), 'sxzy': (0, 1, 0, 0),
    'sxzx': (0, 1, 1, 0), 'syzx': (1, 0, 0, 0), 'syzy': (1, 0, 1, 0),
    'syxz': (1, 1, 0, 0), 'syxy': (1, 1, 1, 0), 'szxy': (2, 0, 0, 0),
    'szxz': (2, 0, 1, 0), 'szyx': (2, 1, 0, 0), 'szyz': (2, 1, 1, 0),
    'rzyx': (0, 0, 0, 1), 'rxyx': (0, 0, 1, 1), 'ryzx': (0, 1, 0, 1),
    'rxzx': (0, 1, 1, 1), 'rxzy': (1, 0, 0, 1), 'ryzy': (1, 0, 1, 1),
    'rzxy': (1, 1, 0, 1), 'ryxy': (1, 1, 1, 1), 'ryxz': (2, 0, 0, 1),
    'rzxz': (2, 0, 1, 1), 'rxyz': (2, 1, 0, 1), 'rzyz': (2, 1, 1, 1)}

_TUPLE2AXES = dict((v, k) for k, v in _AXES2TUPLE.items())

# axis sequences for Euler angles
_NEXT_AXIS = [1, 2, 0, 1]


def quaternion_from_euler(ai, aj, ak, axes='sxyz'):
    """Return quaternion from Euler angles and axis sequence.
    ai, aj, ak : Euler's roll, pitch and yaw angles
    axes : One of 24 axis sequences as string or encoded tuple
    >>> q = quaternion_from_euler(1, 2, 3, 'ryxz')
    >>> numpy.allclose(q, [0.310622, -0.718287, 0.444435, 0.435953])
    True
    """
    try:
        firstaxis, parity, repetition, frame = _AXES2TUPLE[axes.lower()]
    except (AttributeError, KeyError):
        _ = _TUPLE2AXES[axes]
        firstaxis, parity, repetition, frame = axes

    i = firstaxis
    j = _NEXT_AXIS[i+parity]
    k = _NEXT_AXIS[i-parity+1]

    if frame:
        ai, ak = ak, ai
    if parity:
        aj = -aj

    ai /= 2.0
    aj /= 2.0
    ak /= 2.0
    ci = math.cos(ai)
    si = math.sin(ai)
    cj = math.cos(aj)
    sj = math.sin(aj)
    ck = math.cos(ak)
    sk = math.sin(ak)
    cc = ci*ck
    cs = ci*sk
    sc = si*ck
    ss = si*sk

    quaternion = np.empty((4, ), dtype=np.float64)
    if repetition:
        quaternion[i] = cj*(cs + sc)
        quaternion[j] = sj*(cc + ss)
        quaternion[k] = sj*(cs - sc)
        quaternion[3] = cj*(cc - ss)
    else:
        quaternion[i] = cj*sc - sj*cs
        quaternion[j] = cj*ss + sj*cc
        quaternion[k] = cj*cs - sj*sc
        quaternion[3] = cj*cc + sj*ss
    if parity:
        quaternion[j] *= -1

    return Quaternion(real=quaternion[-1], imaginary=quaternion[:3])


class ComplementaryFilterQ(object):
    def __init__(self, alpha: float = 0.98) -> None:
        self.alpha = alpha
        self.compAngleX = 0.
        self.compAngleY = 0.
        self.q = Quaternion([1., 0., 0., 0.])  # wxyz

    def init(self, ax, ay, az):
        # Calculate accelerometer angles
        self.q = self.accelerometer_angle(ax, ay, az)

    def attitude_propagation(self, q: Quaternion, omega: np.ndarray, dt: float) -> Quaternion:
        """
        Attitude propagation of the orientation.
        Estimate the current orientation at time :math:`t`, from a given
        orientation at time :math:`t-1` and a given angular velocity,
        :math:`\\omega`, in rad/s.
        It is computed by numerically integrating the angular velocity and
        adding it to the previous orientation.
        .. math::
            \\mathbf{q}_\\omega =
            \\begin{bmatrix}
            q_w - \\frac{\\Delta t}{2} \\omega_x q_x - \\frac{\\Delta t}{2} \\omega_y q_y - \\frac{\\Delta t}{2} \\omega_z q_z\\\\
            q_x + \\frac{\\Delta t}{2} \\omega_x q_w - \\frac{\\Delta t}{2} \\omega_y q_z + \\frac{\\Delta t}{2} \\omega_z q_y\\\\
            q_y + \\frac{\\Delta t}{2} \\omega_x q_z + \\frac{\\Delta t}{2} \\omega_y q_w - \\frac{\\Delta t}{2} \\omega_z q_x\\\\
            q_z - \\frac{\\Delta t}{2} \\omega_x q_y + \\frac{\\Delta t}{2} \\omega_y q_x + \\frac{\\Delta t}{2} \\omega_z q_w
            \\end{bmatrix}
        [1] "Quaternion kinematics for the error-state Kalman filter", Joan Solà. https://arxiv.org/abs/1711.02508
        Parameters
        ----------
        q : numpy.ndarray
            A-priori quaternion.
        omega : numpy.ndarray
            Tri-axial angular velocity, in rad/s.
        dt : float
            Time step, in seconds, between consecutive Quaternions.
        Returns
        -------
        q_omega : numpy.ndarray
            Estimated orientation, as quaternion.
        """
        w = dt * omega
        angle = np.linalg.norm(w)
        q_w = Quaternion(angle=angle, axis=w / angle)
        q_omega = q * q_w  # See [1] eq. 214
        # q_omega = Quaternion(compute_omega_skew(w) @ q.elements)  # See [1] eq. 214
        return q_omega

    def update(self, accel, gyro, dt):
        q_omega = self.attitude_propagation(self.q, gyro, dt)
        q_am = self.accelerometer_angle(*accel)
        # Complementary Estimation
        self.q = Quaternion.slerp(q_omega, q_am, 1 - self.alpha)  # self.alpha * q_omega + (1.0 - self.alpha) * q_am
        self.q.yaw_pitch_roll
        return self.q

    @staticmethod
    def accelerometer_angle(ax: float, ay: float, az: float) -> Quaternion:
        # Angle rotation around by X axis acceleration vector relative to ground
        ex = np.arctan2(ay, -az)  # roll

        # Angle rotation around Y axis acceleration vector relative to ground
        ey = np.arctan2(ax, np.sighn(az) * np.sqrt(ay * ay + az * az))  # pitch

        #qq1 = quaternion_from_euler(ex, ey, 0, axes='sxyz')
        qq2 = Quaternion(quaternion_from_euler(ex, ey, 0, axes='sxyz'))
        cx, sx = np.cos(0.5 * ex), np.sin(0.5 * ex)
        cy, sy = np.cos(0.5 * ey), np.sin(0.5 * ey)
        q_a = Quaternion([cy*cx, cy*sx, sy*cx, -sy*sx])

        return qq2.normalised

    @staticmethod
    def world_to_body(phi, theta):
        """
        Z is up. [1]
        [1] https://en.wikipedia.org/wiki/Davenport_chained_rotations#Tait%E2%80%93Bryan_chained_rotations
        :param phi: Roll angle around x-axis
        :param theta: Pitch around y-axis
        :return: R_phi * R_theta
        """

        # The negative sign is due to world_to_body transform
        R_phi = np.array([[1., 0., 0.],
                          [0., np.cos(-phi), -np.sin(-phi)],
                          [0., np.sin(-phi), np.cos(-phi)]])

        R_theta = np.array([[np.cos(-theta),  0., np.sin(-theta)],
                            [0.,              1., 0.],
                            [-np.sin(-theta), 0., np.cos(-theta)]])

        return R_phi @ R_theta

    def run(self, reader,
            acb: Optional[np.ndarray] = None,
            gyb: Optional[np.ndarray] = None) -> Tuple[np.ndarray, np.array]:
        """

        :param reader:
        :param acb: Accelerometer bias
        :param gyb: Gyro bias
        :return:
        """
        if gyb is None:
            gyb = reader.get_gyro_bias(200)
        if acb is None:
            acb = np.zeros((3, ))
        print(f"gyb: {gyb}")
        print(f"acb: {acb}")
        t_prev = None
        pitch_roll_ = []
        time_stamps = []
        for imu_raw in tqdm(reader.imu_data.iterrows(), "Complementary filter", total=len(reader.imu_data)):
            ind, data = imu_raw
            time_sec = data['nav-time [sec]']

            acc_raw = data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']].values
            gyro_raw = data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values

            [ax, ay, az] = acc_raw - acb

            if t_prev is None:
                self.init(ax, ay, az)
                t_prev = time_sec
                continue
            dt = time_sec - t_prev

            [gx, gy, gz] = gyro_raw - gyb

            # Update complimentary filter
            q_pitch_roll = self.update(np.array([ax, ay, az]), np.array([gx, gy, gz]), dt)

            pitch_roll_.append([comp_x, comp_y])
            time_stamps.append(time_sec)

            t_prev = time_sec

        pitch_roll_ = np.array(pitch_roll_)
        time_stamps = np.array(time_stamps)

        return time_stamps, pitch_roll_
