import numpy as np
import os
import matplotlib.pyplot as plt
import yaml
from warnings import warn

from scipy.spatial.transform import Rotation
from evo.tools.file_interface import read_euroc_csv_trajectory

from hand_eye import estimate_camera_imu_orientation
from readers.sightec_data_reader import DataReader
from utils.apriltag_pose import AprilTagPose


if __name__ == "__main__":
    # Filter coefficient
    alpha = 0.99
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = False
    has_april_tag = True
    show_imu = True

    # run_folder = 'run_#5'
    # input_folder = os.path.join('/home/yonatan/data/imu_basler_calib/26_09_2021', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/imu_basler_calib_26_09_2021', run_folder + '_v14')
    # run_folder = 'run_#20'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/22_10_2021', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_22_10_2021', run_folder + '_v0')

    run_folder = 'run_#17'
    input_folder = os.path.join('/home/yonatan/data/complemetary_filter', run_folder)
    output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_22_10_2021', run_folder + '_v0')

    output_gt_pose = os.path.join(input_folder, 'April_WC_gt_.csv')
    has_april_tag_gt = os.path.isfile(output_gt_pose)

    try:
        cam_imu_extrinsics_calibration = os.path.join(input_folder, "imu_camera_extrinsics.yaml")
        with open(cam_imu_extrinsics_calibration) as f:
            imu_cam_calibration = yaml.load(f, Loader=yaml.FullLoader)
        print(imu_cam_calibration)

        imu_calibration_cam0 = imu_cam_calibration['cam0']

        T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])
        R_cam_imu = T_cam_imu[:3, :3]
    except Exception as e:
        print(e)
        warn("Has no Extrinsic calibration!! Rough Estimation instead")
        R_cam_imu = np.array([[0.0, -1.0, 0.0],
                              [-1.0, 0.0, 0.0],
                              [0.0, 0.0, -1.0]])
        T_cam_imu = np.eye(4)
        T_cam_imu[:3, :3] = R_cam_imu

    # world_T_cam poses
    vio_ref_results_path = os.path.join(output_folder, 'WC.csv')
    try:
        rovio_results = read_euroc_csv_trajectory(vio_ref_results_path)
    except Exception as e:
        warn(e.message)
        rovio_results = None
    reader = DataReader(input_folder)
    april_detector = AprilTagPose(K=reader.camera.K,
                                  dist_coeffs=reader.camera.dist_coeffs,
                                  tag_cols=6,
                                  tag_rows=6,
                                  tag_size=tag_size,
                                  tag_spacing_ratio=tag_spacing_ratio,
                                  min_detected_tags=18,
                                  projection_threshold=3.)

    if show_video:
        image_list = reader.get_image_list()
        april_detector.display_video(image_list)

    if has_april_tag:
        if not has_april_tag_gt:
            image_path_list = reader.get_image_list(use_time=True)
            pose_list = april_detector.estimate_poses(image_path_list)
            april_detector.plot_poses_2d(pose_list)
            april_detector.write_pose_list(pose_list, output_gt_pose)
            pose_trajectory = april_detector.convert_pose_list_to_trajectory(pose_list)
        else:
            pose_trajectory = read_euroc_csv_trajectory(output_gt_pose)

        # World to Cam
        pose_trajectory_cam_world = pose_trajectory.invert()
        cam_fps = 1 / np.median(np.diff(pose_trajectory.timestamps))

        ts_cam = pose_trajectory.timestamps
        xyz_cam = pose_trajectory.positions_xyz
        euler_cam = pose_trajectory.get_orientations_euler(axes="rzyx")

        fig, axes = plt.subplots(2, 2, sharex=True)
        axes[0, 0].plot(ts_cam, xyz_cam)
        axes[0, 0].set_title("CAM to world XYZ [m]")
        axes[0, 0].legend(['x', 'y', 'z'])
        axes[1, 0].plot(ts_cam, euler_cam)
        axes[1, 0].set_title("CAM to world orientation (euler) [rad]")
        axes[1, 0].legend(['Yaw', 'Pitch', 'Roll'])

        ts_world = pose_trajectory_cam_world.timestamps
        xyz_world = pose_trajectory_cam_world.positions_xyz
        euler_world = pose_trajectory_cam_world.get_orientations_euler(axes="szyx")
        axes[0, 1].plot(ts_world, xyz_world)
        axes[0, 1].set_title("World to CAM XYZ [m]")
        axes[0, 1].legend(['x', 'y', 'z'])
        axes[1, 1].plot(ts_world, euler_world)
        axes[1, 1].set_title("World to CAM orientation (euler) [rad]")
        axes[1, 1].legend(['Yaw', 'Pitch', 'Roll'])
        plt.tight_layout()
    else:
        pose_trajectory = None

    fake_R_real = Rotation.from_euler('ZYX',  [45, 0, 45], degrees=True).as_matrix().squeeze()
    # fake_R_real = np.eye(3)
    fake_T_real = np.eye(4)
    fake_T_real[:3, :3] = fake_R_real
    pose_trajectory.transform(np.linalg.inv(fake_T_real), right_mul=True)

    imu_fps = 1 / np.median(np.diff(reader.imu_data['nav-time [sec]'].values))
    # Deploy complementary filter
    acc = reader.imu_data.copy()[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]', 'nav-time [sec]']]
    gyro = reader.imu_data.copy()[['Angvel x', ' Angvely y', ' Angvel z [rad/s]', 'nav-time [sec]']]
    gyro.reset_index(inplace=True)

    ts_sec = reader.imu_data['nav-time [sec]']  # reader.convert_system_clk_to_sec(ts_system_clk)

    print("Calculating average gyro bias...")
    gyb = reader.get_gyro_bias(200)  # np.zeros((3,))  # reader.get_gyro_bias(200)
    # Rough guess for q_cam_imu
    R0 = np.array([[0.0,  -1.0,  0.0],
                   [-1.0,  0.0,  0.0],
                   [0.0,   0.0,  -1.0]])
    R0 = fake_R_real @ R0
    R_cam_imu = fake_R_real @ R_cam_imu

    imu_rotation_min_thresh = 3.
    t_imu_shift = imu_cam_calibration['cam0']['timeshift_cam_imu']
    cam_R_imu_est, delta_R_imu_list, delta_R_cam_list, t_mid_sample = \
        estimate_camera_imu_orientation(pose_trajectory, gyro, gyb,
                                        t_imu_shift=t_imu_shift,
                                        imu_rotation_min_thresh=imu_rotation_min_thresh)

    print(f"cam_R_imu est: {Rotation.from_matrix(cam_R_imu_est).as_euler('zyx', degrees=True)}")
    print(f"R_cam_imu gt: {Rotation.from_matrix(R_cam_imu).as_euler('zyx', degrees=True)}")

    R_cam_imu_real_gt = T_cam_imu[:3, :3]
    cam_R_imu_real_est = fake_R_real.T @ cam_R_imu_est

    print(f"Real cam_R_imu est: {Rotation.from_matrix(cam_R_imu_real_est).as_euler('zyx', degrees=True)}")
    print(f"Real R_cam_imu gt: {Rotation.from_matrix(R_cam_imu_real_gt).as_euler('zyx', degrees=True)}")
    print(f"Error: {np.linalg.norm(Rotation.from_matrix(R_cam_imu_real_gt.T @ cam_R_imu_real_est).as_euler('zyx', degrees=True))}")

    euler_imu_opt_list = []
    euler_imu_list = []
    for delta_R_cam, delta_R_imu in zip(delta_R_cam_list, delta_R_imu_list):
        R_cam_opt = cam_R_imu_est.T @ delta_R_cam @ cam_R_imu_est
        euler_imu_opt = Rotation.from_matrix(R_cam_opt).as_euler('zyx', degrees=True)
        euler_imu_opt_list.append(euler_imu_opt)

        euler_imu = Rotation.from_matrix(delta_R_imu).as_euler('zyx', degrees=True)
        euler_imu_list.append(euler_imu)

    euler_imu_opt_list = np.rad2deg(np.array(euler_imu_opt_list))
    euler_imu_est_list = np.rad2deg(np.array(euler_imu_list))

    fig, axes = plt.subplots(3, 1, sharex=True)
    axes[0].set_title('Yaw')
    axes[0].plot(euler_imu_est_list[:, 0], label='IMU')
    axes[0].plot(euler_imu_opt_list[:, 0], label='CAM SVD')
    axes[0].set_ylabel('[deg]')
    axes[0].legend()

    axes[1].set_title('Pitch')
    axes[1].plot(euler_imu_est_list[:, 1], label='IMU')
    axes[1].plot(euler_imu_opt_list[:, 1], label='CAM SVD')
    axes[1].set_ylabel('[deg]')
    axes[1].legend()

    axes[2].set_title('Roll')
    axes[2].plot(euler_imu_est_list[:, 2], label='IMU')
    axes[2].plot(euler_imu_opt_list[:, 2], label='CAM SVD')
    axes[2].set_ylabel('[deg]')
    axes[2].set_ylabel('[sec]')
    axes[2].legend()
    plt.tight_layout()
    plt.show()
