from tqdm import tqdm
from typing import Tuple, Optional
import numpy as np
from scipy.spatial.transform import Rotation


class ComplementaryFilter(object):
    def __init__(self, alpha: float = 0.98, cam_R_imu: np.ndarray=np.eye(3)) -> None:
        self.alpha = alpha
        self.compAngleX = 0.
        self.compAngleY = 0.
        self.cam_R_imu = cam_R_imu

    def init(self, ax, ay, az):
        # Calculate accelerometer angles
        accelAngleX, accelAngleY = self.accelerometer_angle(ax, ay, az)

        # Initialize filter to accel angles
        self.compAngleX = accelAngleX
        self.compAngleY = accelAngleY

    def CompFilterProcess(self, compAngle, accelAngle, omega, dt):
        """
        IIR Filter for a single axis
        :param accelAngle:
        :param omega:
        :return:

        """
        # Speed up filter convergence
        compAngle = self.format_fast_converge(compAngle, accelAngle)

        # Integrate the gyroscope's angular velocity reading to get an angle
        gyroAngle = compAngle + omega * dt

        # Weighting is applied to the gyroscope's angular position and
        # accelerometer's angular position and they are put together to form one
        # angle, the complimentary filter angle.
        compAngle = self.alpha * gyroAngle + (1.0 - self.alpha) * accelAngle

        # Format the Comp. Angle to lie in the range of -pi to pi
        compAngle = ComplementaryFilter.format_angle_range(compAngle)

        return compAngle

    def update(self, accel, gyro, dt):
        ax, ay, az = accel
        gx, gy, gz = gyro

        # Calculate the accelerometer angles
        accelAngleX, accelAngleY = self.accelerometer_angle(ax, ay, az)  # Roll, pitch

        # imu_R_I = self.world_to_body(accelAngleX, accelAngleY)
        # cam_R_I = self.cam_R_imu @ imu_R_I
        # euler_angles = Rotation.from_matrix(cam_R_I).as_euler('ZYX', degrees=True)
        # Roll. Rotation around x-axis of gyro
        self.compAngleX = self.CompFilterProcess(self.compAngleX, accelAngleX, gx, dt)

        # Pitch. Rotation around y-axis of gyro
        self.compAngleY = self.CompFilterProcess(self.compAngleY, accelAngleY, gy, dt)

        return self.compAngleX, self.compAngleY

    @staticmethod
    def format_accel_range(accelAngle, accelZ):
        if accelZ < 0.0:
            # Angle lies in Quadrant 2 or Quadrant 3 of
            # the unit circle
            accelAngle = np.pi - accelAngle
        elif (accelZ > 0.0) and (accelAngle < 0.0):
            # Angle lies in Quadrant 4 of the unit circle
            accelAngle = 2 * np.pi + accelAngle

        # If both of the previous conditions were not satisfied, then
        # the angle must lie in Quadrant 1 and nothing more needs
        # to be done.

        return accelAngle

    @staticmethod
    def accelerometer_angle(ax, ay, az):
        # Angle rotation around by X axis acceleration vector relative to ground
        accelAngleX = -np.arctan2(ay, -az)  # roll

        # Angle rotation around Y axis acceleration vector relative to ground
        accelAngleY = -np.arctan2(-ax, np.sqrt(ay * ay + az * az))  # pitch

        return accelAngleX, accelAngleY

    @staticmethod
    def format_angle_range(compAngle):
        """
        Euler angles are modulo 2*pi
        :param compAngle:
        :return:
        """
        while compAngle >= np.pi:
            compAngle = compAngle - 2 * np.pi

        while compAngle < -np.pi:
            compAngle = compAngle + 2 * np.pi

        return compAngle

    @staticmethod
    def format_fast_converge(compAngle, accAngle):

        # Work with comp. angles that are closest in distance to the accelerometer angle
        # on the unit circle. This allows for significantly faster filter convergence.
        if compAngle > accAngle + np.pi:
            compAngle = compAngle - 2 * np.pi
        elif accAngle > compAngle + np.pi:
            compAngle = compAngle + 2 * np.pi

        return compAngle

    @staticmethod
    def world_to_body(phi, theta):
        """
        Z is down. World frame is NED. [1]
        [1] https://en.wikipedia.org/wiki/Davenport_chained_rotations#Tait%E2%80%93Bryan_chained_rotations
        :param phi: Roll angle around x-axis
        :param theta: Pitch around y-axis
        :return: R_phi * R_theta
        """

        # The negative sign is due to world_to_body transform
        R_phi = np.array([[1., 0.,          0.],
                          [0., np.cos(phi), -np.sin(phi)],
                          [0., np.sin(phi), np.cos(phi)]])

        R_theta = np.array([[np.cos(theta),  0., np.sin(theta)],
                            [0.,             1., 0.],
                            [-np.sin(theta), 0., np.cos(theta)]])

        return R_phi @ R_theta

    def run(self, reader,
            acb: Optional[np.ndarray] = None,
            gyb: Optional[np.ndarray] = None) -> Tuple[np.ndarray, np.array]:
        """

        :param reader:
        :param acb: Accelerometer bias
        :param gyb: Gyro bias
        :return:
        """
        if gyb is None:
            gyb = reader.get_gyro_bias(200)
        if acb is None:
            acb = np.zeros((3, ))
        print(f"gyb: {gyb}")
        print(f"acb: {acb}")
        t_prev = None
        pitch_roll_ = []
        time_stamps = []
        for imu_raw in tqdm(reader.imu_data.iterrows(), "Complementary filter", total=len(reader.imu_data)):
            ind, data = imu_raw
            time_sec = data['nav-time [sec]']

            acc_raw = data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']].values
            gyro_raw = data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values

            [ax, ay, az] = acc_raw - acb

            if t_prev is None:
                self.init(ax, ay, az)
                t_prev = time_sec
                continue
            dt = time_sec - t_prev

            [gx, gy, gz] = gyro_raw - gyb

            # Update complimentary filter
            comp_x, comp_y = self.update([ax, ay, az], [gx, gy, gz], dt)

            pitch_roll_.append([comp_x, comp_y])
            time_stamps.append(time_sec)

            t_prev = time_sec

        pitch_roll_ = np.array(pitch_roll_)
        time_stamps = np.array(time_stamps)

        return time_stamps, pitch_roll_
