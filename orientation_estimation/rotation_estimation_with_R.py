import numpy as np
from pyquaternion import Quaternion
from functools import partial
from typing import Tuple

from utils.common import skew_matrix


def calculate_cost(delta: np.ndarray, R: np.ndarray, p: np.ndarray, q: np.ndarray) -> float:
    """
    :param delta: pertubation around rotation
    :param R: Rotation from source to target frame
    :param q: Relative rotation of target frame
    :param p: Relative rotation of source frame
    :return: MSE
    """

    return 0.5 * calculate_func(delta, R, p, q).T @ calculate_func(delta, R, p, q)


def calculate_func(delta: np.ndarray, R: np.ndarray, p: np.ndarray, q: np.ndarray):
    """
    :param delta: Small angle pertubation
    :param R: Rotation from source to target frame
    :param q: Relative rotation of target frame
    :param p: Relative rotation of source frame
    :return:
    """

    return R @ p + np.cross(delta, p) - q


def jacobian(p: np.ndarray):
    """
    J_G = d/(d delta)(R @ p + delta.cross(p) - q_t)
    :param p: Relative rotation of source frame
    :return:
    """
    J_g = -skew_matrix(p)
    return J_g


def gradient_descent(R0: np.ndarray,
                     p_cam: np.ndarray,
                     p_imu: np.ndarray,
                     der_tol: float = 1e-6) -> Tuple[Quaternion, float]:
    """
    :param q_cam_imu0: Initial guess of imu to camera rotation
    :param q_cam: Relative rotation of Cam (from differentiating the rotation of vision_world_R_cam)
    :param q_imu: Relative rotation of IMU (from integrating the gyro)
    :param mu: Descent rate
    :return: q_cam_imu estimate and cost in euler angles
    """

    R = R0

    v = np.zeros((3, ))
    prev_err = np.finfo(float).max
    for i in range(10000):
        func = partial(calculate_func, R=R, p=p_imu, q=p_cam)
        cost = partial(calculate_cost, R=R, p=p_imu, q=p_cam)
        der = jacobian(p_imu).T @ func(v)  # Gradient of 0.5 * G^T * G

        # Armijo rule: https://en.wikipedia.org/wiki/Wolfe_conditions
        p = -der
        beta = 0.1
        alpha = 0.01
        tau = 0.5

        v_temp = v + alpha * p
        while cost(v_temp) > cost(v) + alpha * beta and alpha > 1e-6:
            alpha = tau * alpha
            v_temp = v + alpha * p

        v = v_temp
        err = cost(v)
        if prev_err < err:  # should never happen
            # in cases of overshoot
            break

        prev_err = err

        R = R @ (np.eye(3) + skew_matrix(v))
        U, s, Vt = np.linalg.svd(R, full_matrices=False)
        R = np.dot(U, Vt)
        if np.linalg.norm(p) <= der_tol:
            break

    return R, err
