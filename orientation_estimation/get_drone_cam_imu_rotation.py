import numpy as np
import os
import matplotlib.pyplot as plt
import yaml

from scipy.spatial.transform import Rotation

from evo.core.trajectory import PoseTrajectory3D
from readers.sightec_data_reader import DataReader
from readers.slam_poses_reader import read_slam_poses

from hand_eye import estimate_camera_imu_orientation


if __name__ == "__main__":
    # Filter coefficient
    alpha = 0.99  #  0.98
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = False
    estimate_gyro_bias = True
    t_start = 0.
    t_end = np.inf
    # run_folder = 'run_#19'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#21")

    run_folder = 'run_#15'
    input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#22")

    # run_folder = 'run_#16'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#23")

    # run_folder = 'run_#17'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#24")

    # run_folder = 'run_#14'
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/07_12_21', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/complemetary_filter', "run_#26")

    input_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#26"
    output_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#26"
    # t_start = 183.
    # t_end = 260.

    input_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#29"
    output_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#29"

    # Get Image and IMU data
    reader = DataReader(input_folder)
    imu_fps = 1 / np.median(np.diff(reader.imu_data['nav-time [sec]'].values))

    try:
        imu_cam_config_yaml = os.path.join(input_folder, "cam_imu_config.yaml")
        with open(imu_cam_config_yaml) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        T_cam_imu = np.array(data['cam0']['T_cam_imu'])
        R_cam_imu = T_cam_imu[:3, :3]
        t_imu_shift = data['cam0']['timeshift_cam_imu']
        # t_imu_shift = 0.
    except Exception as e:
        R_cam_imu = np.array([[0.0, 1.0, 0.0],
                              [-1.0, 0.0, 0.0],
                              [0.0, 0.0, 1.0]])
        t_imu_shift = 0.

    T_cam_imu = np.eye(4)
    T_cam_imu[:3, :3] = R_cam_imu
    fake_R_real = Rotation.from_euler('ZYX', [45, 0, 45], degrees=True).as_matrix().squeeze()
    fake_T_real = np.eye(4)
    fake_T_real[:3, :3] = fake_R_real

    # Get SLAM poses
    indexes, poses = read_slam_poses(os.path.join(output_folder, "r_t_slam.csv"))
    reader.image_ts.set_index(['index']).loc[indexes, 'ts_sec']
    timestamps = reader.image_ts.set_index(['index']).loc[indexes, 'ts_sec']
    pose_trajectory = PoseTrajectory3D(poses_se3=poses, timestamps=timestamps)
    pose_trajectory.transform(np.linalg.inv(fake_T_real), right_mul=True)

    gyro = reader.imu_data.copy()[['Angvel x', ' Angvely y', ' Angvel z [rad/s]', 'nav-time [sec]']]
    gyro.reset_index(inplace=True)
    ts_sec = gyro['nav-time [sec]'].values
    plt.figure()
    ax = plt.gca()
    ax.plot(ts_sec, gyro[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values)
    ax.set_title("IMU Angvel [rad/s]")
    ax.set_ylim(-np.pi / 2, np.pi / 2)

    if estimate_gyro_bias:
        gyb = reader.get_gyro_bias(200)
    else:
        gyb = np.zeros((3,))  # reader.get_gyro_bias(200)  # np.zeros((3,))  # reader.get_gyro_bias(200)

    imu_rotation_min_thresh = 3.
    cam_R_imu_est, delta_R_imu_list, delta_R_cam_list, t_mid_sample = \
        estimate_camera_imu_orientation(pose_trajectory, gyro, gyb,
                                        t_imu_shift=t_imu_shift,
                                        imu_rotation_min_thresh=imu_rotation_min_thresh,
                                        t_start=t_start,
                                        t_end=t_end)

    print(f"cam_R_imu est: {Rotation.from_matrix(cam_R_imu_est).as_euler('ZYX', degrees=True)}")
    print(f"R_cam_imu gt(fake): {Rotation.from_matrix(fake_R_real @ R_cam_imu).as_euler('ZYX', degrees=True)}")

    R_cam_imu_real_gt = T_cam_imu[:3, :3]
    cam_R_imu_real_est = fake_R_real.T @ cam_R_imu_est

    print(f"Real cam_R_imu est: {Rotation.from_matrix(cam_R_imu_real_est).as_euler('ZYX', degrees=True)}")
    print(f"Real R_cam_imu gt: {Rotation.from_matrix(R_cam_imu_real_gt).as_euler('ZYX', degrees=True)}")
    print(f"Error: {np.linalg.norm(Rotation.from_matrix(R_cam_imu_real_gt.T @ cam_R_imu_real_est).as_euler('ZYX', degrees=True))}")

    euler_imu_opt_list = []
    euler_imu_list = []
    for delta_R_cam, delta_R_imu in zip(delta_R_cam_list, delta_R_imu_list):
        R_cam_opt = cam_R_imu_est.T @ delta_R_cam @ cam_R_imu_est
        euler_imu_opt = Rotation.from_matrix(R_cam_opt).as_euler('ZYX', degrees=True)
        euler_imu_opt_list.append(euler_imu_opt)

        euler_imu = Rotation.from_matrix(delta_R_imu).as_euler('ZYX', degrees=True)
        euler_imu_list.append(euler_imu)

    euler_imu_opt_list = np.array(euler_imu_opt_list)
    euler_imu_est_list = np.array(euler_imu_list)
    t_mid_sample = np.array(t_mid_sample)

    fig, axes = plt.subplots(3, 1, sharex=True)
    axes[0].set_title('Yaw')
    axes[0].plot(t_mid_sample, euler_imu_est_list[:, 0], label='IMU')
    axes[0].plot(t_mid_sample, euler_imu_opt_list[:, 0], label='CAM SVD')
    axes[0].set_ylabel('[deg]')
    axes[0].legend()

    axes[1].set_title('Pitch')
    axes[1].plot(t_mid_sample, euler_imu_est_list[:, 1], label='IMU')
    axes[1].plot(t_mid_sample, euler_imu_opt_list[:, 1], label='CAM SVD')
    axes[1].set_ylabel('[deg]')
    axes[1].legend()

    axes[2].set_title('Roll')
    axes[2].plot(t_mid_sample, euler_imu_est_list[:, 2], label='IMU')
    axes[2].plot(t_mid_sample, euler_imu_opt_list[:, 2], label='CAM SVD')
    axes[2].set_ylabel('[deg]')
    axes[2].set_xlabel('[sec]')
    axes[2].legend()
    plt.tight_layout()

    np.savetxt(os.path.join(output_folder, 'cam_R_imu.txt'), cam_R_imu_real_est, fmt='%.15f')
    plt.show()
    print("done")
