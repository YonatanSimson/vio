from typing import Optional

import cv2 as cv
import numpy as np
import pandas as pd
from evo.tools.file_interface import read_euroc_csv_trajectory

from orientation_estimation.develop_complemetary_filter import april_detector
from readers.sightec_data_reader import DataReader
from utils.common import pose_to_vectors, transform_points
from utils.render_3d import render_cube


def get_gt_from_grid(gt_path: str, vio_ref_results_path: str, tag_size: float, reader: DataReader,
                    visualize_drift: bool = True,
                    video_path: Optional[str] = None) -> pd.Series:

    if video_path is not None:
        fourcc = cv.VideoWriter_fourcc(*'XVID')
        video_out = cv.VideoWriter(video_path, fourcc, 60.0, (960, 600))

    traj_gt = read_euroc_csv_trajectory(gt_path)
    rovio_results = read_euroc_csv_trajectory(vio_ref_results_path)

    # Cube in april tag frame
    cube = tag_size * np.float32([[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 0, 0],
                                  [0, 0, -1], [0, 1, -1], [1, 1, -1], [1, 0, -1]])
    image_ts = reader.get_image_timestamps()
    vio_timestamps = (rovio_results.timestamps * 1e6).astype(np.int64)

    # Get closest image & april tag grid pose to first vio pose
    image_ts_idx = np.argmin(np.abs(vio_timestamps[0] - image_ts))
    at_ts_idx = np.argmin(np.abs(vio_timestamps[0] / 1e6 - traj_gt.timestamps))

    w_T_c0 = rovio_results.poses_se3[0]
    a_T_c0 = traj_gt.poses_se3[at_ts_idx]

    g_w = np.array([0., 0., 1.])

    # Cube in world coordinates
    w_T_a = w_T_c0 @ np.linalg.inv(a_T_c0)
    cube_w = (w_T_a[:3, :3] @ cube.T).T + w_T_a[:3, -1]
    for ts in image_ts[image_ts_idx:]:
        image, = reader.read_images(ts)
        if image is None:
            continue
        rgb = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

        # Get matching VIO pose w_T_c
        vio_ts_index = np.argmin(np.abs(vio_timestamps - ts))
        w_T_c = rovio_results.poses_se3[vio_ts_index]
        rvec, tvec = pose_to_vectors(w_T_c)  # w_T_c -> c_T_w

        # Get matching april tag grid pose
        at_ts_idx = np.argmin(np.abs(ts / 1e6 - traj_gt.timestamps))
        # Check time gap to nearest april tag grid pose
        cube_3d_april = None
        a_T_c = None
        if np.min(np.abs(vio_timestamps[0] / 1e6 - traj_gt.timestamps)) <= 1 / 30:
            a_T_c = traj_gt.poses_se3[at_ts_idx]
            cube_3d_april = transform_points(np.linalg.inv(a_T_c), cube)

        imgpts, jac = cv.projectPoints(cube_w, rvec, tvec, april_detector.K, april_detector.dist_coeffs)
        cube_3d_vio = transform_points(np.linalg.inv(w_T_c), cube_w)

        rgb = render_cube(rgb, imgpts)

        if cube_3d_april is not None:
            drift_3d = np.linalg.norm(cube_3d_vio - cube_3d_april, axis=1)[:4]
            g_cam = a_T_c[:3, :3].T @ g_w

            g_pivot = np.array([0., 0., 1.])  # 1m in front of the camera
            g_plum = g_pivot + 0.1 * g_cam  # dangle 10cm string from the pivot

            g_pivot_2d, jac = cv.projectPoints(g_pivot, np.zeros((3, 1)), np.zeros((3, 1)), april_detector.K,
                                               april_detector.dist_coeffs)
            g_plum_2d, jac = cv.projectPoints(g_plum, np.zeros((3, 1)), np.zeros((3, 1)), april_detector.K,
                                              april_detector.dist_coeffs)

            rgb = cv.line(rgb,
                          tuple(np.round(g_pivot_2d.squeeze()).astype(np.int32)),
                          tuple(np.round(g_plum_2d.squeeze()).astype(np.int32)),
                          (0, 255, 0), 2)

        if visualize_drift:
            cv.imshow('frame', rgb)
            cv.waitKey(1)
        if video_path is not None:
            video_out.write(rgb)

    if visualize_drift:
        cv.destroyAllWindows()

    if video_path is not None:
        video_out.release()

    return drift_3d.max()