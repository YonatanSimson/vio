import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import math

from readers.euroc_mav_reader import EurocMavReader
from evo.tools.file_interface import read_euroc_csv_trajectory

from orientation_estimation.complementary_filter import ComplementaryFilter


if __name__ == "__main__":
    alpha = 0.02
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['w_RS_S_x [rad s^-1]', 'w_RS_S_y [rad s^-1]', 'w_RS_S_z [rad s^-1]']]

    ts = acc.index.values

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-10., 20.)
    axes[1].plot(ts, gyro.values)
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    # Show GT
    gt_filename = os.path.join(filepath, 'state_groundtruth_estimate0/data.csv')
    df_gt = pd.read_csv(gt_filename)
    gt_trajectory = read_euroc_csv_trajectory(gt_filename)

    ts_gt = gt_trajectory.timestamps / 1e9
    xyz_gt = gt_trajectory.positions_xyz
    euler_gt = gt_trajectory.get_orientations_euler(axes='szyx')

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts_gt, xyz_gt)
    axes[0].set_title("GT XYZ [m]")
    axes[0].legend(['x', 'y', 'z'])
    axes[1].plot(ts_gt, np.unwrap(euler_gt, axis=0))
    axes[1].set_title("GT orientation (euler) [deg]")
    axes[1].legend(['yaw', 'pitch', 'roll'])

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-10., 20.)
    axes[1].plot(ts, gyro.values)
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    # Read IMU and run Complementary filter
    acc_bias = df_gt[[' b_a_RS_S_x [m s^-2]', ' b_a_RS_S_y [m s^-2]', ' b_a_RS_S_z [m s^-2]']]
    acb = acc_bias.mean().values

    print("Calculating average gyro bias...")
    gyro_bias = df_gt[[' b_w_RS_S_x [rad s^-1]', ' b_w_RS_S_y [rad s^-1]', ' b_w_RS_S_z [rad s^-1]']]
    gyb = gyro_bias.mean().values

    t_prev = None
    # Complimentary filter estimates
    phi_hat = 0.0
    theta_hat = 0.0
    pitch_roll = []

    comp_filter = ComplementaryFilter(alpha=1 - alpha)
    for imu_raw in reader.imu_data.iterrows():
        ind, data = imu_raw
        time_sec = data['nav-time [sec]']

        acc_raw = data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']].values
        gyro_raw = data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']].values

        [ax, ay, az] = acc_raw - acb

        if t_prev is None:
            comp_filter.init(ax, ay, az)
            t_prev = data['nav-time [sec]']
            continue
        dt = data['nav-time [sec]'] - t_prev
        phi_hat_acc = -math.atan2(-ay, az)  # math.atan2(ay, math.sqrt(ax ** 2.0 + az ** 2.0))
        theta_hat_acc = -math.atan2(ax, az)  # math.atan2(-ax, math.sqrt(ay ** 2.0 + az ** 2.0))

        [p, q, r] = gyro_raw - gyb

        # Calculate Euler angle derivatives
        phi_dot = p  # p + np.sin(phi_hat) * np.tan(theta_hat) * q + np.cos(phi_hat) * np.tan(theta_hat) * r
        theta_dot = q  # np.cos(phi_hat) * q - np.sin(phi_hat) * r

        # Update complimentary filter
        phi_hat = alpha * (phi_hat + dt * phi_dot) + alpha * phi_hat_acc
        theta_hat = alpha * (theta_hat + dt * theta_dot) + alpha * theta_hat_acc

        comp_x, comp_y = comp_filter.update([ax, ay, az], [p, q, r], dt)
        # Display results
        # print("Phi: " + str(round(phi_hat * 180.0 / np.pi, 1)) + " | Theta: " + str(round(theta_hat * 180.0 / np.pi, 1)))
        # print(
        #     "comp_x: " + str(round(comp_x * 180.0 / np.pi, 1)) + " | comp_y: " + str(round(comp_y * 180.0 / np.pi, 1)))
        # pitch_roll.append([phi_hat * 180.0 / np.pi, theta_hat * 180.0 / np.pi])
        pitch_roll.append([comp_x * 180.0 / np.pi, comp_y * 180.0 / np.pi])

    pitch_roll = np.array(pitch_roll)

    n = pitch_roll.shape[0]
    plt.figure()
    plt.plot(reader.imu_data['nav-time [sec]'].values[:n], np.unwrap(pitch_roll, axis=0))
    plt.ylim(-360, 360)
    plt.legend(['roll', 'pitch'])
    plt.title("Complementary filter output")
    plt.show()

    ax = acc.values[:, 0]
    ay = acc.values[:, 1]
    az = acc.values[:, 2]

    plt.figure()
    plt.plot(reader.imu_data['nav-time [sec]'].values, np.unwrap(-np.arctan2(ay, az), axis=0), label="roll")
    plt.plot(reader.imu_data['nav-time [sec]'].values, np.unwrap(-np.arctan2(ax, np.sign(az) * np.sqrt(az * az + ay * ay)), axis=0), label="pitch")
    # plt.ylim(0, 400)
    # plt.legend(['roll', 'roll'])
    plt.title("Complementary filter output")
    plt.show()