# VIO

Ramp up on IMU Data and test ideas for VIO/ivSLAM development.

Goals:
- Flexible VIO lego blocks that can be used for quick exploration of new idea
- Agnostic to input source. Must support Multiple Camera rigs with or without IMU inputs
- Make it easy to test and verify calibration of input sensors


## Installation

Create a virtual environment with python 3.7
Install requirements for installing third party c++ and python packages in Ubuntu 18.04 LTS

```commandline
sudo apt-get install -y texlive texlive-latex-extra texlive-fonts-recommended dvipng
sudo apt-get install -y cm-super
```

```
# clone from source
git clone --recurse-submodules git@bitbucket.org:YonatanSimson/vio.git
cd vio

# make virtual environment
sudo apt-get install cmake ccache virtualenv
sudo apt-get install python3-venv
sudo apt-get install python3-tk
python3 -m venv venv
source ./venv/bin/activate
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install --upgrade pip
pip install -r requirements.txt


# pybind11
PYBIND11_GLOBAL_SDIST=1 python3 -m pip install https://github.com/pybind/pybind11/archive/master.zip

# Sophus
python3 -m pip install sophuspy

# submodules
git submodule init
git submodule update

cd thirdparty/evo
python setup.py develop
cd ..

cd apriltags2_ethz
python setup.py develop
cd ../../

```

## Data Reading and Visualization

TBD

## Patch basedTracking

TBD

## IMU Integration

TBD

## Data

Two popular and accepted datasets are EurocMAV and KITTI 

Download EurocMAV:
```
cd ~/data
mkdir euroc_mav
cd euroc_mav

EUROC_MAV_URL="http://robotics.ethz.ch/~asl-datasets/ijrr_euroc_mav_dataset/"
curl -o MH_01_easy.zip "${EUROC_MAV_URL}machine_hall/MH_01_easy/MH_01_easy.zip"
curl -o MH_02_easy.zip "${EUROC_MAV_URL}machine_hall/MH_02_easy/MH_02_easy.zip"
curl -o MH_03_medium.zip "${EUROC_MAV_URL}machine_hall/MH_03_medium/MH_03_medium.zip"
curl -o MH_04_difficult.zip "${EUROC_MAV_URL}machine_hall/MH_04_difficult/MH_04_difficult.zip"
curl -o MH_05_difficult.zip "${EUROC_MAV_URL}machine_hall/MH_05_difficult/MH_05_difficult.zip"
curl -o V1_01_easy.zip "${EUROC_MAV_URL}vicon_room1/V1_01_easy/V1_01_easy.zip"
curl -o V1_02_medium.zip "${EUROC_MAV_URL}vicon_room1/V1_02_medium/V1_02_medium.zip"
curl -o V1_03_difficult.zip "${EUROC_MAV_URL}vicon_room1/V1_03_difficult/V1_03_difficult.zip"
curl -o V2_01_easy.zip "${EUROC_MAV_URL}vicon_room2/V2_01_easy/V2_01_easy.zip"
curl -o V2_02_medium.zip "${EUROC_MAV_URL}vicon_room2/V2_02_medium/V2_02_medium.zip"
curl -o V2_03_difficult.zip "${EUROC_MAV_URL}vicon_room2/V2_03_difficult/V2_03_difficult.zip"

# unzip files to folders
unzip MH_01_easy.zip -d MH_01_easy
unzip MH_02_easy.zip -d MH_02_easy
unzip MH_03_medium.zip -d MH_03_medium
unzip MH_04_difficult.zip -d MH_04_difficult
unzip MH_05_difficult.zip -d MH_05_difficult
unzip V1_01_easy.zip -d V1_01_easy
unzip V1_02_medium.zip -d V1_02_medium
unzip V1_03_difficult.zip -d V1_03_difficult
unzip V2_01_easy.zip -d V2_01_easy
unzip V2_02_medium.zip -d V2_02_medium
unzip V2_03_difficult.zip -d V2_03_difficult

```
