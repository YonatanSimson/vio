import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == "__main__":
    input_folder = "/home/yonatan/output/kalman_fusion/run_#16/CSV"
    output_folder1 = "/home/yonatan/output/kalman_fusion/run_#16/CSV_debug2"
    output_folder2 = "/home/yonatan/output/kalman_fusion/run_#16/CSV_debug4"

    # Read anchor inputs
    anchor_filename = os.path.join(input_folder, "anchors.csv")
    anchor_df = pd.read_csv(anchor_filename)
    anchor_df = anchor_df.loc[anchor_df['anchor_agree_points'] > 0]

    slam_filename = os.path.join(input_folder, "velocity.csv")
    slam_df = pd.read_csv(slam_filename, index_col=[0, 1])

    # Read KF fusion output
    kf_fusion_filename = os.path.join(output_folder1, "kf_fusion.csv")
    kf_df1 = pd.read_csv(kf_fusion_filename, index_col=[0])

    kf_fusion_filename = os.path.join(output_folder2, "kf_fusion.csv")
    kf_df2 = pd.read_csv(kf_fusion_filename, index_col=[0])

    plt.figure()
    plt.plot(kf_df1['kf_utm_x'].values, kf_df1['kf_utm_y'].values, label="KF output w/ outlier rejection")
    plt.plot(kf_df2['kf_utm_x'].values, kf_df2['kf_utm_y'].values, label="KF output w/o outlier rejection")
    plt.legend()
    plt.title("KF Output")
    plt.savefig(os.path.join(output_folder2, "kf_fusion_outliers.png"))

    print("None")
