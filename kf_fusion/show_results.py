import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == "__main__":
    input_folder = "/home/yonatan/output/kalman_fusion/run_#16/CSV"
    output_folder = "/home/yonatan/output/kalman_fusion/run_#16/CSV_debug5"
    slam_scale = 0.9

    # Read anchor inputs
    anchor_filename = os.path.join(input_folder, "anchors.csv")
    anchor_df = pd.read_csv(anchor_filename)
    anchor_df = anchor_df.loc[anchor_df['anchor_agree_points'] > 0]

    slam_filename = os.path.join(input_folder, "velocity.csv")
    slam_df = pd.read_csv(slam_filename, index_col=[0, 1])

    # Read KF fusion output
    kf_fusion_filename = os.path.join(output_folder, "kf_fusion.csv")
    kf_df = pd.read_csv(kf_fusion_filename, index_col=[0])

    slam_position_xyz_diff = np.cumsum(np.diff(slam_df[['SLAM pos x current frame', 'SLAM pos Y current frame', 'SLAM pos Z current frame']].values, axis=0) * slam_scale, axis=0)
    slam_position_xyz = slam_position_xyz_diff + \
                        slam_df[['SLAM pos x current frame', 'SLAM pos Y current frame', 'SLAM pos Z current frame']].iloc[0, :].values
    plt.figure()
    plt.plot(slam_df['SLAM pos x current frame'].values, slam_df['SLAM pos Y current frame'].values, label='SLAM input')
    plt.plot(slam_position_xyz[:, 0], slam_position_xyz[:, 1], label='SLAM input scaled')
    plt.plot(kf_df['kf_utm_x'].values, kf_df['kf_utm_y'].values, label="KF output")
    plt.scatter(anchor_df['curr_sift_utm.x'].values, anchor_df['curr_sift_utm.y'].values,
                label="Anchor input",
                facecolors='none', edgecolors='r')
    plt.legend()
    plt.title("SLAM/Anchor input - KF Output")
    plt.savefig(os.path.join(output_folder, "kf_fusion.png"))

    print("None")
