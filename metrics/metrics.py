import copy
import os
from typing import Dict
import pandas as pd

from evo.tools import file_interface
from evo.core import sync
from evo.core.trajectory import PoseTrajectory3D
from evo.core import metrics as evo_metrics
from evo.core.metrics import PE as PoseError

from .config import Config as ConfigMetrics


class Metrics(object):
    traj_ref: PoseTrajectory3D
    traj_est: PoseTrajectory3D
    all_metrics: Dict[str, PoseError]
    all_stats: Dict[str, Dict[str, float]]

    def __init__(self, reference_path: str, estimate_path: str, config: ConfigMetrics) -> None:
        """
        The poses are first synced in time then aligned using Umeyama's method with is the SVD version of Horns
        absolute orientation solution
        :param reference_path: Path to ground truth
        :param estimate_path:
        :param config: configuration of the metrics. Such as delta between relative poses
        """

        self.traj_ref = file_interface.read_euroc_csv_trajectory(reference_path)
        self.traj_est = file_interface.read_euroc_csv_trajectory(estimate_path)

        # Time sync
        self.traj_ref, self.traj_est = sync.associate_trajectories(self.traj_ref, self.traj_est)

        # Align
        self.traj_est_aligned = copy.deepcopy(self.traj_est)
        self.traj_est_aligned.align(self.traj_ref, correct_scale=config.correct_scale)

        if config.use_aligned_trajectories:
            data = (self.traj_ref, self.traj_est_aligned)
        else:
            data = (self.traj_ref, self.traj_est)

        # Absolute translation error
        pose_relation = evo_metrics.PoseRelation.translation_part
        ape_translation_metric = evo_metrics.APE(pose_relation)
        ape_translation_metric.process_data(data)

        # Absolute rotation error
        pose_relation = evo_metrics.PoseRelation.rotation_angle_deg
        ape_rotation_metric = evo_metrics.APE(pose_relation)
        ape_rotation_metric.process_data(data)

        #############################
        # RPE #######################
        #############################

        # Relative rotation error
        pose_relation = evo_metrics.PoseRelation.rotation_angle_deg
        rpe_rotation_metric = evo_metrics.RPE(pose_relation,
                                              delta=config.delta,
                                              delta_unit=config.delta_unit,
                                              all_pairs=config.all_pairs)
        rpe_rotation_metric.process_data(data)

        # Relative translation error
        pose_relation = evo_metrics.PoseRelation.translation_part
        # Run RRE on Data
        rpe_translation_metric = evo_metrics.RPE(pose_relation,
                                                 delta=config.delta,
                                                 delta_unit=config.delta_unit,
                                                 all_pairs=config.all_pairs)
        rpe_translation_metric.process_data(data)

        self.all_metrics = {"ape_translation": ape_translation_metric, "ape_rotation": ape_rotation_metric,
                            "rpe_translation": rpe_translation_metric, "rpe_rotation": rpe_rotation_metric}

        self.all_stats = {k: v.get_all_statistics() for k, v in self.all_metrics.items()}

    def output_statistics_to_file(self, output_folder: str) -> None:
        """
        Ouputs the errors to two files (vio_errors.csv, vio_errors.html) in the output_folder
        :param output_folder:
        :return: None
        """
        df_stats = pd.DataFrame.from_dict(self.all_stats, orient="index")
        df_stats.drop("sse", axis=1, inplace=True)
        df_stats.to_csv(os.path.join(output_folder, "vio_errors.csv"))
        df_stats.to_html(os.path.join(output_folder, "vio_errors.html"))
