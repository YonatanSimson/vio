from evo.core import metrics


class Config(object):
    correct_scale: bool
    use_aligned_trajectories: bool
    delta: float
    delta_unit: metrics.Unit
    all_pairs: bool

    def __init__(self) -> None:

        self.correct_scale = True
        self.use_aligned_trajectories = True
        self.delta = 10
        self.delta_unit = metrics.Unit.meters
        self.all_pairs = True
