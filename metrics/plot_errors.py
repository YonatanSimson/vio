import os
import copy
from typing import Optional, Any
from .metrics import Metrics as MetricsCalculator
import matplotlib.pyplot as plt

from evo.tools import plot


def get_object_class_name(obj: Any):
    return obj.__class__.__name__


def error_plotter(error_metrics: MetricsCalculator, output_folder: Optional[str] = None) -> None:
    """
    Plots all the errors
    There are absolute and relative errors, further slit into the rotational and transitional parts in
    degrees and meters respectively
    :param error_metrics:
    :param output_folder: output folder for plots.
    """

    ape_translation_metric = error_metrics.all_metrics["ape_translation"]
    ape_translation_stats = error_metrics.all_stats["ape_translation"]

    ape_rotation_metric = error_metrics.all_metrics["ape_rotation"]
    ape_rotation_stats = error_metrics.all_stats["ape_rotation"]

    rpe_translation_metric = error_metrics.all_metrics["rpe_translation"]
    rpe_translation_stats = error_metrics.all_stats["rpe_translation"]

    rpe_rotation_metric = error_metrics.all_metrics["rpe_rotation"]
    rpe_rotation_stats = error_metrics.all_stats["rpe_rotation"]

    # 3D plots
    fig = plt.figure()
    traj_by_label = {
        "estimate (aligned)": error_metrics.traj_est_aligned,
        "reference": error_metrics.traj_ref
    }
    plot.trajectories(fig, traj_by_label, plot.PlotMode.xyz)
    ax = plt.gca()
    ax.set_title("Trajectories")
    plt.savefig(os.path.join(output_folder, "trajectories.png"))

    plot_mode = plot.PlotMode.xyz
    fig = plt.figure()
    ax = plot.prepare_axis(fig, plot_mode)
    plot.traj(ax, plot_mode, error_metrics.traj_ref, '--', "gray", "reference")
    plot.traj_colormap(ax, error_metrics.traj_est_aligned, ape_translation_metric.error,
                       plot_mode, min_map=ape_translation_stats["min"],
                       max_map=ape_translation_stats["max"])
    ax.set_title(str(ape_translation_metric))
    ax.legend()
    if output_folder is not None:
        fig_filename = f"{get_object_class_name(ape_translation_metric)}_{ape_translation_metric.pose_relation.name}_3D.png"
        plt.savefig(os.path.join(output_folder, fig_filename))

    plot_mode = plot.PlotMode.xyz
    fig = plt.figure()
    ax = plot.prepare_axis(fig, plot_mode)
    plot.traj(ax, plot_mode, error_metrics.traj_ref, '--', "gray", "reference")
    plot.traj_colormap(ax, error_metrics.traj_est_aligned, ape_rotation_metric.error,
                       plot_mode, min_map=ape_rotation_stats["min"], max_map=ape_rotation_stats["max"])
    ax.set_title(str(ape_rotation_metric))
    ax.legend()
    if output_folder is not None:
        fig_filename = f"{get_object_class_name(ape_rotation_metric)}_{ape_rotation_metric.pose_relation.name}_3D.png"
        plt.savefig(os.path.join(output_folder, fig_filename))

    # important: restrict data to delta ids for plot
    traj_ref_plot = copy.deepcopy(error_metrics.traj_ref)
    traj_est_plot = copy.deepcopy(error_metrics.traj_est_aligned)
    traj_ref_plot.reduce_to_ids(rpe_translation_metric.delta_ids)
    traj_est_plot.reduce_to_ids(rpe_translation_metric.delta_ids)

    plot_mode = plot.PlotMode.xyz
    fig = plt.figure()
    ax = plot.prepare_axis(fig, plot_mode)
    plot.traj(ax, plot_mode, traj_ref_plot, '--', "gray", "reference")
    plot.traj_colormap(ax, traj_est_plot, rpe_translation_metric.error, plot_mode, min_map=rpe_translation_stats["min"],
                       max_map=rpe_translation_stats["max"])
    ax.set_title(f"{str(rpe_translation_metric)}")
    ax.legend()
    if output_folder is not None:
        fig_filename = f"{get_object_class_name(rpe_translation_metric)}_{rpe_translation_metric.pose_relation.name}_3D.png"
        plt.savefig(os.path.join(output_folder, fig_filename))

    # important: restrict data to delta ids for plot
    traj_ref_plot = copy.deepcopy(error_metrics.traj_ref)
    traj_est_plot = copy.deepcopy(error_metrics.traj_est_aligned)
    traj_ref_plot.reduce_to_ids(rpe_rotation_metric.delta_ids)
    traj_est_plot.reduce_to_ids(rpe_rotation_metric.delta_ids)
    plot_mode = plot.PlotMode.xyz
    fig = plt.figure()
    ax = plot.prepare_axis(fig, plot_mode)
    plot.traj(ax, plot_mode, traj_ref_plot, '--', "gray", "reference")
    plot.traj_colormap(ax, traj_est_plot, rpe_rotation_metric.error, plot_mode, min_map=rpe_rotation_stats["min"],
                       max_map=rpe_rotation_stats["max"])
    ax.set_title(f"{str(rpe_rotation_metric)}")
    ax.legend()
    if output_folder is not None:
        fig_filename = f"{get_object_class_name(rpe_rotation_metric)}_{rpe_rotation_metric.pose_relation.name}_3D.png"
        plt.savefig(os.path.join(output_folder, fig_filename))

    #########################
    # Error graph subplots  #
    #########################

    # APE
    # Translation
    seconds_from_start = []
    for t in error_metrics.traj_est_aligned.timestamps:
        seconds_from_start.append(t - error_metrics.traj_est_aligned.timestamps[0])

    fig_error, axes = plt.subplots(2, 2, figsize=(15, 10))
    plot.error_array(axes[0, 0], ape_translation_metric.error, x_array=seconds_from_start,
                     statistics={s: v for s, v in ape_translation_stats.items() if s != "sse"},
                     title=str(ape_translation_metric), xlabel="$t$ (s)",
                     ylabel=f"[{ape_translation_metric.unit.value}]")

    # Rotation
    plot.error_array(axes[0, 1], ape_rotation_metric.error, x_array=seconds_from_start,
                     statistics={s: v for s, v in ape_rotation_stats.items() if s != "sse"},
                     title=str(ape_rotation_metric), xlabel="$t$ (s)",
                     ylabel=f"[{ape_rotation_metric.unit.value}]")

    # RPE
    # Translation
    x_array = error_metrics.traj_est.distances[rpe_translation_metric.delta_ids]
    plot.error_array(axes[1, 0], rpe_translation_metric.error, x_array=x_array,
                     statistics={s: v for s, v in rpe_translation_stats.items() if s != "sse"},
                     ylabel=f"[{rpe_translation_metric.unit.value}]", title=str(rpe_translation_metric),
                     xlabel=f"[{rpe_translation_metric.delta_unit.value}]")

    # Rotation
    x_array = error_metrics.traj_est.distances[rpe_rotation_metric.delta_ids]
    plot.error_array(axes[1, 1], rpe_rotation_metric.error, x_array=x_array,
                     statistics={s: v for s, v in rpe_rotation_stats.items() if s != "sse"},
                     ylabel=f"[{rpe_rotation_metric.unit.value}]", title=str(rpe_rotation_metric),
                     xlabel=f"[{rpe_rotation_metric.delta_unit.value}]")
    plt.tight_layout()
    if output_folder is not None:
        fig_filename = f"ape_rpe_translation_rotation_errors.png"
        plt.savefig(os.path.join(output_folder, fig_filename))
