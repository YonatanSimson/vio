from copy import deepcopy

import numpy as np
from pyquaternion import Quaternion


def compute_omega_skew(omega: np.ndarray) -> np.ndarray:
    """
    Skew matrix of 3x1 numpy array
    :param omega:
    :return:
    """
    skew = np.array([[0, -omega[0], -omega[1], -omega[2]],
                     [omega[0], 0, omega[2], -omega[1]],
                     [omega[1], -omega[2], 0, omega[0]],
                     [omega[2], omega[1], -omega[0], 0]
                     ])
    return skew


def quat_integration_step_rk4(q_start: Quaternion, omega0: np.ndarray, omega1: np.ndarray, dt: float) -> Quaternion:
    """
    Integrate orientation between to gyro measurements
    :param q_start: quaternion start position. Quaternion wxyz
    :param omega0: corrected gyro measurement. 3x1 numpy vector
    :param omega1: next corrected gyro measurement. 3x1 numpy vector
    :param dt: time delta in seconds between the two measurements
    :return:
    """
    q = deepcopy(q_start.elements)  # quaternion in wxyz format
    omega01 = 0.5 * (omega0 + omega1)

    # First Runge - Kutta coefficient
    omega_skew = compute_omega_skew(omega0)
    k1 = 0.5 * omega_skew @ q

    # Second Runge-Kutta coefficient
    tmp_q = q + 0.5 * dt * k1
    omega_skew = compute_omega_skew(omega01)
    k2 = 0.5 * omega_skew @ tmp_q

    # Third Runge-Kutta coefficient (same omega skew as second coeff.)
    tmp_q = q + 0.5 * dt * k2
    k3 = 0.5 * omega_skew @ tmp_q

    # Fourth Runge-Kutta coefficient
    tmp_q = q + dt * k3
    omega_skew = compute_omega_skew(omega1)
    k4 = 0.5 * omega_skew @ tmp_q
    mult1 = 1.0 / 6.0
    mult2 = 1.0 / 3.0
    quat_res = q + dt * (mult1 * k1 + mult2 * k2 + mult2 * k3 + mult1 * k4)
    quat_res = Quaternion(quat_res)
    return quat_res.normalised