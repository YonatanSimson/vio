from typing import Tuple

import cv2 as cv
import numpy as np
import sophus as sp

from sensors.camera_config import CameraConfig, CameraConfigSource


class Camera(object):
    camera_config: CameraConfig
    pose_world2rig: sp.SE3

    def __init__(self, config_json: str, source: CameraConfigSource = CameraConfigSource.SIGHTEC):

        self.camera_config = CameraConfig(config_json, source=source)
        self.pose_world2rig = sp.SE3()

    @property
    def pose_cam2rig(self) -> sp.SE3:
        return self.camera_config.rig_T_cam

    @property
    def size(self) -> np.ndarray:
        return np.asarray(self.camera_config.size)

    @property
    def K(self) -> np.ndarray:
        return self.camera_config.K

    @property
    def dist_coeffs(self) -> np.ndarray:
        return self.camera_config.distortion_coefficients

    def set_pose_world2rig(self, pose: sp.SE3) -> None:
        self.pose_world2rig = pose

    def project(self, p3d: np.ndarray) -> np.ndarray:
        """
        p3d: Nx3 points in meters
        Returns:
            Nx2 features in pixel coordinates
        """
        R = self.R
        t = self.trans
        rvec, _ = cv.Rodrigues(R)
        if len(p3d.shape) == 1:
            projected_pt2d, _ = cv.projectPoints(p3d[np.newaxis, :], rvec, t, self.K, self.dist_coeffs)
        elif len(p3d.shape) == 2:
            rvec, _ = cv.Rodrigues(R)
            projected_pt2d, _ = cv.projectPoints(p3d, rvec, t, self.K, self.dist_coeffs)

        return projected_pt2d.squeeze()

    def project_and_calculate_residual(self, p3d: np.ndarray, p2d: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """

        p3d: Nx3 points in meters
        p2d: Nx2 features in pixel coordinates
        Returns:
            Nx2 p3d points projected into image
            N residuals in pixel coordinates
        """
        p2d_proj = self.project(p3d)
        residuals = np.linalg.norm(p2d - p2d_proj, axis=1)
        return p2d_proj, residuals

    def unproject(self, p2d: np.ndarray):
        """
        p3d: Nx2 feature coordinates in xy pixel units
        Returns:
            features converted into 3D rays in world frame and camera center in world frame
        """
        undistorted = cv.undistortPoints(np.expand_dims(p2d, axis=1).astype(np.float32),
                                         self.K,
                                         self.dist_coeffs)
        # Convert to homogeneous
        rays = np.concatenate((undistorted, [1]))
        # Normalize
        rays = rays / np.linalg.norm(rays)
        # Orientate ray to world
        rays_world = self.R.T @ rays
        center = self.camera_position_in_world

        return rays_world, center

    def projection_matrix_3x4(self) -> np.ndarray:
        """
        Return a 3x4 numpy array transform from world to cam.Useful for non homogenous points
        """
        return self.projection_matrix().matrix3x4()

    def projection_matrix(self) -> sp.SE3:
        if self.pose_world2rig is None:
            pose_world2cam = self.pose_cam2rig.inverse()
        else:
            pose_world2cam = self.pose_cam2rig.inverse() * self.pose_world2rig
        return pose_world2cam

    @property
    def P(self) -> np.ndarray:
        """
        Projection matrix 3x4
        """
        return self.projection_matrix_3x4()

    @property
    def R(self) -> np.ndarray:
        """
        Translation
        """
        rotation_world2cam = self.pose_cam2rig.rotationMatrix().T @ \
                             self.pose_world2rig.rotationMatrix()
        return rotation_world2cam

    @property
    def trans(self) -> np.ndarray:
        """
        The translation from the projection matrix [R|t] from world to camera
        """
        pose_world2cam = self.projection_matrix()
        return pose_world2cam.translation()

    @property
    def camera_position_in_world(self) -> np.ndarray:
        """
        Position of camera origin in world
        """
        pose_world2cam = self.projection_matrix()
        return pose_world2cam.inverse().translation()

    def rotate_point(self, point: np.ndarray) -> np.ndarray:
        """
        Rotate point or ray from camera to world orientation
        """
        return self.R.T @ point

    @property
    def cam_to_rig(self) -> sp.SE3:
        return self.camera_config.cam_to_rig

    @property
    def time_shift(self) -> float:
        return self.camera_config.time_shift
