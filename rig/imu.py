from typing import Tuple

import numpy as np
import sophus as sp

from sensors.imu_config import IMUIntrinsics


class IMU(object):
    imu_intrinsics: IMUIntrinsics
    rig_T_imu: sp.SE3  # Extrinsics between rig and IMU/body

    def __init__(self, gyro_calib_fn: str, acc_calib_fn: str) -> None:
        self.imu_intrinsics = IMUIntrinsics(gyro_calib_fn, acc_calib_fn)
        self.rig_T_imu = sp.SE3()  # By default the rig and IMU are the same

    def calibrate_raw_input(self, gyro_input: np.ndarray, acc_input: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """
        Corrects the raw inputs of the IMU
        :param gyro_input: Nx3 numpy array
        :param acc_input: Nx3 numpy array
        :return:
            gyro_corrected: Nx3 numpy array
            acc_corrected: Nx3 numpy array
        """
        gyro_calib = self.imu_intrinsics.gyro_intrinsics
        gyro_corrected = gyro_calib.misaligned_matrix @ gyro_calib.scale_matrix @ \
                         (gyro_input.T - gyro_calib.bias_vector[:, np.newaxis])

        acc_calib = self.imu_intrinsics.acc_intrinsics
        acc_corrected = acc_calib.misaligned_matrix @ acc_calib.scale_matrix @ \
                        (acc_input.T - acc_calib.bias_vector[:, np.newaxis])

        return gyro_corrected.T, acc_corrected.T
