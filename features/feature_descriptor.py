from typing import Tuple
import numpy as np
from skimage.feature.util import FeatureDetector as SkimageFeatureDetector
from skimage.feature.util import DescriptorExtractor
from skimage.feature import CENSURE, BRIEF
from skimage.morphology import octagon
from skimage.feature import corner_orientations

from .feature_detector import FeatureDetector


class FeatureDescriptor(object):
    def __init__(self,
                 detector: SkimageFeatureDetector = CENSURE(mode='Octagon'),
                 extractor: DescriptorExtractor = BRIEF()
                 ) -> None:
        self.detector = FeatureDetector(detector)
        self.extractor = extractor

    def calculate_descriptor(self, img: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        """

        @param img:
        @return:
        """
        kps, scale = self.detector.detect(img)

        self.extractor.extract(img, kps)
        desc = self.extractor.descriptors
        kps = kps[self.extractor.mask]
        scale = scale[self.extractor.mask]
        orientation = corner_orientations(img, kps, octagon(9, 9))

        return kps, desc, scale, orientation
