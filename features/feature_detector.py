from typing import Tuple
import numpy as np
from skimage.feature.util import FeatureDetector as SkimageFeatureDetector
from skimage.feature import CENSURE


class FeatureDetector(object):
    def __init__(self, detector: SkimageFeatureDetector = CENSURE(mode='Octagon'), *args, **kwargs) -> None:
        self.detector = detector

    def detect(self, img: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        """

        @param img:
        @return:
        """
        self.detector.detect(img)
        kps = self.detector.keypoints
        scale = self.detector.scales

        return kps, scale
