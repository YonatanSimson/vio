from typing import Tuple
import numpy as np
import matplotlib.pyplot as plt
from skimage.feature import match_descriptors, plot_matches

from vo.frame import Frame


class FrameToFrameMatcher(object):
    def __init__(self):
        pass

    @staticmethod
    def stereo_match(frame0: Frame, frame1: Frame, debug: bool) -> Tuple[np.ndarray, np.ndarray]:
        """
        Return matching points from both frames
        @param frame0: First frame
        @param frame1: Second frame
        @param debug: Display debug image
        @return:
            A tuple of matching points corresponding first and second frame accordingly
            The points are in xy format Nx2 with N the number of points
        """
        matches01 = match_descriptors(frame0.desc, frame1.desc, cross_check=True, max_ratio=0.9)

        if debug:
            fig, ax = plt.subplots(nrows=1, ncols=1)
            plt.gray()
            plot_matches(ax, frame0.img, frame1.img, frame0.kps, frame1.kps, matches01)
            ax.axis('off')
            ax.set_title("cam0 vs. cam1")

        points0 = frame0.kps[matches01[:, 0], :][:, ::-1].astype(np.float32)
        points1 = frame1.kps[matches01[:, 1], :][:, ::-1].astype(np.float32)

        return points0, points1

