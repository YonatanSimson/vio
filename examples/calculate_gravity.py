import numpy as np

if __name__ == "__main__":

    lat = 32.0687375
    lon = 34.7915885
    height = 0.
    # Mean radius of curvature of Earth and rotation rate
    R0 = 6371008.00  #             // in meters
    rotEarth = 0.00007292115  #    // in ra d /s

    # Define magnitude of gravity
    s2L = np.sin(lat) ** 2.
    s22L = np.sin(2.0 * lat) ** 2.0
    g_norm = 9.780318 * (1.0 + 0.0053024 * s2L - 0.0000059 * s22L)

    g_normHeight = g_norm / ((1.0 + height / R0) ** 2.0)

    # Compute gravity with the plumb-bob effect
    gravNED = np.zeros((3, ))
    gravNED[0] = -((rotEarth ** 2.0) * (R0 + height) * np.sin(2.0 * lat) / 2.0)
    gravNED[1] = 0.0
    gravNED[2] = g_normHeight - ((rotEarth ** 2.0) * (R0 + height) * (1.0 + np.cos(2.0 * lat)) / 2.0)

    gravENU = np.zeros((3,))
    gravENU[0] = 0.0
    gravENU[1] = -((rotEarth ** 2.0) * (R0 + height) * np.sin(2.0 * lat) / 2.0)
    gravENU[2] = -g_normHeight + ((rotEarth ** 2.0) * (R0 + height) * (1.0 + np.cos(2.0 * lat)) / 2.0)

    print(f"gravNED: {gravNED}, magnitude: {np.linalg.norm(gravNED)}")
    print(f"gravENU: {gravENU}, magnitude: {np.linalg.norm(gravENU)}")
