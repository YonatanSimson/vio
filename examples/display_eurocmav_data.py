import cv2 as cv
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
from readers.euroc_mav_reader import EurocMavReader
from evo.tools.file_interface import read_euroc_csv_trajectory


if __name__ == "__main__":
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['w_RS_S_x [rad s^-1]', 'w_RS_S_y [rad s^-1]', 'w_RS_S_z [rad s^-1]']]

    ts = acc.index.values

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-10., 20.)
    axes[1].plot(ts, gyro.values)
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    # Show GT
    gt_filename = os.path.join(filepath, 'state_groundtruth_estimate0/data.csv')
    df_gt = pd.read_csv(gt_filename)
    gt_trajectory = read_euroc_csv_trajectory(gt_filename)

    ts_gt = gt_trajectory.timestamps / 1e9
    xyz_gt = gt_trajectory.positions_xyz
    euler_gt = gt_trajectory.get_orientations_euler(axes='szyx')

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts_gt, xyz_gt)
    axes[0].set_title("GT XYZ [m]")
    axes[0].legend(['x', 'y', 'z'])
    axes[1].plot(ts_gt, np.unwrap(euler_gt, axis=0))
    axes[1].set_title("GT orientation (euler) [deg]")
    axes[1].legend(['yaw', 'pitch', 'roll'])

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-10., 20.)
    axes[1].plot(ts, gyro.values)
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    # Visualize data
    image_ts = reader.get_image_timestamps()
    for ts in image_ts:
        images = reader.read_images(ts)

        # concatenate images
        dbg_image = np.hstack(images)
        cv.imshow('frames', dbg_image)
        cv.waitKey(3)

    plt.show()
    print("Done")
