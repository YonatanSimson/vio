import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from readers.sightec_data_reader import DataReader


if __name__ == "__main__":
    # data_folder = '/home/yonatan/data/TalmeiYosef/run_#21'
    # t_before_engines = [0, 3]  # [0, 35]
    # t_in_air = [85, 110]

    data_folder = '/home/yonatan/data/Hod-Hasharon/17_08_2021/run_70'
    t_before_engines = [0, 35]  # [0, 35]
    t_in_air = [170, 205]

    reader = DataReader(data_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = acc.index.values
    ts = (ts - ts[0]) * 1e-6

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-20., 20.)
    axes[1].plot(ts, np.rad2deg(gyro.values))
    axes[1].set_title("Angvel [deg/s]")
    axes[1].set_ylim(-100., 100.)
    plt.tight_layout()
    plt.show()

    #####
    idx_before_engines = np.where(np.logical_and(t_before_engines[0] <= ts, ts <= t_before_engines[1]))[0]
    acc_before_engines = acc.values[idx_before_engines]
    gyro_before_engines = gyro.values[idx_before_engines]

    fs = 1 / np.median(np.diff(ts[idx_before_engines]))
    fig, axes = plt.subplots(3, 1, sharex=True)
    for label, idx in zip(['x', 'y', 'z'], [0, 1, 2]):
        f, Pxx_den = signal.welch(gyro_before_engines[:, idx], fs, nperseg=1024)
        axes[idx].semilogy(f, Pxx_den, label=label)
        axes[idx].set_ylabel('PSD [V**2/Hz]')
    axes[idx].set_xlabel('frequency [Hz]')
    plt.suptitle("Gyro Before Engines", y=.95)
    plt.tight_layout()

    fig, axes = plt.subplots(3, 1, sharex=True)
    for label, idx in zip(['x', 'y', 'z'], [0, 1, 2]):
        f, Pxx_den = signal.welch(acc_before_engines[:, idx], fs, nperseg=1024)
        axes[idx].semilogy(f, Pxx_den, label=label)
        axes[idx].set_ylabel('PSD [V**2/Hz]')
    axes[idx].set_xlabel('frequency [Hz]')
    plt.suptitle("Accelerometer Before Engines", y=.95)
    plt.tight_layout()

    # PSD in the air
    idx_in_air = np.where(np.logical_and(t_in_air[0] <= ts, ts <= t_in_air[1]))[0]
    acc_in_air = acc.values[idx_in_air]
    gyro_in_air = gyro.values[idx_in_air]
    fs = 1 / np.median(np.diff(ts[idx_in_air]))

    fig, axes = plt.subplots(3, 1, sharex=True)
    for label, idx in zip(['x', 'y', 'z'], [0, 1, 2]):
        f, Pxx_den = signal.welch(acc_in_air[:, idx], fs, nperseg=1024)
        axes[idx].semilogy(f, Pxx_den, label=label)
        axes[idx].set_ylabel('PSD [V**2/Hz]')
    axes[idx].set_xlabel('frequency [Hz]')
    plt.suptitle("Accelerometer in the air", y=.95)
    plt.tight_layout()

    fig, axes = plt.subplots(3, 1, sharex=True)
    for label, idx in zip(['x', 'y', 'z'], [0, 1, 2]):
        f, Pxx_den = signal.welch(gyro_in_air[:, idx], fs, nperseg=1024)
        axes[idx].semilogy(f, Pxx_den, label=label)
        axes[idx].set_ylabel('PSD [V**2/Hz]')
    axes[idx].set_xlabel('frequency [Hz]')
    plt.suptitle("Gyro in the air", y=.95)
    plt.tight_layout()
    plt.show()
