import numpy as np
import cv2, PIL, os
from cv2 import aruco
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd


if __name__ == "__main__":
    aruco_dict = aruco.Dictionary_get(aruco.DICT_5X5_50)

    # squaresX, squaresY, squareLength, markerLength, dictionary
    board = aruco.CharucoBoard_create(3, 3, 1, .8, aruco_dict)
    imboard = board.draw((2000, 2000))

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    plt.imshow(imboard, cmap=mpl.cm.gray, interpolation="nearest")
    ax.axis("off")
    plt.show()

    cv2.imwrite("charuco_board.tiff", imboard)
    # cv2.imwrite("charuco_board.pdf", imboard)
