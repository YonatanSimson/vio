import cv2 as cv
import numpy as np
import os
import matplotlib.pyplot as plt
import yaml
import pandas as pd
from typing import Optional

from evo.tools.file_interface import read_euroc_csv_trajectory
# temporarily override some package settings
from evo.tools.settings import SETTINGS
from evo.tools import plot
from metrics.plot_errors import error_plotter
from metrics.config import Config as ConfigMetrics
from metrics.metrics import Metrics as MetricsCalculator
from evo.core import metrics as vio_metrics
from evo.core.trajectory import PoseTrajectory3D, calc_angular_speed

from readers.sightec_data_reader import DataReader
from utils.apriltag_pose import AprilTagPose
from utils.common import transform_points, pose_to_vectors
from utils.render_3d import render_cube


def calculate_drift(gt_path: str, vio_ref_results_path: str, tag_size: float, reader: DataReader,
                    visualize_drift: bool = True,
                    video_path: Optional[str] = None) -> pd.Series:

    if video_path is not None:
        fourcc = cv.VideoWriter_fourcc(*'XVID')
        video_out = cv.VideoWriter(video_path, fourcc, 60.0, (960, 600))

    traj_gt = read_euroc_csv_trajectory(gt_path)
    rovio_results = read_euroc_csv_trajectory(vio_ref_results_path)

    # Cube in april tag frame
    cube = tag_size * np.float32([[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 0, 0],
                                  [0, 0, -1], [0, 1, -1], [1, 1, -1], [1, 0, -1]])
    image_ts = reader.get_image_timestamps()
    vio_timestamps = (rovio_results.timestamps * 1e6).astype(np.int64)

    # Get closest image & april tag grid pose to first vio pose
    image_ts_idx = np.argmin(np.abs(vio_timestamps[0] - image_ts))
    at_ts_idx = np.argmin(np.abs(vio_timestamps[0] / 1e6 - traj_gt.timestamps))

    w_T_c0 = rovio_results.poses_se3[0]
    a_T_c0 = traj_gt.poses_se3[at_ts_idx]

    g_w = np.array([0., 0., 1.])

    # Cube in world coordinates
    w_T_a = w_T_c0 @ np.linalg.inv(a_T_c0)
    cube_w = (w_T_a[:3, :3] @ cube.T).T + w_T_a[:3, -1]
    for ts in image_ts[image_ts_idx:]:
        image, = reader.read_images(ts)
        if image is None:
            continue
        rgb = cv.cvtColor(image, cv.COLOR_GRAY2BGR)

        # Get matching VIO pose w_T_c
        vio_ts_index = np.argmin(np.abs(vio_timestamps - ts))
        w_T_c = rovio_results.poses_se3[vio_ts_index]
        rvec, tvec = pose_to_vectors(w_T_c)  # w_T_c -> c_T_w

        # Get matching april tag grid pose
        at_ts_idx = np.argmin(np.abs(ts / 1e6 - traj_gt.timestamps))
        # Check time gap to nearest april tag grid pose
        cube_3d_april = None
        a_T_c = None
        if np.min(np.abs(vio_timestamps[0] / 1e6 - traj_gt.timestamps)) <= 1 / 30:
            a_T_c = traj_gt.poses_se3[at_ts_idx]
            cube_3d_april = transform_points(np.linalg.inv(a_T_c), cube)

        imgpts, jac = cv.projectPoints(cube_w, rvec, tvec, april_detector.K, april_detector.dist_coeffs)
        cube_3d_vio = transform_points(np.linalg.inv(w_T_c), cube_w)

        rgb = render_cube(rgb, imgpts)

        if cube_3d_april is not None:
            drift_3d = np.linalg.norm(cube_3d_vio - cube_3d_april, axis=1)[:4]
            g_cam = a_T_c[:3, :3].T @ g_w

            g_pivot = np.array([0., 0., 1.])  # 1m in front of the camera
            g_plum = g_pivot + 0.1 * g_cam  # dangle 10cm string from the pivot

            g_pivot_2d, jac = cv.projectPoints(g_pivot, np.zeros((3, 1)), np.zeros((3, 1)), april_detector.K,
                                               april_detector.dist_coeffs)
            g_plum_2d, jac = cv.projectPoints(g_plum, np.zeros((3, 1)), np.zeros((3, 1)), april_detector.K,
                                              april_detector.dist_coeffs)

            rgb = cv.line(rgb,
                          tuple(np.round(g_pivot_2d.squeeze()).astype(np.int32)),
                          tuple(np.round(g_plum_2d.squeeze()).astype(np.int32)),
                          (0, 255, 0), 2)

        if visualize_drift:
            cv.imshow('frame', rgb)
            cv.waitKey(1)
        if video_path is not None:
            video_out.write(rgb)

    if visualize_drift:
        cv.destroyAllWindows()

    if video_path is not None:
        video_out.release()

    return drift_3d.max()


if __name__ == "__main__":
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = True
    show_imu = True

    run_folder = 'run_#20'
    # input_folder = os.path.join('/home/yonatan/data/imu_basler_calib/26_09_2021', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/imu_basler_calib_26_09_2021', run_folder + '_v14')
    # input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/22_10_2021', run_folder)
    # output_folder = os.path.join('/home/yonatan/output/sightec_vio/hodhasharon_22_10_2021', run_folder + '_v0')
    run_folder = 'run_#45'
    input_folder = os.path.join('/home/yonatan/data/complemetary_filter/22_03_2022', run_folder)
    output_folder = os.path.join('/home/yonatan/output/sightec_vio/home_22_03_2022', run_folder + '_v1')
    has_april_tag = os.path.isfile(os.path.join(input_folder, 'April_WC_gt.csv'))

    cam_imu_extrinsics_calibration = os.path.join(input_folder, "imu_camera_extrinsics.yaml")
    with open(cam_imu_extrinsics_calibration) as f:
        imu_cam_calibration = yaml.load(f, Loader=yaml.FullLoader)
    print(imu_cam_calibration)

    imu_calibration_cam0 = imu_cam_calibration['cam0']
    # invert and convert to quaternion
    T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])

    # world_T_cam poses
    vio_ref_results_path = os.path.join(output_folder, 'WC.csv')
    rovio_results = read_euroc_csv_trajectory(vio_ref_results_path)
    reader = DataReader(input_folder, rig_extrinsics_config="imu_camera_extrinsics.yaml")
    april_detector = AprilTagPose(K=reader.camera.K,
                                  dist_coeffs=reader.camera.dist_coeffs,
                                  tag_cols=6,
                                  tag_rows=6,
                                  tag_size=tag_size,
                                  tag_spacing_ratio=tag_spacing_ratio,
                                  min_detected_tags=18)

    gt_path = os.path.join(input_folder, 'April_WC_gt.csv')
    if show_video and has_april_tag:
        video_path = os.path.join(output_folder, "drift_metrics", "drift_video.avi")
        if not os.path.isdir(os.path.join(output_folder, "drift_metrics")):
            os.makedirs(os.path.join(output_folder, "drift_metrics"))

        drift_3d = calculate_drift(gt_path, vio_ref_results_path, tag_size, reader, video_path=video_path)
        print(f"Drift: {drift_3d.max()}[m]")

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    # ts_system_clk = acc.index.values
    ts_sec = reader.imu_data['nav-time [sec]'] #reader.convert_system_clk_to_sec(ts_system_clk)

    # IMU-Camera extrinsics results
    cam_imu_results = read_euroc_csv_trajectory(os.path.join(output_folder, 'MC.csv'))

    # Visualize IMU
    fig, axes = plt.subplots(2, 3 if show_imu else 2, sharex=False, figsize=(16, 8))

    ts_vio = rovio_results.timestamps
    xyz_rovio = rovio_results.positions_xyz
    euler_rovio = rovio_results.get_orientations_euler()

    axes[0, 0].plot(ts_vio, xyz_rovio)
    axes[0, 0].set_title("ROVIO XYZ [m]")
    axes[0, 0].legend(['x', 'y', 'z'])
    axes[1, 0].plot(ts_vio, euler_rovio)
    axes[1, 0].set_title("ROVIO orientation (euler) [deg]")
    axes[1, 0].legend(['x', 'y', 'z'])

    ts_imu_cam = cam_imu_results.timestamps
    xyz_imu_cam = cam_imu_results.positions_xyz
    euler_imu_cam = cam_imu_results.get_orientations_euler()

    axes[0, 1].plot(ts_imu_cam, xyz_imu_cam)
    axes[0, 1].set_title("IMU-CAM XYZ [m]")
    axes[0, 1].legend(['x', 'y', 'z'])
    axes[1, 1].plot(ts_imu_cam, euler_imu_cam)
    axes[1, 1].set_title("IMU-CAM orientation (euler) [deg]")
    axes[1, 1].legend(['x', 'y', 'z'])

    if show_imu:
        axes[0, 2].plot(ts_sec, acc.values)
        axes[0, 2].set_title("IMU Accel [m/s2]")
        axes[0, 2].set_ylim(-20., 20.)
        axes[1, 2].plot(ts_sec, gyro.values)
        axes[1, 2].set_title("IMU Angvel [rad/s]")
        axes[1, 2].set_ylim(-np.pi / 2, np.pi / 2)

    plt.tight_layout()

    plt.savefig(os.path.join(output_folder, "results.png"))

    # Display state log evolution
    df_update_state = pd.read_csv(os.path.join(output_folder, 'state_update_log.csv'), header=0)
    df_update_state.reset_index(inplace=True)

    stamps = np.divide(df_update_state["#timestamp"].values, 1e9)  # n x 1  -  nanoseconds to seconds
    xyz = df_update_state[["p_MC_x[m]", "p_MC_y[m]", "p_MC_z[m]"]]  # n x 3
    quat = df_update_state[["q_MC_w", "q_MC_x", "q_MC_y", "q_MC_z"]]  # n x 4
    traj_mc = PoseTrajectory3D(xyz, quat, stamps)
    ts_mc = traj_mc.timestamps
    xyz_mc = traj_mc.positions_xyz
    euler_mc = traj_mc.get_orientations_euler()

    gyro_bias = df_update_state[['gyb_x[rad/s]', 'gyb_y[rad/s]', 'gyb_z[rad/s]']]
    acc_bias = df_update_state[['acb_x[m/s2]', 'acb_y[m/s2]', 'acb_z[m/s2]']]
    speed_imu = df_update_state[['v_MW_x[m/s]', 'v_MW_y[m/s]', 'v_MW_z[m/s]']]
    fig, axes = plt.subplots(2, 2, sharex=True)
    axes[0, 0].plot(ts_mc, xyz_mc)
    axes[0, 0].set_title("State MC XYZ [m]")
    axes[0, 0].legend(['x', 'y', 'z'])
    axes[1, 0].plot(ts_mc, speed_imu)
    axes[1, 0].set_title("State IMU speed [m/s^2]")
    axes[1, 0].legend(['x', 'y', 'z'])

    axes[0, 1].plot(ts_mc, gyro_bias)
    axes[0, 1].set_title("State gyro bias XYZ [rad/s]")
    axes[0, 1].legend(['x', 'y', 'z'])
    axes[1, 1].plot(ts_mc, acc_bias)
    axes[1, 1].set_title("State accel bias [m/s^2]")
    axes[1, 1].legend(['x', 'y', 'z'])

    plt.tight_layout()

    end_time = 485.0
    end_idx = int(np.argmin(np.abs(end_time - ts_vio)))
    # God view
    fig, axes = plt.subplots(1, 1)
    axes.plot(xyz_rovio[:end_idx, 0], xyz_rovio[:end_idx, 1])
    axes.set_title("ROVIO XY [m]")
    axes.set_xlabel('x')
    axes.set_ylabel('y')

    if not has_april_tag:
        plt.show()
        exit(1)

    time_zoom = 38.2479  # [sec]
    index = np.argmin(np.abs(time_zoom - ts_mc))
    timestamp = df_update_state.loc[index, "#timestamp"]
    df_zoom = df_update_state.loc[df_update_state["#timestamp"] == timestamp][["id", "p_MC_x[m]", "p_MC_y[m]", "p_MC_z[m]"]]
    df_zoom.set_index("id", inplace=True)

    df_zoom.plot(kind='bar', rot=0)
    plt.title(f"Update @{time_zoom} [sec]")

    indexes = range(len(df_zoom.index.values))
    plt.figure()
    plt.bar(df_zoom.index.values, np.linalg.norm(df_zoom[["p_MC_x[m]", "p_MC_y[m]", "p_MC_z[m]"]].values, axis=1))
    plt.xticks(indexes, df_zoom.index.values)  # set the X ticks and labels
    plt.xlabel("Feature ID")

    s_mc_norm = pd.Series(np.linalg.norm(df_zoom[["p_MC_x[m]", "p_MC_y[m]", "p_MC_z[m]"]].values, axis=1),
                                         index=df_zoom.index.values)
    plt.figure()
    s_mc_norm.plot(kind='bar', rot=0)

    # IMU frame rate
    imu_fps = 1e6 / np.median(np.diff(ts_system_clk))
    # VIO frame rate
    rovio_fps = 1 / np.median(np.diff(ts_out))

    #############
    # config ####
    #############
    config = ConfigMetrics()
    config.delta = 0.1
    config.delta_unit = vio_metrics.Unit.meters
    SETTINGS.plot_usetex = False
    SETTINGS.plot_pose_correspondences = True

    gt_path = os.path.join(input_folder, 'April_WC_gt.csv')

    ref_plots_folder = os.path.join(output_folder, 'plots')
    if not os.path.isdir(ref_plots_folder):
        os.makedirs(ref_plots_folder)

    ##############################
    #  Compare GT to IMU results #
    ##############################

    # Convert point of reference from camera to IMU
    traj_gt = read_euroc_csv_trajectory(gt_path)
    traj_gt.transform(T_cam_imu, right_mul=True)

    ##############################
    #  Compare GT to IMU results #
    ##############################
    ts_prev = None
    pose_prev = None
    omega = np.zeros((traj_gt.num_poses - 1, 3))
    for idx, (ts, pose) in enumerate(zip(traj_gt.timestamps, traj_gt.poses_se3)):
        if pose_prev is not None:
            w = calc_angular_speed(pose_prev, pose, ts_prev, ts)
            omega[idx - 1, :] = w
        pose_prev = pose
        ts_prev = ts

    #  np.interp(3.14, traj_gt.timestamps[:-1], omega, right=np.nan, left=np.nan)
    fig, axes = plt.subplots(2, 1, sharex=True, sharey=True)
    axes[0].plot(traj_gt.timestamps[:-1], omega)
    axes[0].set_title("GT angular velocity [rad/s]")
    axes[0].legend(['x', 'y', 'z'])
    axes[1].plot(ts_sec, gyro.values)
    axes[1].set_title("IMU Angvel [rad/s]")
    axes[1].legend(['x', 'y', 'z'])

    # Calculate metrics ##
    ######################
    ref_error_metrics = MetricsCalculator(gt_path, vio_ref_results_path, config)

    #####################
    # Build plots #######
    #####################
    error_plotter(ref_error_metrics, ref_plots_folder)

    # 3D plots
    fig = plt.figure()
    traj_by_label = {
        "ref estimate (aligned)": ref_error_metrics.traj_est_aligned,
        "reference": ref_error_metrics.traj_ref
    }
    plot.trajectories(fig, traj_by_label, plot.PlotMode.xyz)
    ax = plt.gca()
    ax.set_title("Trajectories")

    fig, axes = plt.subplots(3, 1, sharex=True)
    axes[0].plot(ref_error_metrics.traj_est_aligned.timestamps,
                 ref_error_metrics.traj_est_aligned.positions_xyz[:, 0],
                 label="reference")
    axes[0].legend()
    axes[0].set_title('x')
    axes[0].set_xlabel('t[us]')

    axes[1].plot(ref_error_metrics.traj_est_aligned.timestamps,
                 ref_error_metrics.traj_est_aligned.positions_xyz[:, 1],
                 label="reference")
    axes[1].legend()
    axes[1].set_title('y')
    axes[1].set_xlabel('t[us]')

    axes[2].plot(ref_error_metrics.traj_est_aligned.timestamps,
                 ref_error_metrics.traj_est_aligned.positions_xyz[:, 2],
                 label="reference")
    axes[2].legend()
    axes[2].set_title('z')
    axes[2].set_xlabel('t[us]')
    plt.tight_layout()

    print("Done")
    plt.show()



