import numpy as np
import cv2 as cv
import sophus as sp
import matplotlib.pyplot as plt

from readers.sightec_data_reader import DataReader


if __name__ == "__main__":
    data_folder = '/home/yonatan/data/TalmeiYosef/run_#21'

    reader = DataReader(data_folder)

    # make grid points
    r = 8
    c = 10
    x = np.linspace(0, 2 * c, c + 1) - 2 * r / 2
    y = np.linspace(0, 2 * r, r + 1) - 2 * c / 2

    X, Y = np.meshgrid(x, y)
    Z = np.zeros(X.shape)
    points_3d = np.vstack((X.ravel(), Y.ravel(), Z.ravel())).T

    plt.figure()
    plt.scatter(X, Y)
    for idx, (x, y) in enumerate(zip(X.ravel(), Y.ravel())):
        plt.text(x, y, f"{idx}")
    plt.axis("equal")
    plt.grid()

    # Camera pose looking down
    t_cw = np.array([0, 0, 20])  # world to camera
    R_cw = np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]])

    Iest = R_cw.T @ R_cw

    reader.camera.camera_config.distortion_coefficients[:] = 0.
    reader.camera.pose_world2rig = sp.SE3(R=R_cw, t=t_cw)
    points_2d_ref = reader.camera.project(points_3d)

    t_cw = np.array([0, 0, 21])  ## 20 cm elevation
    reader.camera.pose_world2rig = sp.SE3(R=R_cw, t=t_cw)
    points_2d_cur = reader.camera.project(points_3d)

    plt.figure()
    plt.scatter(points_2d_ref[:, 0], points_2d_ref[:, 1])
    plt.xlim([0, reader.camera.size[0]])
    plt.ylim([0, reader.camera.size[1]])
    for idx, (x, y) in enumerate(points_2d_ref):
        plt.text(x, y, f"{idx}")
    plt.gca().invert_yaxis()
    plt.show()

    # Now calculate homography
    ransacReprojThreshold = 3
    maxIterations_ = 2000
    confidence = 0.999
    H21, mask = cv.findHomography(points_2d_ref, points_2d_cur,
                                  method=cv.RHO,
                                  ransacReprojThreshold=ransacReprojThreshold,
                                  mask=None,
                                  maxIters=maxIterations_,
                                  confidence=confidence)

    retval, rotations, translations, normals = cv.decomposeHomographyMat(H21, reader.camera.K)

    # Try estimate the translation iteratively - with the assumption of translation only
    # or know rotation from integrating the gyro


    print("")


