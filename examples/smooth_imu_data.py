import os
from readers.sightec_data_reader import DataReader
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal


if __name__ == "__main__":
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14

    run_folder = 'run_#20'
    input_folder = os.path.join('/home/yonatan/data/Hod-Hasharon/22_10_2021', run_folder)

    reader = DataReader(input_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts_imu = reader.imu_data['nav-time [sec]'].values

    b, a = signal.butter(3, 0.3, fs=200)
    w, h = signal.freqs(b, a)
    plt.semilogx(w, 20 * np.log10(abs(h)))
    plt.title('Butterworth filter frequency response')
    plt.xlabel('Frequency [radians / second]')
    plt.ylabel('Amplitude [dB]')
    plt.margins(0, 0.5)
    plt.grid(which='both', axis='both')
    plt.axvline(100, color='green')  # cutoff frequency
    plt.show()

    # Use filtfilt to apply the filter.
    acc_smooth = signal.filtfilt(b, a, acc, axis=0)
    gyro_smooth = signal.filtfilt(b, a, gyro, axis=0)

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True, figsize=(16, 8))

    axes[0].plot(ts_imu, acc.values, label='raw')
    axes[0].plot(ts_imu, acc_smooth, label='smooth')
    axes[0].legend()
    axes[0].set_title("IMU Accel [m/s2]")
    axes[0].set_ylim(-20., 20.)

    axes[1].plot(ts_imu, gyro.values, label='raw')
    axes[1].plot(ts_imu, gyro_smooth, label='smooth')
    axes[1].legend()
    axes[1].set_title("IMU Angvel [rad/s]")
    axes[1].set_ylim(-np.pi / 2, np.pi / 2)

    plt.show()

    # Write smooth IMU signals back to file
    reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']] = acc_smooth
    reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']] = gyro_smooth

    imu_data_smooth = reader.imu_data.copy()

    imu_data_smooth.reset_index(inplace=True)

    # rename column 'ts_us' ->' IMU-Timestamp [nano]'
    imu_data_smooth.rename(columns={"ts_us": " IMU-Timestamp [nano]"}, inplace=True)

    # imu_data_smooth.set_index('nav-time [sec]', inplace=True)
    # set index to 'nav-time [sec]'
    imu_data_smooth.set_index('nav-time [sec]', drop=True, inplace=True)
    imu_data_smooth.to_csv(os.path.join(input_folder, "imuLog.csv"))

