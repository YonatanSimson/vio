import numpy as np
import copy
import cv2
import cv2.aruco as aruco
import matplotlib.pyplot as plt

if __name__ == "__main__":
    square_length = 0.09233
    aruco_dict = aruco.Dictionary_get(aruco.DICT_5X5_50)
    grid_x = 3
    grid_y = 3

    # squaresX, squaresY, squareLength, markerLength, dictionary
    board = aruco.CharucoBoard_create(grid_x, grid_y, square_length, square_length * .8, aruco_dict)

    cap = cv2.VideoCapture(0)
    allCorners = []
    allIds = []
    decimator = 0
    rvec, tvec = None, None
    camera_matrix = None
    while (True):
        # Capture frame-by-frame
        ret, frame = cap.read()
        # Our operations on the frame come here
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        imsize = gray.shape

        parameters = aruco.DetectorParameters_create()
        corners_int, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
        print(corners_int)

        # Detect board
        corners = copy.deepcopy(corners_int)
        res2 = False
        if len(corners) > 0:
            # SUB PIXEL CORNER DETECTION CRITERION
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.00001)
            # SUB PIXEL DETECTION
            for corner in corners:
                cv2.cornerSubPix(gray, corner,
                                 winSize=(3, 3),
                                 zeroZone=(-1, -1),
                                 criteria=criteria)
            res2, c_corners, c_ids = cv2.aruco.interpolateCornersCharuco(corners, ids, gray, board)
            if c_corners is not None and c_ids is not None and len(c_corners) > 3:
                allCorners.append(c_corners)
                allIds.append(c_ids)

        # Calibrate intrinsics
        if len(allCorners) > 100 and camera_matrix is None:
            cameraMatrixInit = np.array([[1000., 0., imsize[0] / 2.],
                                         [0., 1000., imsize[1] / 2.],
                                         [0., 0., 1.]])

            distCoeffsInit = np.zeros((5, 1))
            flags = cv2.CALIB_USE_INTRINSIC_GUESS
            print("Calibrating. Please wait... ")
            (ret, camera_matrix, dist_coeff,
             rotation_vectors, translation_vectors,
             stdDeviationsIntrinsics, stdDeviationsExtrinsics,
             perViewErrors) = cv2.aruco.calibrateCameraCharucoExtended(
                charucoCorners=allCorners,
                charucoIds=allIds,
                board=board,
                imageSize=imsize,
                cameraMatrix=cameraMatrixInit,
                distCoeffs=distCoeffsInit,
                flags=flags,
                criteria=(cv2.TERM_CRITERIA_EPS & cv2.TERM_CRITERIA_COUNT, 10000, 1e-9))

        useExtrinsicGuess = False if rvec is None else True
        if camera_matrix is not None and len(corners) > 0:
            ret3, rvec, tvec = cv2.aruco.estimatePoseCharucoBoard(c_corners,
                                                                 c_ids,
                                                                 board,
                                                                 camera_matrix,
                                                                 dist_coeff,
                                                                 rvec, tvec, useExtrinsicGuess=useExtrinsicGuess)
            if ret3:
                print('Translation : {0}'.format(tvec))
                print('Rotation    : {0}'.format(rvec))
                print('Distance from camera: {0} m'.format(np.linalg.norm(tvec)))
                cv2.aruco.drawAxis(frame,
                                   camera_matrix,
                                   dist_coeff,
                                   rvec,
                                   tvec,
                                   square_length*min(grid_x, grid_y))

        if res2 != 0:
            frame = aruco.drawDetectedCornersCharuco(frame, c_corners, c_ids)

        # Display the resulting frame
        frame = aruco.drawDetectedMarkers(frame, corners, ids)

        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        decimator += 1

    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
