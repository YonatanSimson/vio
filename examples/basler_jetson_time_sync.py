import numpy as np
import os
import re
import matplotlib.pyplot as plt
from sklearn import linear_model


if __name__ == "__main__":
    input_folder = "/home/yonatan/data/temp2"

    # grep "Jetson image trigger time" log.txt > jetson_basler_sync.txt
    time_sync_filename = os.path.join(input_folder, "jetson_basler_sync.txt")

    with open(time_sync_filename) as f:
        lines = f.readlines()
        lines = [line.rstrip("\n") for line in lines]

    jetson_trigger_ts_list = []
    jetson_return_ts_list = []
    basler_ts_list = []
    clks_time_diff_list = []
    basler_fixed_ts_list = []
    for line in lines:
        m = re.match(".* Jetson image trigger time (\d+). Jetson image return time (\d+). Basler timestamp (\d+). Fixed Basler (\d+). clks time diff (\d+)", line)
        jetson_trigger_ts = m.group(1)
        jetson_return_ts = m.group(2)
        basler_ts = m.group(3)
        basler_fixed_ts = m.group(4)
        clks_time_diff = m.group(5)

        jetson_trigger_ts_list.append(np.int64(jetson_trigger_ts))
        jetson_return_ts_list.append(np.int64(jetson_return_ts))
        basler_ts_list.append(np.int64(basler_ts))
        clks_time_diff_list.append(np.int64(clks_time_diff))
        basler_fixed_ts_list.append(np.int64(basler_fixed_ts))

    jetson_trigger_ts_list = np.array(jetson_trigger_ts_list)
    jetson_return_ts_list = np.array(jetson_return_ts_list)
    basler_ts_list = np.array(basler_ts_list)
    clks_time_diff_list = np.array(clks_time_diff_list)
    basler_fixed_ts_list = np.array(basler_fixed_ts_list)

    a = (basler_ts_list - jetson_trigger_ts_list) / 1e6
    a_min = a.min()

    plt.figure()
    plt.plot(a - a_min)
    plt.ylabel("[msec]")
    plt.title("basler_ts - jetson_trigger_ts")

    plt.figure()
    plt.plot(np.diff(jetson_trigger_ts_list) / 1e6)
    plt.ylabel("[msec]")
    plt.title("jetson_trigger_ts jitter")

    plt.figure()
    plt.plot(np.diff(jetson_return_ts_list) / 1e6)
    plt.ylabel("[msec]")
    plt.title("jetson_return_ts jitter")

    plt.figure()
    plt.plot(np.diff(basler_ts_list) / 1e6)
    plt.ylabel("[msec]")
    plt.title("basler_ts jitter")

    plt.figure()
    plt.plot(clks_time_diff_list.astype(np.float64) / 1e6)
    plt.ylabel("[mili sec]")
    plt.title("Jetson basler timediffs")

    plt.figure()
    plt.plot(np.diff(clks_time_diff_list.astype(np.float64) / 1e6))
    plt.ylabel("[msec]")
    plt.title("Jetson basler timediffs - spikes")

    plt.figure()
    plt.plot(np.diff(basler_fixed_ts_list) / 1e6)
    plt.ylabel("[msec]")
    plt.title("Fixed basler ts")

    # Create linear regression object
    regr = linear_model.LinearRegression()

    # Train the model using the training sets
    regr.fit(jetson_trigger_ts_list.reshape(-1, 1), basler_ts_list.reshape(-1, 1))

    # Make predictions using the testing set
    sys_clk_array = regr.predict(jetson_return_ts_list.reshape(-1, 1)).astype(np.int64)

    plt.show()

    print("")
