import numpy as np
import matplotlib.pyplot as plt
import os
import re
import json
import yaml
from shutil import copyfile
from scipy.spatial.transform import Rotation

if __name__ == "__main__":
    # kalibr_calibration_folder = '/home/yonatan/data/complemetary_filter/run_#18/kalibr_format'
    # output_folder = '/home/yonatan/data/complemetary_filter/run_#18'
    # run_id = 18
    kalibr_calibration_folder = '/home/yonatan/data/madgwick_filter/run_#47'
    cam_imu_calibration_yaml = os.path.join(kalibr_calibration_folder, 'imu_camera_extrinsics.yaml')
    output_folder = '/home/yonatan/data/madgwick_filter/run_#47'
    bin_camera_intrinsics = 1.

    print(f"imu+cam: {cam_imu_calibration_yaml}")
    # print(f"cam: {camera_calibration_yaml}")

    with open(cam_imu_calibration_yaml) as f:
        cam_imu_calibration = yaml.load(f, Loader=yaml.FullLoader)

    # Intrinsics - TODO move to separate script
    if bin_camera_intrinsics > 1.:
        # with open(os.path.join(kalibr_calibration_folder, camera_intrinsics_yaml)) as f:
        #     cam_intrinsic_calibration = yaml.load(f, Loader=yaml.FullLoader)

        fx, fy, cx, cy = cam_imu_calibration['cam0']['intrinsics']
        image_width, image_height = cam_imu_calibration['cam0']['resolution']

        cam_imu_calibration['cam0']['intrinsics'] = [fx / bin_camera_intrinsics,
                                                     fy / bin_camera_intrinsics,
                                                     cx / bin_camera_intrinsics,
                                                     cy / bin_camera_intrinsics]

        cam_imu_calibration['cam0']['resolution'] = [int(image_width / bin_camera_intrinsics),
                                                     int(image_height / bin_camera_intrinsics)]

        with open(os.path.join(output_folder, 'cam0.yaml'), 'w') as f:
            yaml.dump(cam_imu_calibration, f)

        print(cam_imu_calibration)

    ##############################
    # extrinsics   ###############
    ##############################
    imu_calibration_cam0 = cam_imu_calibration['cam0']
    # invert and convert to quaternion
    T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])

    R_cam_imu = Rotation.from_matrix(T_cam_imu[:3, :3])
    t_cam_imu = T_cam_imu[:3, 3]
    qCM = R_cam_imu.as_quat()  # quaternion from IMU to camera frame, Hamilton-convention
    MrMC = -T_cam_imu[:3, :3].T @ t_cam_imu  # Translation between IMU and Camera expressed in the IMU frame

    # Make Json format for extrinsics
    imu_cam_dict = {"Camera0":
        [
            f"    CalibrationFile ;                                Camera-Calibration file for intrinsics",
            f"    qCM_x  {qCM[0]};                                 X-entry of IMU to Camera quaterion (Hamilton)",
            f"    qCM_y  {qCM[1]};                                 Y-entry of IMU to Camera quaterion (Hamilton)",
            f"    qCM_z  {qCM[2]};                                 Z-entry of IMU to Camera quaterion (Hamilton)",
            f"    qCM_w  {qCM[3]};                                 W-entry of IMU to Camera quaterion (Hamilton)",
            f"    MrMC_x {MrMC[0]};                                X-entry of IMU to Camera vector (expressed in IMU CF) [m]",
            f"    MrMC_y {MrMC[1]};                                Y-entry of IMU to Camera vector (expressed in IMU CF) [m]",
            f"    MrMC_z {MrMC[2]};                                Z-entry of IMU to Camera vector (expressed in IMU CF) [m]",
        ]}
    print("\n".join(imu_cam_dict["Camera0"]))

    imu_cam_dict = {
        "Camera0":
        {
            "CalibrationFile": "",
            "qCM_x": qCM[0],
            "qCM_y": qCM[1],
            "qCM_z": qCM[2],
            "qCM_w": qCM[3],
            "MrMC_x": MrMC[0],
            "MrMC_y": MrMC[1],
            "MrMC_z": MrMC[2]
        }
    }
    with open(os.path.join(output_folder, 'imu_cam.json'), 'w') as f:
        json.dump(imu_cam_dict, f, indent=4)
    # Make yaml config for intrinsics
    fx, fy, cx, cy = cam_imu_calibration['cam0']['intrinsics']
    image_width, image_height = cam_imu_calibration['cam0']['resolution']
    distortion_model = cam_imu_calibration['cam0']['distortion_model']

    cam_config = {
        'cam_overlaps': [],
        'camera_model': 'pinhole',
        'distortion_coefficients':
            {
                'cols': 5,
                'data': cam_imu_calibration['cam0']['distortion_coeffs'] + [0.],
                'rows': 1
            },
        # ROVIO call radtan 'plumb_bob'
        'distortion_model': 'plumb_bob' if distortion_model == 'radtan' else distortion_model,
        'camera_matrix':
            {
                'cols': 3,
                'data': [fx, 0, cx, 0, fy, cy, 0, 0, 1],
                'rows': 3,
            },
        'image_height': image_height,
        'image_width': image_width,
    }

    with open(os.path.join(output_folder, 'camera_config.yaml'), 'w') as f:
        yaml.dump(cam_config, f)

    output_extrinsics_yaml = os.path.join(output_folder, "imu_camera_extrinsics.yaml")
    if cam_imu_calibration_yaml != output_extrinsics_yaml:
        copyfile(cam_imu_calibration_yaml,
                 output_extrinsics_yaml)
    #
    # copyfile(imu_intrinsics_yaml, os.path.join(output_folder, "imu_intrinsics.yaml"))

    print("")
