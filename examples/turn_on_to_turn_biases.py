import os
from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sophus as sp
from pyquaternion import Quaternion

from imu_integration.gyro_integration import quat_integration_step_rk4
from readers.sightec_data_reader import DataReader
from rig.imu import IMU
from sensors.imu_config import IMUIntrinsics


if __name__ == "__main__":
    base_folder = '/home/yonatan/data/imu_calibration/turn_2_turn_on_bias'
    subfolders = os.listdir(base_folder)

    df_t2t_bias = pd.DataFrame(columns=['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]'])
    for folder_name in subfolders:
        print(folder_name)
        data_folder = os.path.join(base_folder, folder_name)
        gyro_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_gyro.calib"
        accel_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_acc.calib"
        reader = DataReader(data_folder)

        acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
        gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
        ts = acc.index.values
        print(acc.mean())
        df_t2t_bias.loc[folder_name] = acc.mean()

        # Visualize IMU
        fig, axes = plt.subplots(2, 1, sharex=True)
        axes[0].plot(ts, gyro.values)
        axes[0].set_title("Angvel [rad/s]")
        axes[0].set_ylim(-np.pi, np.pi)

        axes[1].plot(ts, acc.values)
        axes[1].set_title("RawAccel [m/s2]")
        axes[1].set_ylim(-15., 15.)

        plt.tight_layout()

    df_t2t_bias["G_norm"] = np.sqrt(np.square(df_t2t_bias).sum(axis=1))
    print(df_t2t_bias)
    print("Done")
