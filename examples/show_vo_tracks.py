import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


if __name__ == "__main__":
    v1_filename = "/home/yonatan/output/sightec_vo/hod_hasharon_run8/v1/live_tracks.csv"
    v2_filename ="/home/yonatan/output/sightec_vo/hod_hasharon_run8/v2/live_tracks.csv"
    v3_filename = "/home/yonatan/output/sightec_vo/hod_hasharon_run8/v3/live_tracks.csv"
    v3_filename = "/home/yonatan/output/sightec_vo/hod_hasharon_run8/v5/live_tracks.csv"
    df_v1 = pd.read_csv(v1_filename, header=None, index_col=[0], names=["number_of_tracks"])
    df_v2 = pd.read_csv(v2_filename, header=None, index_col=[0], names=["number_of_tracks"])
    df_v3 = pd.read_csv(v3_filename, header=0, index_col=[0], names=["number_of_tracks"])
    df_v5 = pd.read_csv(v3_filename, header=0, index_col=[0], names=["number_of_tracks"])

    plt.figure()
    plt.plot(df_v1.index.values, df_v1["number_of_tracks"].values, label="Single level search")
    plt.plot(df_v2.index.values, df_v2["number_of_tracks"].values, label="Multi-level search")
    plt.plot(df_v3.index.values, df_v3["number_of_tracks"].values, label="Prediction bug fixed")
    plt.plot(df_v5.index.values, df_v5["number_of_tracks"].values, label="Glitches fixed")

    plt.legend()
    # df.plot(xlabel="#frame")
    plt.show()
    print("Done")
