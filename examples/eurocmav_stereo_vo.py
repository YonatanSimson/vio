import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import cv2 as cv

from readers.euroc_mav_reader import EurocMavReader
from utils.common import stereo_triangulation_midpoint
from features.feature_descriptor import FeatureDescriptor
from features.feature_matcher import FrameToFrameMatcher
from vo.rig_frame import RigFrame


if __name__ == "__main__":
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)
    feature_descriptor = FeatureDescriptor()

    timestamp_idx = 23
    cam_timestamps = reader.get_image_timestamps()

    timestamp0 = cam_timestamps[timestamp_idx]
    images_a = reader.read_images(timestamp0)

    rig_T_world_a = reader.calc_gt_rig_pose(timestamp0)   # world to slam transform
    reader.rig.set_rig_pose(rig_T_world_a)

    rig_frame_a = RigFrame(timestamp0, reader.rig, save_image=True)
    rig_frame_a.process_images(images_a, feature_descriptor)   # Detect features

    matcher = FrameToFrameMatcher()
    # Match features
    points0, points1 = matcher.stereo_match(rig_frame_a.frames[0], rig_frame_a.frames[1], debug=True)

    # Triangulate points
    points_3d, residuals = stereo_triangulation_midpoint(points0, points1, reader.rig.cameras)

    # For second timestamp get rig-frame as well
    timestamp1 = cam_timestamps[timestamp_idx + 1]
    images_b = reader.read_images(timestamp1)
    rig_T_world_b = reader.calc_gt_rig_pose(timestamp1)  # world to slam transform
    reader.rig.set_rig_pose(rig_T_world_b)

    rig_frame_b = RigFrame(timestamp1, reader.rig, save_image=True)
    rig_frame_b.process_images(images_b, feature_descriptor)   # Detect features

    ######   Now do LK between rig_frames  #######
    # Parameters for lucas kanade optical flow
    lk_params = dict(winSize=(15, 15),
                     maxLevel=2,
                     criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))

    # Create some random colors
    color = np.random.randint(0, 255, (100, 3))
    # Create a mask image for drawing purposes
    h, w = images_a[0].shape
    mask = np.zeros((h, w, 3), dtype=np.uint8)

    # calculate optical flow
    p0 = points0.reshape(-1, 1, 2)
    p1, st, err = cv.calcOpticalFlowPyrLK(images_a[0], images_b[0], p0, None, **lk_params)
    p0r, _st, _err = cv.calcOpticalFlowPyrLK(images_b[0], images_a[0], p1, None, **lk_params)
    rgb = cv.cvtColor(images_b[0], cv.COLOR_GRAY2BGR)

    d = abs(p0 - p0r).reshape(-1, 2).max(-1)
    good = d < 1

    # Select good points
    p2d_good_new = p1[good]
    p2d_good_old = p0[good]
    # draw the tracks
    for i, (new, old) in enumerate(zip(p2d_good_new, p2d_good_old)):
        a, b = new.astype(np.int32).ravel()
        c, d = old.astype(np.int32).ravel()
        mask = cv.line(mask, (a, b), (c, d), color[i].tolist(), 2, cv.LINE_AA)
        rgb = cv.circle(rgb, (a, b), 5, color[i].tolist(), -1, cv.LINE_AA)

    img = cv.add(rgb, mask)

    plt.figure()
    plt.imshow(img)
    plt.show()

    # Solve PnP problem
    # Given 3D points and matching 2D points find the pose of the camera (rvec, tvec)
    p3d_good = points_3d[good]
    cam = reader.rig.cameras[0]

    # Find the rotation and translation vectors.
    ret, rvecs, tvecs = cv.solvePnP(p3d_good, p2d_good_new, cam.K, cam.dist_coeffs)
    pose_gt = rig_T_world_b
    print(f"tvecs: {tvecs}, rvecs: {rvecs}")
    rvec_, J = cv.Rodrigues(cam.R)
    print(f"t: {cam.trans}, rvec_: {rvec_}")
