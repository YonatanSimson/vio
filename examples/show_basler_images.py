from readers.sightec_data_reader import DataReader
import cv2 as cv


if __name__ == "__main__":
    calibration_folder = "/home/yonatan/data/imu_basler_calib/26_09_2021/run_#46"
    reader = DataReader(calibration_folder)

    image_ts = reader.get_image_timestamps()
    for ts in image_ts:
        images, = reader.read_images(ts)
        if images is None:
            break
        cv.imshow('frame', images)
        cv.waitKey(1)
