from copy import deepcopy

import matplotlib.pyplot as plt
import numpy as np
import sophus as sp
from pyquaternion import Quaternion

from imu_integration.gyro_integration import quat_integration_step_rk4
from readers.sightec_data_reader import DataReader
from rig.imu import IMU
from sensors.imu_config import IMUIntrinsics


if __name__ == "__main__":
    data_folder = '/home/yonatan/data/imu_calibration/repeatability/run_#6' #2/run_#20'
    gyro_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_gyro.calib"
    accel_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_acc.calib"
    reader = DataReader(data_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = acc.index.values

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, gyro.values)
    axes[0].set_title("Angvel [rad/s]")
    axes[0].set_ylim(-np.pi, np.pi)

    axes[1].plot(ts, acc.values)
    axes[1].plot([ts[200], ts[200]], [-15., 15.], "k")
    axes[1].plot([ts[4200], ts[4200]], [-15., 15.], "k")
    axes[1].set_title("RawAccel [m/s2]")
    axes[1].set_ylim(-15., 15.)

    plt.tight_layout()

    # Load IMU_TK calibration
    imu_intrinsics = IMUIntrinsics(gyro_calib_fn=gyro_calibration, acc_calib_fn=accel_calibration)

    #  Correct calibration
    imu = IMU(gyro_calib_fn=gyro_calibration, acc_calib_fn=accel_calibration)
    gyro_corrected, acc_corrected = imu.calibrate_raw_input(gyro.values, acc.values)

    # Now integrate
    # initial orientation
    w_b = np.zeros((3,))
    q0 = Quaternion()  # Identity quaternion
    r = sp.SO3()
    q = deepcopy(q0)
    q_array = []
    euler_array = []
    position_array = []
    g0 = -acc_corrected[:200, :].mean(axis=0)
    v0 = np.zeros((3,))
    p0 = np.zeros((3,))

    v = deepcopy(v0)
    p = deepcopy(p0)

    # g0_ = acc.values.mean(axis=0)
    for idx, (w, a) in enumerate(zip(gyro_corrected[:-1, :], acc_corrected[:-1, :])):
        dt = (gyro.iloc[idx + 1].name - gyro.iloc[idx].name) * 1e-6
        dv = (q.rotation_matrix @ a + g0) * dt
        p = p + v * dt + 0.5 * dv * dt
        v = v + dv

        w_plus = gyro_corrected[idx]
        q = quat_integration_step_rk4(q, w, w_plus, dt)

        q_array.append(q)
        euler_array.append(q.yaw_pitch_roll)
        position_array.append(p)

    euler_array = np.array(euler_array)
    position_array = np.array(position_array)

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot((ts[:-1] - ts[0]) * 1e-6, np.rad2deg(euler_array))
    axes[0].set_title("Orientation from calibrated gyro")
    axes[0].set_ylabel("[deg]")
    axes[0].set_xlabel("[sec]")

    axes[1].plot((ts[:-1] - ts[0]) * 1e-6, position_array)
    axes[1].set_title("Position from calibrated accelerometer")
    axes[1].set_ylabel("[m]")
    axes[1].set_xlabel("[sec]")

    plt.tight_layout()
    plt.show()
    print("Done")
