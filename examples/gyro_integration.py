"""
Check gyro integration between two image points
"""

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import cv2 as cv

from readers.euroc_mav_reader import EurocMavReader
from features.feature_descriptor import FeatureDescriptor


if __name__ == "__main__":
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)
    feature_descriptor = FeatureDescriptor()

    timestamp_idx = 23
    cam_timestamps = reader.get_image_timestamps()

    timestamp0 = cam_timestamps[timestamp_idx]
    images_a = reader.read_images(timestamp0)

    rig_T_world_a = reader.calc_gt_rig_pose(timestamp0)   # world to slam transform
    reader.rig.set_rig_pose(rig_T_world_a)

    # For second timestamp get gt pose as well
    timestamp1 = cam_timestamps[timestamp_idx + 1]
    images_b = reader.read_images(timestamp1)
    rig_T_world_b = reader.calc_gt_rig_pose(timestamp1)  # world to slam transform

