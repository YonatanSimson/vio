import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import cv2 as cv
import os
import pandas as pd
import plotly.express as px

from readers.euroc_mav_reader import EurocMavReader
from utils.common import stereo_triangulation_midpoint
from features.feature_descriptor import FeatureDescriptor
from features.feature_matcher import FrameToFrameMatcher
from vo.rig_frame import RigFrame
from utils.align_3d import ralign


def rmse(err):
    sq_err = err * err
    mean_sq_err = np.mean(sq_err)
    return np.sqrt(mean_sq_err)


if __name__ == "__main__":
    output_folder = '/home/yonatan/output/MH_01_easy'
    filepath = '/home/yonatan/data/euroc_mav/MH_01_easy/mav0'
    reader = EurocMavReader(filepath)
    feature_descriptor = FeatureDescriptor()

    cam_timestamps = reader.get_image_timestamps()

    # rig_T_world = reader.calc_gt_rig_pose(cam_timestamps)   # world to slam transform

    # Read Body to world from ROVIO
    vio_path = os.path.join(output_folder, 'WB.csv')
    df_vio = pd.read_csv(vio_path, header=[0])
    df_vio.set_index("#timestamp", inplace=True)

    # get positions
    position = df_vio[[' p_WB_x [m]', ' p_WB_y [m]', ' p_WB_z [m]']]
    q_wxyz = df_vio[[' q_WB_w []', ' q_WB_x []', ' q_WB_y []', ' q_WB_z []']].values

    fig = px.line_3d(position, x=' p_WB_x [m]', y=' p_WB_y [m]', z=' p_WB_z [m]')
    fig.show()

    # display GT path of IMU
    position_gt = reader.df_gt[[" p_RS_R_x [m]", " p_RS_R_y [m]", " p_RS_R_z [m]"]]  # p_RS_R_x [m], p_RS_R_y [m], p_RS_R_z [m], q_RS_w [], q_RS_x [], q_RS_y [], q_RS_z []
    fig = px.line_3d(position_gt, x=' p_RS_R_x [m]', y=' p_RS_R_y [m]', z=' p_RS_R_z [m]')
    fig.show()

    # Match by timestamps
    vio_ts = position.index.values
    gt_ts = position_gt.index.values

    pos_gt_interp = np.zeros((len(vio_ts), 3))
    for n in range(3):
        pos_gt_interp[:, n] = np.interp(vio_ts, gt_ts, position_gt.values[:, n], left=np.nan, right=np.nan)

    nan_list = np.isnan(pos_gt_interp).any(axis=1)

    pos_gt_interp = pos_gt_interp[~nan_list]
    position.drop(labels=vio_ts[nan_list], axis=0, inplace=True)
    df_position_gt = pd.DataFrame(pos_gt_interp, columns=[" p_RS_R_x [m]", " p_RS_R_y [m]", " p_RS_R_z [m]"], index=vio_ts[~nan_list])

    R, c, t = ralign(position.values.T, df_position_gt.values.T)
    Y_est = (c * R @ position.values.T + t[:, np.newaxis]).T

    np.linalg.norm(Y_est - df_position_gt.values, axis=1)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(df_position_gt[' p_RS_R_x [m]'].values,
            df_position_gt[' p_RS_R_y [m]'].values,
            df_position_gt[' p_RS_R_z [m]'].values,
            label='Ground truth')
    ax.plot(Y_est[:, 0],
            Y_est[:, 1],
            Y_est[:, 2],
            label='Aligned ROVIO output Ground truth')
    ax.legend()

    # Absolute translation error
    ate = rmse(np.linalg.norm(Y_est - df_position_gt.values, axis=1))
    print("Done")
