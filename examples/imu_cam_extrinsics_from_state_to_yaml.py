import cv2 as cv
import numpy as np
import os
import matplotlib.pyplot as plt
import yaml
import pandas as pd
from scipy.spatial.transform import Rotation

from evo.core.trajectory import PoseTrajectory3D, calc_angular_speed

if __name__ == "__main__":
    run_folder = 'run_#95'
    input_folder = os.path.join('/home/yonatan/data/imu_basler_calib/26_09_2021', run_folder)
    output_folder = os.path.join('/home/yonatan/output/sightec_vio/imu_basler_calib_26_09_2021', run_folder + '_v12')

    # Display state log evolution
    df_imu_cam = pd.read_csv(os.path.join(output_folder, 'MC.csv'), header=0)
    df_imu_cam.reset_index(inplace=True)

    stamps = np.divide(df_imu_cam["#timestamp"].values, 1e9)  # n x 1  -  nanoseconds to seconds
    xyz = df_imu_cam[[" p_MC_x [m]", " p_MC_y [m]", " p_MC_z [m]"]].values  # n x 3
    quat = df_imu_cam[[" q_MC_w []", " q_MC_x []", " q_MC_y []", " q_MC_z []"]].values  # n x 4
    traj_mc = PoseTrajectory3D(xyz, quat, stamps)

    poses = traj_mc.poses_se3
    last_pose = poses[-1]
    R_CM = last_pose[:3, :3].T
    MrMC = last_pose[:3, -1]
    qCM = Rotation.from_matrix(R_CM).as_quat()  # xyzw

    # R_imu_cam = last_pose[:3, :3].T
    # T_cam_imu = np.eye(4)
    # T_cam_imu[:3, :3] = R_imu_cam
    # T_cam_imu[:3, -1] = -R_imu_cam @ last_pose[:3, -1]
    data_dict = {"cam0": {"MrMC": MrMC.tolist(), "qCM_xyzw": qCM.tolist()}}
    with open(os.path.join(output_folder, 'imu_camera_rovio_output.yaml'), "w") as f:
        yaml.dump(data_dict, f)

    print("Done")
