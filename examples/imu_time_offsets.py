import numpy as np
from scipy import signal
from scipy import interpolate
import cv2 as cv
import matplotlib.pyplot as plt

from readers.sightec_data_reader import DataReader
from utils.global_shift import GlobalShift

import matplotlib
matplotlib.rcParams['text.usetex'] = True


def correlation_lags(in1_len, in2_len, mode='full'):
    r"""
    Calculates the lag / displacement indices array for 1D cross-correlation.
    Parameters
    ----------
    in1_len : int
        First input size.
    in2_len : int
        Second input size.
    mode : str {'full', 'valid', 'same'}, optional
        A string indicating the size of the output.
        See the documentation `correlate` for more information.
    See Also
    --------
    correlate : Compute the N-dimensional cross-correlation.
    Returns
    -------
    lags : array
        Returns an array containing cross-correlation lag/displacement indices.
        Indices can be indexed with the np.argmax of the correlation to return
        the lag/displacement.
    Notes
    -----
    Cross-correlation for continuous functions :math:`f` and :math:`g` is
    defined as:
    .. math ::
        \left ( f\star g \right )\left ( \tau \right )
        \triangleq \int_{t_0}^{t_0 +T}
        \overline{f\left ( t \right )}g\left ( t+\tau \right )dt
    Where :math:`\tau` is defined as the displacement, also known as the lag.
    Cross correlation for discrete functions :math:`f` and :math:`g` is
    defined as:
    .. math ::
        \left ( f\star g \right )\left [ n \right ]
        \triangleq \sum_{-\infty}^{\infty}
        \overline{f\left [ m \right ]}g\left [ m+n \right ]
    Where :math:`n` is the lag.
    Examples
    --------
    Cross-correlation of a signal with its time-delayed self.
    >>> from scipy import signal
    >>> from numpy.random import default_rng
    >>> rng = default_rng()
    >>> x = rng.standard_normal(1000)
    >>> y = np.concatenate([rng.standard_normal(100), x])
    >>> correlation = signal.correlate(x, y, mode="full")
    >>> lags = signal.correlation_lags(x.size, y.size, mode="full")
    >>> lag = lags[np.argmax(correlation)]
    """

    # calculate lag ranges in different modes of operation
    if mode == "full":
        # the output is the full discrete linear convolution
        # of the inputs. (Default)
        lags = np.arange(-in2_len + 1, in1_len)
    elif mode == "same":
        # the output is the same size as `in1`, centered
        # with respect to the 'full' output.
        # calculate the full output
        lags = np.arange(-in2_len + 1, in1_len)
        # determine the midpoint in the full output
        mid = lags.size // 2
        # determine lag_bound to be used with respect
        # to the midpoint
        lag_bound = in1_len // 2
        # calculate lag ranges for even and odd scenarios
        if in1_len % 2 == 0:
            lags = lags[(mid-lag_bound):(mid+lag_bound)]
        else:
            lags = lags[(mid-lag_bound):(mid+lag_bound)+1]
    elif mode == "valid":
        # the output consists only of those elements that do not
        # rely on the zero-padding. In 'valid' mode, either `in1` or `in2`
        # must be at least as large as the other in every dimension.

        # the lag_bound will be either negative or positive
        # this let's us infer how to present the lag range
        lag_bound = in1_len - in2_len
        if lag_bound >= 0:
            lags = np.arange(lag_bound + 1)
        else:
            lags = np.arange(lag_bound, 1)
    return lags


if __name__ == "__main__":
    # data_folder = '/home/yonatan/data/TalmeiYosef/run_#21'
    data_folder = '/home/yonatan/data/Hod-Hasharon/17_08_2021/run_#5'
    data_folder = '/home/yonatan/data/max_test'
    data_folder = '/home/yonatan/data/imu_basler_calib/22_09_2021/run_#37'
    data_folder = '/home/yonatan/data/imu_basler_calib/26_09_2021/run_#40'
    reader = DataReader(data_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = acc.index.values
    ts = (ts - ts[0]) * 1e-6

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, np.linalg.norm(acc.values, axis=1))
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-20., 20.)
    axes[1].plot(ts, np.linalg.norm(np.rad2deg(gyro.values), axis=1))
    axes[1].set_title("Angvel [deg/s]")
    axes[1].set_ylim(-100., 100.)
    plt.tight_layout()

    fig, axes = plt.subplots(1, 1)
    axes.plot(ts[:-1], 1e3 * np.diff(ts))
    axes.set_ylabel('[msec]')
    axes.set_title("sampling delta")

    fs = 1 / np.median(np.diff(ts))
    print(f"IMU fps: {fs} [Hz]")

    # Histogram of IMU timestamps
    plt.figure()
    plt.hist(np.diff(ts) * 1e3, 50)
    plt.title("IMU sampling times histogram")
    plt.xlabel(r"$\delta t [ms]$")
    print("")

    ts_jetson = reader.imu_data['nav-time [sec]'].values
    fs_jetson = 1 / np.median(np.diff(ts_jetson))
    print(f"IMU print fps: {fs} [Hz]")

    acc_zero_mean = acc - acc.mean().values
    acc_norm = np.linalg.norm(acc_zero_mean, axis=1)
    mu_acc = acc_norm.mean()

    gyro_zero_mean = gyro - gyro.mean().values
    gyro_norm = np.linalg.norm(gyro_zero_mean, axis=1)
    mu_gyro = gyro_norm.mean()

    corr = signal.correlate(acc_norm - mu_acc, gyro_norm - mu_gyro, mode='same')

    lags = correlation_lags(len(acc), len(gyro), mode='same')
    lag = lags[np.argmax(corr)]

    plt.figure()
    plt.plot(lags, corr)
    plt.title("Cross correlation between gyro and accelerometer")

    #  Image offsets
    image_ts = reader.get_image_timestamps()
    for ts in image_ts:
        images, = reader.read_images(ts)
        cv.imshow('frame', images)
        cv.waitKey(1)
    global_shift = GlobalShift()

    prev_img = None
    xy_array = []
    for ts_frame in image_ts:
        curr_img, = reader.read_images(ts_frame)
        if prev_img is not None:
            shift_xy, conf = global_shift.calculate(prev_img, curr_img)
            xy_array.append((ts_frame, shift_xy))
        prev_img = np.copy(curr_img)

    xy_array = np.array(xy_array)

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts_jetson, acc_norm)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-20., 20.)

    axes[1].plot(xy_array[:, 0] * 1e-6, np.linalg.norm(np.stack(xy_array[:, 1]), axis=1))
    axes[1].set_title("image xy [pixel]")

    # Interpolate image spikes
    shift_xy = np.stack(xy_array[:, 1])
    shift_xy_zero_mean = shift_xy - shift_xy.mean(axis=0)
    shift_xy_norm = np.linalg.norm(shift_xy_zero_mean, axis=1)

    f = interpolate.interp1d(xy_array[:, 0] * 1e-6, shift_xy_norm, fill_value=np.nan, bounds_error=False)
    shift_xy_norm_interp = f(ts_jetson)
    shift_xy_norm_interp = shift_xy_norm_interp.astype(np.float)
    nan_index = np.isnan(shift_xy_norm_interp)

    shift_xy_norm_interp = shift_xy_norm_interp[~nan_index]
    acc_norm_interp = acc_norm[~nan_index]

    mu_shift_xy_interp = shift_xy_norm_interp.mean()
    mu_acc_norm_interp = acc_norm_interp.mean()

    corr_imu_image = signal.correlate(acc_norm_interp - mu_acc_norm_interp, shift_xy_norm_interp - mu_shift_xy_interp, mode='same')

    lags_imu_image = correlation_lags(len(acc_norm_interp), len(shift_xy_norm_interp), mode='same')
    lag_imu_image = lags_imu_image[np.argmax(corr_imu_image)]

    plt.figure()
    plt.plot(lags_imu_image, corr_imu_image)
    plt.title("Cross correlation between image and accelerometer")

    warp_mode = cv.MOTION_TRANSLATION
