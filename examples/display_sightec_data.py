import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

from readers.sightec_data_reader import DataReader

if __name__ == "__main__":
    data_folder = '/home/yonatan/data/Hod-Hasharon/06_05_2021_Basler/run_8'

    reader = DataReader(data_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = acc.index.values

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-20., 10.)
    axes[1].plot(ts, np.rad2deg(gyro.values))
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    # Visualize images
    image_ts = reader.get_image_timestamps()
    for ts in image_ts:
        images, = reader.read_images(ts)
        cv.imshow('frame', images)
        cv.waitKey(1)

    plt.show()
    print("Done")
