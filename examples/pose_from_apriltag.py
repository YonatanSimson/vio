import numpy as np
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

from evo.tools.file_interface import read_euroc_csv_trajectory

from readers.sightec_data_reader import DataReader
from utils.apriltag_pose import AprilTagPose
import cv2 as cv

from utils.render_3d import render_cube

if __name__ == "__main__":
    tag_cols = 6
    tag_rows = 6
    tag_size = 0.14
    tag_spacing_ratio = 0.021 / 0.14
    show_video = False
    """
    https://www.pyimagesearch.com/2020/11/02/apriltag-with-python/
    """
    # calibration_folder = "/home/yonatan/docker_data/sightec/calaibration/run_#40"
    calibration_folder = "/home/yonatan/data/imu_basler_calib/26_09_2021/run_#95"
    output_gt_pose = os.path.join(calibration_folder, "April_WC_gt.csv")
    reader = DataReader(calibration_folder)

    april_detector = AprilTagPose(K=reader.camera.K, dist_coeffs=reader.camera.dist_coeffs, min_detected_tags=24)

    if show_video:
        cube_size = tag_size
        cube = cube_size * np.float32([[0, 0, 0], [0, 1, 0], [1, 1, 0], [1, 0, 0],
                                       [0, 0, -1], [0, 1, -1], [1, 1, -1], [1, 0, -1]])
        axis = cube_size * np.float32([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, -1]]).reshape(-1, 3)

        image_ts = reader.get_image_timestamps()
        for ts in tqdm(image_ts[::2], "Grid pose"):
            images, = reader.read_images(ts)
            if images is None:
                continue
            rgb = cv.cvtColor(images, cv.COLOR_GRAY2BGR)
            image_points, object_points = april_detector.detect_grid(images)
            # Camera pose w_T_c
            if image_points is not None:
                pos, R = april_detector.calculate_grid_pose(image_points, object_points)
            else:
                pos, R = None, None

            if pos is not None:
                Rt = R.T
                rvec, jac_R = cv.Rodrigues(Rt)

                tvec = -Rt @ pos

                # project 3D cube corners to image plane
                imgpts, jac = cv.projectPoints(cube, rvec, tvec, april_detector.K, april_detector.dist_coeffs)

                rgb = render_cube(rgb, imgpts)

            cv.imshow('frame', rgb)
            cv.waitKey(1)
        cv.destroyAllWindows()

    image_path_list = reader.get_image_list(use_time=True)
    pose_list = april_detector.estimate_poses(image_path_list)
    april_detector.plot_poses_2d(pose_list)
    april_detector.write_pose_list(pose_list, output_gt_pose)

    # Verify by reading back and displaying pose

    gt_results = read_euroc_csv_trajectory(output_gt_pose)
    ts_out = gt_results.timestamps

    xyz_gt = gt_results.positions_xyz
    rot_vec_gt = gt_results.get_rotation_vector()

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts_out, xyz_gt)
    axes[0].set_title("GT XYZ [m]")
    axes[0].legend(['x', 'y', 'z'])
    plt.grid()
    axes[1].plot(ts_out, np.unwrap(rot_vec_gt))
    axes[1].set_title("GT orientation (rot) [rad]")
    axes[1].legend(['x', 'y', 'z'])
    plt.grid()
    plt.tight_layout()

    plt.savefig(os.path.join(calibration_folder, "gt_results.png"))

    # Found outliers
    from scipy.spatial.transform import Rotation
    positions = []
    euler_angles = []
    time_stamps = []
    for ts_clk, W_t_WC, R_WC in pose_list:
        r = Rotation.from_matrix(R_WC)
        euler_angles.append(r.as_euler('xyz'))
        positions.append(W_t_WC)
        time_stamps.append(ts_clk / 1e9)

    positions = np.array(positions)
    euler_angles = np.array(euler_angles)

    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(time_stamps, positions)
    axes[0].set_title("Camera positions")
    axes[0].legend(['x', 'y', 'z'])

    axes[1].plot(time_stamps, euler_angles)
    axes[1].set_title("Camera orientations")
    axes[1].legend(['x', 'y', 'z'])

    norm_positions = np.linalg.norm(positions, axis=1)
    norm_positions_median = np.median(norm_positions)
    outliers_positions = np.where(norm_positions == norm_positions.max())[0]

    try:
        _, img_path = image_path_list[outliers_positions[0]]
        img = cv.imread(img_path, cv.IMREAD_GRAYSCALE)
        plt.figure()
        plt.imshow(img)

        results = april_detector.detect_tags(img)

        print("[INFO] {} total AprilTags detected".format(len(results)))

        fig = plt.figure()
        april_detector.show_tags(results, img, fig)

        image_points, object_points = april_detector.detect_grid(img)
        # Camera pose w_T_c
        pos, R = april_detector.calculate_grid_pose(image_points, object_points)
        success, rvec, tvec = cv.solvePnP(object_points, image_points, april_detector.K, april_detector.dist_coeffs,
                                    flags=cv.SOLVEPNP_IPPE)  # SOLVEPNP_SQPNP, SOLVEPNP_IPPE

        # Calculate projection errors
        projected_grid_points, _ = cv.projectPoints(object_points,
                                                    rvec,
                                                    tvec,
                                                    april_detector.K,
                                                    april_detector.dist_coeffs)
        point_errors = np.linalg.norm(image_points - projected_grid_points.squeeze(), axis=1)
    except Exception as e:
        print(e)
    print("Done")
