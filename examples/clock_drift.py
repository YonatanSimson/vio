import numpy as np
import matplotlib.pyplot as plt
import re

if __name__ == "__main__":

    # string = "2021-08-12 17:07:41.126201 [tid:9117(MainThread)](INFO)[BaslerCamera.cpp:74]: timesync nav 4456800414624. timesync basler letch 1922693385080. diff 2534107029544"
    # m = re.search(".*timesync nav (\d+). timesync basler letch (\d+). diff (\d+)", string)
    #
    # timesync_nav = m.group(1)
    # timesync_basler_letch = m.group(2)
    # diff = m.group(3)
    #
    # print(f"{timesync_nav}, {timesync_basler_letch}, {diff}")
    # print("")

    log_file = "/home/yonatan/sandbox/filtered.log"
    with open(log_file, "r") as f:
        lines = f.read().splitlines()

    timesync_nav = []
    timesync_basler_letch = []
    ts_diff = []
    for line in lines:
        m = re.search(".*timesync nav (\d+). timesync basler letch (\d+). diff (\d+)", line)

        timesync_nav.append(int(m.group(1)))
        timesync_basler_letch.append(int(m.group(2)))
        ts_diff.append(float(m.group(3)))

    timesync_nav = np.array(timesync_nav)
    timesync_basler_letch = np.array(timesync_basler_letch)
    ts_diff = np.array(ts_diff)

    fig, axes = plt.subplots(2, 1)
    axes[0].plot(np.diff(ts_diff))
    axes[1].plot(timesync_nav - timesync_nav[0], label="timesync_nav")
    axes[1].plot(timesync_basler_letch - timesync_basler_letch[0], label="timesync_basler_letch")
    plt.legend()
    plt.show()
    print("")

    plt.figure()
    plt.plot(np.diff(timesync_basler_letch))
    plt.ylabel("nano sec")
    plt.title("Basler clock jitter")

    plt.figure()
    plt.plot(np.diff(timesync_nav))
    plt.ylabel("nano sec")
    plt.title("Jetson clock jitter")

    plt.figure()
    plt.plot(timesync_nav - timesync_basler_letch)
    plt.ylabel("nano sec")
    plt.title("Jetson - Basler clock jitter")

    recording_length = (timesync_nav[-1] - timesync_nav[0])
    clock_drift = (ts_diff[0] - ts_diff[-1])

    recording_length/clock_drift

