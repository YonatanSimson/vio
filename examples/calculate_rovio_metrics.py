import os
import matplotlib.pyplot as plt

from evo.tools import log

from metrics.config import Config as ConfigMetrics
from metrics.metrics import Metrics as MetricsCalculator


# temporarily override some package settings
from evo.tools.settings import SETTINGS
from metrics.plot_errors import error_plotter


if __name__ == "__main__":
    log.configure_logging(verbose=True, debug=True, silent=False)
    #############
    # config ####
    #############
    config = ConfigMetrics()
    SETTINGS.plot_usetex = False
    SETTINGS.plot_pose_correspondences = True

    ######################
    #  Input/Output  #####
    ######################
    reference_path = "/home/yonatan/data/euroc_mav/MH_01_easy/mav0/state_groundtruth_estimate0/data.csv"
    estimate_path = "/home/yonatan/output/MH_01_easy/WB.csv"
    output_folder = "/home/yonatan/metrics/MH_01_easy"

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    ######################
    # Calculate metrics ##
    ######################
    error_metrics = MetricsCalculator(reference_path, estimate_path, config)
    error_metrics.output_statistics_to_file(output_folder)

    #####################
    # Build plots #######
    #####################
    error_plotter(error_metrics, output_folder)
    plt.show()
    print("Done")
