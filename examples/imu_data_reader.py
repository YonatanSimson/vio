import numpy as np
import matplotlib.pyplot as plt
from readers.sightec_data_reader import DataReader
import matplotlib

matplotlib.rcParams['text.usetex'] = True
import cv2 as cv

if __name__ == "__main__":
    # data_folder = '/home/yonatan/data/TalmeiYosef/run_#21'
    data_folder = '/home/yonatan/data/Hod-Hasharon/17_08_2021/run_#5'
    data_folder = '/home/yonatan/data/max_test'
    data_folder = '/home/yonatan/data/imu_basler_calib/22_09_2021/run_#37'
    data_folder = '/home/yonatan/data/imu_basler_calib/26_09_2021/run_#38'
    data_folder = '/home/yonatan/data/imu_basler_calib/26_09_2021/run_#76'
    data_folder = '/home/yonatan/data/Hod-Hasharon/22_10_2021/run_#14'
    data_folder = "/home/yonatan/data/complemetary_filter/21_02_2022/run_#26"
    data_folder = "/home/yonatan/data/madgwick_filter/run_#47"

    reader = DataReader(data_folder)

    acc = reader.imu_data[['RawAccel x', ' RawAccel y', ' RawAccel z [m/s2]']]
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = acc.index.values
    ts = (ts - ts[0]) * 1e-6

    # Visualize IMU
    fig, axes = plt.subplots(2, 1, sharex=True)
    axes[0].plot(ts, acc.values)
    axes[0].set_title("RawAccel [m/s2]")
    axes[0].set_ylim(-20., 20.)
    axes[1].plot(ts, gyro.values)
    axes[1].set_title("Angvel [rad/s]")
    axes[1].set_ylim(-1., 1.)
    plt.tight_layout()

    fig, axes = plt.subplots(1, 1)
    axes.plot(ts[:-1], 1e3 * np.diff(ts))
    axes.set_ylabel('[msec]')
    axes.set_title("sampling delta")

    fs = 1 / np.mean(np.diff(ts))
    print(f"IMU mean fps: {fs} [Hz]")

    fs = 1 / np.median(np.diff(ts))
    print(f"IMU median fps: {fs} [Hz]")

    # Image frame rate
    camera_fps = 1 / np.median(np.diff(reader.image_ts["ts_sec"]))
    print(f"Camera fps: {camera_fps} [Hz]")

    # Histogram of IMU timestamps
    plt.figure()
    plt.hist(np.diff(ts) * 1e3, 50)
    plt.title("IMU sampling times histogram")
    plt.xlabel(r"$\delta t [ms]$")
    print("")

    ts_jetson = reader.imu_data['nav-time [sec]'].values
    fs_jetson = 1 / np.median(np.diff(ts_jetson))
    print(f"IMU print fps: {fs} [Hz]")
    plt.show()

    image_ts = reader.get_image_timestamps()
    for ts in image_ts:
        images, = reader.read_images(ts)
        if images is None:
            break
        cv.imshow('frame', images)
        cv.waitKey(1)
