import numpy as np
import matplotlib.pyplot as plt
import os
from tqdm import tqdm
import shutil


if __name__ == "__main__":

    # input_folder = "/home/yonatan/data/wideTwo/002"
    # new_images_dir = "/home/yonatan/data/wide_angle_calibration/BaslerWide_40134611/cam0"
    input_folder = "/home/yonatan/data/wideTwo/003"
    new_images_dir = "/home/yonatan/data/wide_angle_calibration/BaslerWide_40134616/cam0"

    image_list = os.listdir(input_folder)

    image_time = 0
    for filename in image_list:

        curr_image_time_stamp = round(1e11 + 1e9 * image_time)
        png_filename = f"{curr_image_time_stamp}.png"
        png_path = os.path.join(new_images_dir, png_filename)

        shutil.copyfile(os.path.join(input_folder, filename), png_path)

        image_time += 1 / 30.