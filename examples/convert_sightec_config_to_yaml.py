import yaml
import os

from readers.sightec_data_reader import DataReader

if __name__ == "__main__":
    data_folder = '/home/yonatan/data/Hod-Hasharon/06_05_2021_Basler/run_8'
    gyro_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_gyro.calib"
    accel_calibration = "/home/yonatan/data/imu_calibration/calibration/test_imu_acc.calib"
    reader = DataReader(data_folder)

    camera_config = reader.camera
    # Convert Camera config to YAML in thi following format
    yaml_config_dict = {
    'image_width': camera_config.size[0].item(),
    'image_height': camera_config.size[1].item(),
    'camera_name': 'cam0',
    'camera_matrix':
        {
            'rows': 3,
            'cols': 3,
            'data': camera_config.K.ravel().tolist(),
        },
    'distortion_model': 'plumb_bob',
    'distortion_coefficients':
        {
            'rows': 1,
            'cols': 5,
            'data': camera_config.dist_coeffs.tolist(),
        }
    }
    camera_calib_yaml = os.path.join(data_folder, 'camera_config.yaml')
    with open(camera_calib_yaml, 'w') as f:
        yaml.dump(yaml_config_dict, f)

    print("Done")



