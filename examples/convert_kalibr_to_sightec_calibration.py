import numpy as np
import os
import json
import yaml


if __name__ == "__main__":
    cam_imu_calibration_yaml = '/home/yonatan/data/complemetary_filter/21_02_2022/run_#29/cam_imu_config.yaml'
    output_folder = '/home/yonatan/data/complemetary_filter/21_02_2022/run_#29'
    ScaleNav = 2
    ##############################
    # extrinsics   ###############
    ##############################
    with open(cam_imu_calibration_yaml) as f:
        cam_imu_calibration = yaml.load(f, Loader=yaml.FullLoader)
    imu_calibration_cam0 = cam_imu_calibration['cam0']
    # invert and convert to quaternion
    T_cam_imu = np.array(imu_calibration_cam0['T_cam_imu'])
    timeshift_cam_imu = imu_calibration_cam0["timeshift_cam_imu"]

    # Make yaml config for intrinsics
    fx, fy, cx, cy = cam_imu_calibration['cam0']['intrinsics']
    image_width, image_height = cam_imu_calibration['cam0']['resolution']
    distortion_model = cam_imu_calibration['cam0']['distortion_model']
    distortion_coeffs = cam_imu_calibration['cam0']['distortion_coeffs']
    k1, k2, p1, p2 = distortion_coeffs
    cam_config = {
        "focalX": fx * ScaleNav,
        "focalY": fy * ScaleNav,
        "ppx": cx * ScaleNav,
        "ppy": cy * ScaleNav,
        "DayNight": "Day",
        "k1": k1,
        "k2": k2,
        "r1": p1,
        "r2": p2,
        "k3": 0.,
        "Serial": "40080457",
        "Color": "Monochrome",
        "Name": "Basler",
        "ResX": image_width * ScaleNav,
        "ResY": image_height * ScaleNav,
        "ScaleNav": ScaleNav,
        "DistortionModel": "Radtan",
        "SLAMrow": 480,
        "SLAMcol": 480,
        "CameraAngle": 72
    }
    imu_cam_config = {
        "T_cam_imu": T_cam_imu.ravel().tolist(),
        "timeshift_cam_imu": timeshift_cam_imu
    }

    with open(os.path.join(output_folder, "basler_daa1920-160um_kalibr.json"), "w") as f:
        json.dump(cam_config, f, indent=4)

    with open(os.path.join(output_folder, "cam_imu_config.json"), "w") as f:
        json.dump(imu_cam_config, f, indent=4)

    print("")
