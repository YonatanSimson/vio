import os

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from readers.sightec_data_reader import DataReader

if __name__ == "__main__":
    input_folder = "/home/yonatan/data/palmahim/21_05_2021_basler/run_#9"
    v0_folder = "/home/yonatan/output/sightec_vo/palmahim_run8/v0"
    v1_folder = "/home/yonatan/output/sightec_vo/palmahim_run8/v1"
    live_tracks_path0 = os.path.join(v0_folder, "live_tracks.csv")
    live_tracks_path1 = os.path.join(v1_folder, "live_tracks.csv")
    average_shift_path = os.path.join(v0_folder, "average_shift.csv")
    reader = DataReader(input_folder)

    df_live_tracks0 = pd.read_csv(live_tracks_path0, header=0, index_col=[0], names=["number_of_tracks"])
    df_live_tracks1 = pd.read_csv(live_tracks_path1, header=0, index_col=[0], names=["number_of_tracks"])

    df_average_shift = pd.read_csv(average_shift_path, index_col=[0])
    gyro = reader.imu_data[['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
    ts = gyro.index.values

    plt.figure()
    plt.plot(df_live_tracks0.index.values, df_live_tracks0["number_of_tracks"].values, label="Before glitch fix")
    plt.plot(df_live_tracks1.index.values, df_live_tracks1["number_of_tracks"].values, label="After glitch fix")
    plt.legend()

    fig, axes = plt.subplots(3, 1)
    axes[0].plot(df_live_tracks1.index.values / 15., df_live_tracks1["number_of_tracks"].values, label="number_of_tracks")
    axes[0].set_title("Number of tracks")
    axes[0].set_xlabel("#Frame")

    y = np.linalg.norm(df_average_shift / 15., axis=1)
    axes[1].plot(df_average_shift.index.values, y, label="norm of average shift")
    axes[1].set_title("norm of average shift")
    axes[1].set_xlabel("#Frame")

    axes[2].plot((ts - ts[0]) * 1e-6, np.rad2deg(gyro.values))
    axes[2].set_title("Angvel [rad/s]")
    axes[2].set_ylim(-np.pi, np.pi)
    axes[1].set_xlabel("[sec]")

    plt.legend()
    plt.tight_layout()
    # df.plot(xlabel="#frame")
    plt.show()
    print("Done")
