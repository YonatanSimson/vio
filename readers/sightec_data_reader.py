import os
from collections import OrderedDict
from typing import Tuple, List, Optional
from warnings import warn
from sklearn import linear_model
import cv2 as cv
import numpy as np
import pandas as pd
from scipy import interpolate
from rig.camera import Camera
from rig.rig import Rig
import json
import yaml
import sophus as sp


"""
TODO Both sightec and Euroc MAV data readers should inherit from a common base class
"""


class DataReader(object):
    data_folder: str
    image_ts: Optional[pd.DataFrame] = None
    Rig: Optional[Rig]
    imu_data: pd.DataFrame
    system_clk_to_sec: pd.DataFrame = None
    camera: Optional[Camera] = None

    def __init__(self,
                 data_folder: str,
                 camera_intrinsics_config: Optional[str] = None,
                 rig_extrinsics_config: str = "imu_camera_extrinsics.yaml") -> None:
        self.data_folder = data_folder

        try:
            image_ts_csv = os.path.join(self.data_folder, 'DataFeederLog.csv')
            image_ts_df = pd.read_csv(image_ts_csv, on_bad_lines='skip', header=None)
            image_ts_df = image_ts_df.loc[image_ts_df[0] != 'navigation-time']
            image_ts_df = image_ts_df.loc[image_ts_df[1] == 'FRAME_ACQUIRED']
            image_ts_df.set_index(2, inplace=True)
            image_ts_df = image_ts_df.apply(pd.to_numeric, errors='ignore')
            image_ts_df = image_ts_df.astype({0: float})
            image_ts_df.index = pd.to_numeric(image_ts_df.index, errors='coerce')

            image_ts_df[2] = image_ts_df[0].apply(lambda x: np.int64(x * 1e6))
            self.image_ts = image_ts_df[[0, 2]]
        except Exception as e:
            warn(f"Can't read image timestamp data from {e}")
            self.image_ts = None

        try:
            camera_config_fn = os.path.join(self.data_folder, 'camera_config.yaml')
            if not os.path.isfile(camera_config_fn):
                camera_config_fn = os.path.join(self.data_folder, 'camera_config.json')
                if not os.path.isfile(camera_config_fn):
                    camera_config_fn = camera_intrinsics_config
            self.camera = Camera(camera_config_fn)
        except Exception as e:
            warn(f"Can't read image calibration from {e}")
            self.camera = None

        imu_data_fn = os.path.join(self.data_folder, 'imuLog.csv')
        self.imu_data = pd.read_csv(imu_data_fn)
        if 'Unnamed: 13' in self.imu_data.columns.values:
            self.imu_data.drop('Unnamed: 13', axis='columns', inplace=True)
        self.imu_data.rename({' IMU-Timestamp [nano]': 'ts_us'}, axis=1, inplace=True)
        self.imu_data.set_index('ts_us', inplace=True)
        self.imu_data.dropna(axis=0, inplace=True)
        self.system_clk_to_sec = self.imu_data['nav-time [sec]']

        # offset to add to image to get correct [ms] timestamps
        if self.image_ts is not None:
            pd.options.mode.chained_assignment = None
            # t_offset_ms = np.int64(self.imu_data.index.values[0] - self.image_ts.iloc[0, 0] * 1e6)
            # self.image_ts.loc[:, 2] = self.image_ts.loc[:, 2] + t_offset_ms

            self.image_ts.rename({2: 'ts_us', 0: 'ts_sec'}, axis=1, inplace=True)
            self.image_ts.reset_index(inplace=True)
            self.image_ts.set_index('ts_us', inplace=True)
            self.image_ts.rename({2: 'index'}, axis=1, inplace=True)
            self.image_ts.loc[:, "index"] = pd.to_numeric(self.image_ts.loc[:, "index"])

            # Add system clock by guessing from IMU measurements.
            # TODO: Ask for system clk to be added to DataFeederLog.txt
            image_system_clks = self.convert_sec_system_clk(self.image_ts.loc[:, 'ts_sec'].values)
            self.image_ts.loc[:, "ts_clk"] = image_system_clks
            pd.options.mode.chained_assignment = 'warn'

        try:
            camera_imu_config_yaml = os.path.join(self.data_folder, rig_extrinsics_config)
            camera_imu_config_json = os.path.join(self.data_folder, 'cam_imu_config.json')
            if os.path.isfile(camera_imu_config_yaml):
                with open(camera_imu_config_yaml) as f:
                    data = yaml.load(f, Loader=yaml.FullLoader)
                data_cam = data['cam0']
                cam_T_imu = np.array(data_cam["T_cam_imu"]).reshape((4, 4))
                timeshift_cam_imu = np.array(data_cam["timeshift_cam_imu"])

            elif not os.path.isfile(camera_imu_config_json):
                with open(camera_imu_config_json) as f:
                    data = json.load(f)

                cam_T_imu = np.array(data["T_cam_imu"]).reshape((4, 4))
                timeshift_cam_imu = np.array(data["timeshift_cam_imu"])

            cam_R_imu = cam_T_imu[:3, :3]
            imu_T_cam = np.eye(4, dtype=np.float64)
            imu_T_cam[:3, :3] = cam_R_imu.T
            imu_T_cam[:3, 3] = -cam_R_imu.T @ cam_T_imu[:3, 3]
            self.camera.camera_config.rig_T_cam = sp.SE3(imu_T_cam)
            self.camera.camera_config.timeshift_cam_imu = timeshift_cam_imu
        except Exception as e:
            warn(f"Can't read extrinsic calibration {e}")
            self.camera.camera_config.rig_T_cam = sp.SE3()
            self.camera.camera_config.timeshift_cam_imu = 0.

    def get_image_list(self, use_time: bool = False) -> List[Tuple[np.int64, np.int64, float, str]]:
        """
        List of image paths
        :param use_time - use time based stamp instead of system clock
        :return: List of tuples (timestamp[system clock], frame_idx, time_sec[seconds], image_path)
        """

        image_paths = []
        for idx, row in enumerate(self.image_ts.iterrows()):
            frame_index = int(row[1]["index"])
            ts_clk = np.int64(row[1]["ts_clk"])
            ts_sec = row[1]["ts_sec"]

            image_path = os.path.join(self.data_folder, 'REC', f'{frame_index:06d}.jpeg')
            if use_time:
                image_paths.append((np.int64(row[1]["ts_sec"] * 1e9), image_path))
            else:
                image_paths.append((ts_clk, frame_index, ts_sec, image_path))

        return image_paths

    def read_images(self, ts: np.int64) -> Tuple[np.ndarray]:
        """
        Get images at timestamp ts [us] - Can be more than one image for a camera rig
        :param ts: single timestamp in [us]
        :return:
            Tuple of images taken at time ts
        """

        index = self.image_ts.loc[ts, 'index']
        image_path = os.path.join(self.data_folder, 'REC', f'{index:06d}.jpeg')
        img = cv.imread(image_path, 0)
        return (img,)

    def get_image_timestamps(self) -> np.ndarray:
        return self.image_ts.index.values

    def get_imu(self, ts_start: np.int64, ts_end: np.int64) -> OrderedDict:
        """

        :param ts_start:
        :param ts_end:
        :return:
        """
        imu_data = OrderedDict()
        return imu_data

    def convert_sec_system_clk(self, sec_array: np.ndarray) -> np.ndarray:
        """
        For converting DataFeederLog time in sec to system clock
        :param sec_array:
        :return:
        """
        # Create linear regression object
        regr = linear_model.LinearRegression()

        # Train the model using the training sets
        regr.fit(self.system_clk_to_sec.values.reshape(-1, 1), self.system_clk_to_sec.index.values.reshape(-1, 1))

        # Make predictions using the testing set
        sys_clk_array = regr.predict(sec_array.reshape(-1, 1)).astype(np.int64)

        return sys_clk_array.ravel()

    def convert_system_clk_to_sec(self, sc_array: np.ndarray) -> np.ndarray:
        """
        :param sc_array:
        :return:
        """
        f = interpolate.interp1d(self.system_clk_to_sec.index.values, self.system_clk_to_sec.values)
        sec_array = f(sc_array).astype(float)
        return sec_array

    # rad/s
    def get_gyro_bias(self, N=100):
        bx = 0.0
        by = 0.0
        bz = 0.0

        for i in range(N):
            [gx, gy, gz] = self.imu_data.iloc[i, :][['Angvel x', ' Angvely y', ' Angvel z [rad/s]']]
            bx += gx
            by += gy
            bz += gz

        return np.array([bx / float(N), by / float(N), bz / float(N)])

    @property
    def imu_frequency(self):
        ts_sec = self.imu_data['nav-time [sec]'].values
        return 1. / np.median(np.diff(ts_sec))

    @property
    def camera_frequency(self):
        ts_sec = self.image_ts['ts_sec'].values
        return 1. / np.median(np.diff(ts_sec))

    @property
    def cam_to_imu(self) -> np.ndarray:
        rig_T_cam = self.camera.camera_config.rig_T_cam
        cam_T_imu = rig_T_cam.inverse.matrix
        return cam_T_imu

    @property
    def time_shift(self) -> float:
        return self.camera.camera_config.timeshift_cam_imu
