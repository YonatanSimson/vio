import os
from typing import List

import cv2 as cv
import numpy as np
import pandas as pd
import sophus as sp
import yaml
from pyquaternion import Quaternion

from rig.camera import Camera
from rig.rig import Rig
from sensors.camera_config import CameraConfigSource
from utils.common import interp_se3

"""
TODO Both sightec and Euroc MAV data readers should inherit from a common base class
"""


class EurocMavReader(object):
    data_folder: str
    image_timestamps: np.ndarray
    rig: Rig
    imu_data = pd.DataFrame

    def __init__(self, data_folder: str):
        self.data_folder = data_folder

        cam0_fn = os.path.join(self.data_folder, 'cam0/sensor.yaml')
        cam0 = Camera(cam0_fn, source=CameraConfigSource.EUROCMAV)

        cam1_fn = os.path.join(self.data_folder, 'cam1/sensor.yaml')
        cam1 = Camera(cam1_fn, source=CameraConfigSource.EUROCMAV)

        self.num_of_cameras = 2

        self.rig = Rig([cam0, cam1])
        imu_fn = os.path.join(self.data_folder, 'imu0/sensor.yaml')
        with open(imu_fn, 'r') as infile:
            data = yaml.load(infile, Loader=yaml.FullLoader)
        self.T_imu2body = np.array(data['T_BS']['data']).reshape((4, 4))

        self.camera_ts = self.get_image_timestamps()

        imu_data_fn = os.path.join(self.data_folder, 'imu0/data.csv')
        df_imu = pd.read_csv(imu_data_fn, header=[0])
        df_imu.set_index("#timestamp [ns]", inplace=True)
        df_imu.rename({'a_RS_S_x [m s^-2]': 'RawAccel x',
                       'a_RS_S_y [m s^-2]': ' RawAccel y',
                       'a_RS_S_z [m s^-2]': ' RawAccel z [m/s2]'}, axis=1, inplace=True)
        df_imu.rename({'w_RS_S_x [m s^-2]': 'Angvel x',
                       'w_RS_S_y [m s^-2]': ' Angvely y',
                       'w_RS_S_z [m s^-2]': ' Angvel z [rad/s]'}, axis=1, inplace=True)

        self.imu_data = df_imu

        self.imu_ts = df_imu.index.values

        gt_path = os.path.join(self.data_folder, 'state_groundtruth_estimate0/data.csv')
        self.df_gt = pd.read_csv(gt_path, header=[0])
        self.df_gt.set_index("#timestamp", inplace=True)

    def get_image_timestamps(self) -> np.ndarray:
        cam0_ts = np.array(
            [np.uint64(x.rsplit('.')[0]) for x in os.listdir(os.path.join(self.data_folder, 'cam0/data'))])
        cam1_ts = np.array(
            [np.uint64(x.rsplit('.')[0]) for x in os.listdir(os.path.join(self.data_folder, 'cam1/data'))])

        cam_ts = np.intersect1d(cam0_ts, cam1_ts)
        return cam_ts.astype(np.int64)

    def get_gt_6dof_timestamps(self) -> np.ndarray:
        return self.df_gt.index.values

    def get_imu_timestamps(self) -> np.ndarray:
        return self.imu_ts

    def read_image(self, ts: np.uint64, cam_idx: int) -> np.ndarray:
        """
        Read single camera with camera id
        @param ts:
        @param cam_idx:
        @return: WxH numpy array
        """
        image_path = os.path.join(self.data_folder, f"cam{cam_idx}/data/{ts}.png")
        image = cv.imread(image_path, 0)
        return image

    def read_images(self, ts: np.uint64) -> List[np.ndarray]:
        """
        Read images taken at a single timestamp
        @param ts:
        @return:
        """
        images = []
        for cam_idx in range(self.num_of_cameras):
            img = self.read_image(ts, cam_idx)
            images.append(img)

        return images

    def calc_gt_rig_pose(self, timestamp: np.int64) -> sp.SE3:
        """
        Get rig pose from GT for given timestamp
        :param timestamp:
        :return: world_T_rig SE3 pose. 'World' is the Leica or Optotrack sensor
        """

        def convert_row_to_se3(row: pd.Series) -> sp.SE3:
            position = row[[' p_RS_R_x [m]', ' p_RS_R_y [m]', ' p_RS_R_z [m]']].values
            q_wxyz = row[[' q_RS_w []', ' q_RS_x []', ' q_RS_y []', ' q_RS_z []']].values
            R = Quaternion(q_wxyz).rotation_matrix
            return sp.SE3(R, position)

        gt_timestamps = self.get_gt_6dof_timestamps()
        idx0 = np.where(timestamp - gt_timestamps.astype(np.int64) >= 0)[0][-1]
        idx1 = idx0 + 1
        t = (timestamp - np.int64(gt_timestamps[idx0])) / \
            (np.int64(gt_timestamps[idx1] - np.int64(gt_timestamps[idx0])))

        pose0 = self.df_gt.iloc[idx0]
        pose1 = self.df_gt.iloc[idx1]

        se3_0 = convert_row_to_se3(pose0)
        se3_1 = convert_row_to_se3(pose1)

        return interp_se3(se3_0, se3_1, t)
