import numpy as np


def read_slam_poses(file_path: str):
    """

    :param file_path:
    :return:
    """
    raw_mat = np.genfromtxt(file_path, dtype=np.float, delimiter=',', skip_header=1)  # names=True)
    error_msg = ("SLAM pose files must have 14 entries per row "
                 "and no trailing delimiter at the end of the rows (space)")
    if len(raw_mat) > 0 and len(raw_mat[0]) != 14:
        raise Exception(error_msg)
    # world_R_cam | world_t_cam
    poses = [np.array([[r[0], r[3], r[6], r[9]],
                       [r[1], r[4], r[7], r[10]],
                       [r[2], r[5], r[8], r[11]],
                       [0, 0, 0, 1]]) for r in raw_mat[:, 2:]]
    # Frame Id. Same as image source not the system tick
    indexes = raw_mat[:, 0].astype(np.int)
    return indexes, poses
