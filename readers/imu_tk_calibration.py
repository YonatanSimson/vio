from typing import Tuple
import numpy as np
import re


def read_single_calibration(filename: str) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """

    :param filename:
    :return:
        misalignment_matrix - 3x3 numpy matrix
        scale_matrix - 3x3 numpy matrix
        bias_vector - 3x1 numpy vector
    """

    with open(filename) as f:
        lines = f.readlines()

    lines = [line.rstrip("\n") for line in lines]

    float_number_pattern = '\S+'  # '[-+]?\d+|[-+]?\d+\.\d+'
    pattern = f"\s*({float_number_pattern})\s+({float_number_pattern})\s+({float_number_pattern})"
    misalignment_matrix = np.zeros((3, 3))
    for row in range(3):
        m = re.match(pattern, lines[row])
        for col in range(3):
            misalignment_matrix[row, col] = float(m.group(col + 1))

    scale_matrix = np.zeros((3, 3))
    for idx, row in enumerate(range(4, 7)):
        m = re.match(pattern, lines[row])
        for col in range(3):
            scale_matrix[idx, col] = float(m.group(col + 1))

    bias_vector = np.zeros((3,))
    for idx, row in enumerate(range(8, 11)):
        bias_vector[idx] = float(lines[row])

    return misalignment_matrix, scale_matrix, bias_vector


class CalibrationReader(object):
    def __init__(self, gyro_filename: str, acc_filename: str) -> None:
        """

        :param gyro_filename:
        :param acc_filename:
        """

        misalignment_matrix, scale_matrix, bias_vector = \
            read_single_calibration(gyro_filename)



