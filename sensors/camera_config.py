import json
import os
from enum import Enum
from typing import Tuple, Dict, Any

import numpy as np
import sophus as sp
import yaml


class CameraConfigSource(Enum):
    SIGHTEC = 0
    EUROCMAV = 1


class CameraIntrinsics(object):
    distortion_coefficients: np.ndarray  # 1xN numpy array with distortion parameters
    distortion_model: str
    camera_model: str
    K: np.ndarray  # 3x3 numpy array

    def __init__(self, data: Dict[str, Any],
                 source: CameraConfigSource = CameraConfigSource.SIGHTEC,
                 ) -> None:
        if source == CameraConfigSource.SIGHTEC:
            if 'camera_matrix' in data:
                fu = data['camera_matrix']['data'][0]
                fv = data['camera_matrix']['data'][4]
                cu = data['camera_matrix']['data'][2]
                cv = data['camera_matrix']['data'][5]

                k1 = data['distortion_coefficients']['data'][0]
                k2 = data['distortion_coefficients']['data'][1]
                p1 = data['distortion_coefficients']['data'][2]
                p2 = data['distortion_coefficients']['data'][3]
                k3 = data['distortion_coefficients']['data'][4]
            else:  # from json source
                down_scale = data['ScaleNav']

                fu = data['focalX'] / down_scale
                fv = data['focalY'] / down_scale
                cu = data['ppx'] / down_scale
                cv = data['ppy'] / down_scale

                k1 = data['k1']
                k2 = data['k2']
                p1 = data['r1']
                p2 = data['r2']
                k3 = data['k3']
        elif source == CameraConfigSource.EUROCMAV:
            # fu, fv, cu, cv
            fu, fv, cu, cv = np.array(data["intrinsics"])
            k1, k2, p2, p1 = np.array(data["distortion_coefficients"])
            k3 = 0.
        else:
            raise Exception(f"Invalid source: {source}")

        self.distortion_coefficients = np.array([k1, k2, p1, p2, k3])
        self.K = np.array([[fu, 0, cu], [0, fv, cv], [0, 0, 1]])
        self.distortion_model = 'RadTan'  # Classic opencv model
        self.camera_model = 'pinhole'


class CameraConfig(object):
    intrinsics: CameraIntrinsics
    rig_T_cam: sp.SE3  # Extrinsics between camera and IMU/body. Rig is the IMU
    size: Tuple[int, int]  # (x, y)
    timeshift_cam_imu: float

    def __init__(self, camera_config_fn: str, source: CameraConfigSource = CameraConfigSource.SIGHTEC) -> None:
        if source == CameraConfigSource.SIGHTEC:
            if os.path.isfile(camera_config_fn) and os.path.splitext(camera_config_fn)[1] == ".json":
                with open(camera_config_fn, 'r') as infile:
                    data = json.load(infile)
                self.intrinsics = CameraIntrinsics(data, source=source)
                self.rig_T_cam = sp.SE3()  # Currently missing from calibration
                self.size = (data['ResX'] // data['ScaleNav'], data['ResY'] // data['ScaleNav'])
            elif os.path.isfile(camera_config_fn) and os.path.splitext(camera_config_fn)[1] == ".yaml":
                with open(camera_config_fn, 'r') as infile:
                    data = yaml.load(infile, Loader=yaml.FullLoader)
                self.intrinsics = CameraIntrinsics(data, source=source)
                self.rig_T_cam = sp.SE3()  # Currently missing from calibration
                self.size = (data['image_width'], data['image_height'])
        elif source == CameraConfigSource.EUROCMAV:
            with open(camera_config_fn, 'r') as infile:
                data = yaml.load(infile, Loader=yaml.FullLoader)

            T_cam2body = np.array(data['T_BS']['data']).reshape((4, 4))
            self.size = tuple(data["resolution"])
            self.rig_T_cam = sp.SE3(T_cam2body[:3, :3], T_cam2body[:3, -1])
            self.intrinsics = CameraIntrinsics(data, source=source)
        else:
            raise Exception(f"Invalid source type: {source}")

    @property
    def K(self) -> np.ndarray:
        return self.intrinsics.K

    @property
    def distortion_coefficients(self) -> np.ndarray:
        return self.intrinsics.distortion_coefficients

    @property
    def cam_to_rig(self) -> sp.SE3:
        return self.rig_T_cam

    @property
    def time_shift(self) -> float:
        return self.timeshift_cam_imu
