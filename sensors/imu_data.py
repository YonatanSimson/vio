import numpy as np

from .imu_config import IMUIntrinsics


class IMUData(object):
    angular_speed: np.ndarray  # [Rad/Sec]
    linear_acceleration: np.ndarray  # [m/s^2]

    def __init__(self, data: np.ndarray):
        self.angular_speed = data[:3]
        self.linear_acceleration = data[3:]

        # TDB apply intrinsics when possible

    @property
    def number_of_measurements(self):
        return self.linear_acceleration.shape[0]
