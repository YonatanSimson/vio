import abc
from typing import Optional

import numpy as np

from readers.imu_tk_calibration import read_single_calibration


class IMUIntrinsicsBase(metaclass=abc.ABCMeta):
    scale_matrix: Optional[np.ndarray]
    misaligned_matrix: Optional[np.ndarray]
    bias_vector: Optional[np.ndarray]

    def __init__(self, misaligned_matrix: np.ndarray, scale_matrix: np.ndarray, bias_vector: np.ndarray,
                 *args, **kwargs) -> None:
        """
        X' = misaligned_matrix * scale_matrix * (X - bias_vector)

        :param misaligned_matrix: 3x3 numpy array
        :param scale_matrix: 3x3 numpy array
        :param bias_vector: 3x1 numpy vector
        """
        self.scale_matrix = scale_matrix
        self.misaligned_matrix = misaligned_matrix
        self.bias_vector = bias_vector


class GyroIntrinsics(IMUIntrinsicsBase):
    def __init__(self, misaligned_matrix: np.ndarray, scale_matrix: np.ndarray, bias_vector: np.ndarray,
                 *args, **kwargs) -> None:
        super().__init__(misaligned_matrix=misaligned_matrix,
                         scale_matrix=scale_matrix,
                         bias_vector=bias_vector)


class AccIntrinsics(IMUIntrinsicsBase):
    def __init__(self, misaligned_matrix: np.ndarray, scale_matrix: np.ndarray, bias_vector: np.ndarray,
                 *args, **kwargs) -> None:
        super().__init__(misaligned_matrix=misaligned_matrix,
                         scale_matrix=scale_matrix,
                         bias_vector=bias_vector)


class IMUIntrinsics(object):
    gyro_intrinsics: IMUIntrinsicsBase
    acc_intrinsics: IMUIntrinsicsBase

    def __init__(self, gyro_calib_fn: str, acc_calib_fn: str) -> None:
        gyro_calib_tuple = read_single_calibration(gyro_calib_fn)
        acc_calib_tuple = read_single_calibration(acc_calib_fn)

        self.gyro_intrinsics = GyroIntrinsics(*gyro_calib_tuple)
        self.acc_intrinsics = AccIntrinsics(*acc_calib_tuple)
