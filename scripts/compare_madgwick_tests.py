import os.path

import numpy as np
import pandas as pd
from collections import namedtuple


if __name__ == "__main__":
    new_output_folder = "/home/yonatan/data/madgwick_filter/metrics_true_north_summary"
    base_output_folder = "/home/yonatan/data/madgwick_filter/metrics_summary"

    summary_stats_fn = "summary_stats.csv"

    base_summary_df = pd.read_csv(os.path.join(base_output_folder, summary_stats_fn), index_col=[0, 1])
    new_summary_df = pd.read_csv(os.path.join(new_output_folder, summary_stats_fn), index_col=[0, 1])

    new_summary_df.compare(base_summary_df)

    comparision_df = new_summary_df.compare(base_summary_df)
    comparision_df.rename(columns={"self": "new", "other": "reference"}, inplace=True)
    comparision_html = os.path.join(new_output_folder, "comparision_stats.html")
    comparision_df.to_html(comparision_html)

