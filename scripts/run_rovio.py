import os.path
from typing import List
import argparse
import subprocess
import logging

from utils.file_io import read_list


if __name__ == "__main__":
    logger = logging.getLogger("logging_tryout2")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='Arguments for running ROVIO algorithm')
    parser.add_argument(
        "--input_folder",
        help="Input folder containing the tests listsed in input_test_list",
        type=str)

    parser.add_argument(
        "--vio_results_folder",
        help="Folder for output VIO results",
        type=str)

    parser.add_argument(
        "--input_test_list",
        help="Test list",
        default=r"./euroc_mav_test_list.txt",
        type=str)

    parser.add_argument(
        "--vio_executable",
        help="Binary file for running vio",
        type=str,
    )

    args = parser.parse_args()

    test_list = read_list(args.input_test_list)
    test_base_path = args.input_folder
    results_base_folder = args.vio_results_folder
    binary_exec = args.vio_executable
    # Run tests
    for test_name in test_list:
        logger.info(f"Running: {test_name}")
        test_input_folder = os.path.join(test_base_path, test_name, "mav0")
        test_results_folder = os.path.join(results_base_folder, test_name)
        if not os.path.isdir(test_results_folder):
            os.makedirs(test_results_folder)

        cwd = os.getcwd()
        binary_path = os.path.dirname(os.path.dirname(binary_exec))
        os.chdir(binary_path)
        cmd_list = [binary_exec,
                    "-i", test_input_folder,
                    "-s", "0",
                    "-e", "-1",
                    "-c", "/home/yonatan/develop/rovio_noros/cfg/euroc_cam0.yaml",
                    "-f", "/home/yonatan/develop/rovio_noros/cfg/rovio.info",
                    "-o", test_results_folder]

        logger.info(f"Executing: {' '.join(cmd_list)}")
        try:
            result = subprocess.run(
                cmd_list,
            )
        except Exception:
            # logger.info(f"stdout: {result.stdout}")
            logger.warning(f"stderr: {result.stderr}")

        os.chdir(cwd)
