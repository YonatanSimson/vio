import os.path

import numpy as np
import pandas as pd
from collections import namedtuple

from orientation_estimation.develop_madgwik_filter import run_and_calculate_metrics

if __name__ == "__main__":
    test_folders_list = [
        "/home/yonatan/data/madgwick_filter/run_#48",
        "/home/yonatan/data/madgwick_filter/run_#49",
        "/home/yonatan/data/madgwick_filter/run_#50",
        "/home/yonatan/data/madgwick_filter/run_#51",
        # "/home/yonatan/data/madgwick_filter/run_#56"
    ]

    sub_folder_metrics_name = "metrics_v1"
    output_folder = "/home/yonatan/data/madgwick_filter/metrics_true_north_summary"
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    df_list = []
    df_dict = {}
    key_list = []
    for test_folder in test_folders_list:
        InputArgs = namedtuple("InputArgs", ["input_folder", "metric_subfolder", "output_folder"])
        args = InputArgs(test_folder, sub_folder_metrics_name, test_folder)
        df_error = run_and_calculate_metrics(args)
        df_list.append(df_error)
        key = os.path.basename(test_folder)
        key_list.append(key)
        df_dict[key] = df_error

    df_total = pd.concat(df_dict)
    df_max = df_total.groupby(level=1).max()
    df_median = df_total.groupby(level=1).median()
    df_mean = df_total.groupby(level=1).mean()

    df_summary = pd.concat(dict(zip(["mean", "median", "max"], [df_mean, df_median, df_max])))

    total_csv = os.path.join(output_folder, "total_stats.csv")
    total_html = os.path.join(output_folder, "total_stats.html")
    df_total.to_csv(total_csv)
    with open(total_html, 'w') as fo:
        df_total.to_html(fo)

    summary_csv = os.path.join(output_folder, "summary_stats.csv")
    summary_html = os.path.join(output_folder, "summary_stats.html")
    df_summary.to_csv(summary_csv)
    with open(summary_html, 'w') as fo:
        df_summary.to_html(fo)

    print("Done")
