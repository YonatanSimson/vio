import argparse
import logging
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt

import os

from metrics.config import Config as ConfigMetrics
from metrics.metrics import Metrics as MetricsCalculator

# temporarily override some package settings
from evo.tools.settings import SETTINGS
from evo.tools import plot
from metrics.plot_errors import error_plotter

from utils.file_io import read_list


if __name__ == "__main__":
    logger = logging.getLogger("logging_tryout2")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='Arguments for running ROVIO metrics')
    parser.add_argument(
        "--vio_results_folder_ref",
        help="Folder for output VIO reference results",
        type=str)

    parser.add_argument(
        "--vio_results_folder_new",
        help="Folder for output VIO new results",
        type=str)

    parser.add_argument(
        "--ground_truth_folder",
        help="Folder for output VIO results",
        type=str)

    args = parser.parse_args()

    #############
    # config ####
    #############
    config = ConfigMetrics()
    SETTINGS.plot_usetex = False
    SETTINGS.plot_pose_correspondences = True

    results_ref_folder = args.vio_results_folder_ref
    results_new_folder = args.vio_results_folder_new

    gt_base_folder = args.ground_truth_folder

    # Run comparision
    test_name = os.path.basename(results_ref_folder)
    logger.info(f"Running metrics: {test_name}")

    gt_path = os.path.join(gt_base_folder, test_name, "mav0/state_groundtruth_estimate0/data.csv")

    vio_ref_results_path = os.path.join(results_ref_folder, "WB.csv")
    vio_new_results_path = os.path.join(results_new_folder, "WB.csv")

    # metrics_output_folder = os.path.join(metrics_base_folder, test_name)
    #
    # statistics_folder = os.path.join(metrics_output_folder, "statistics")
    ref_plots_folder = os.path.join(results_ref_folder, "plots_debug")
    new_plots_folder = os.path.join(results_new_folder, "plots_debug")

    # if not os.path.isdir(statistics_folder):
    #     os.makedirs(statistics_folder)
    if not os.path.isdir(ref_plots_folder):
        os.makedirs(ref_plots_folder)
    if not os.path.isdir(new_plots_folder):
        os.makedirs(new_plots_folder)

    try:
        ######################
        # Calculate metrics ##
        ######################
        ref_error_metrics = MetricsCalculator(gt_path, vio_ref_results_path, config)
        new_error_metrics = MetricsCalculator(gt_path, vio_new_results_path, config)

        #####################
        # Build plots #######
        #####################
        error_plotter(ref_error_metrics, ref_plots_folder)
        error_plotter(new_error_metrics, new_plots_folder)

        # 3D plots
        fig = plt.figure()
        traj_by_label = {
            "ref estimate (aligned)": ref_error_metrics.traj_est_aligned,
            "new estimate (aligned)": new_error_metrics.traj_est_aligned,
            "reference": ref_error_metrics.traj_ref
        }
        plot.trajectories(fig, traj_by_label, plot.PlotMode.xyz)
        ax = plt.gca()
        ax.set_title("Trajectories")

        fig, axes = plt.subplots(3, 1, sharex=True)
        axes[0].plot(ref_error_metrics.traj_est_aligned.timestamps,
                     ref_error_metrics.traj_est_aligned.positions_xyz[:, 0],
                     label="reference")
        axes[0].plot(new_error_metrics.traj_est_aligned.timestamps,
                     new_error_metrics.traj_est_aligned.positions_xyz[:, 0],
                     label="new")
        axes[0].legend()
        axes[0].set_title('x')
        axes[0].set_xlabel('t[us]')

        axes[1].plot(ref_error_metrics.traj_est_aligned.timestamps,
                     ref_error_metrics.traj_est_aligned.positions_xyz[:, 1],
                     label="reference")
        axes[1].plot(new_error_metrics.traj_est_aligned.timestamps,
                     new_error_metrics.traj_est_aligned.positions_xyz[:, 1],
                     label="new")
        axes[1].legend()
        axes[1].set_title('y')
        axes[1].set_xlabel('t[us]')

        axes[2].plot(ref_error_metrics.traj_est_aligned.timestamps,
                     ref_error_metrics.traj_est_aligned.positions_xyz[:, 2],
                     label="reference")
        axes[2].plot(new_error_metrics.traj_est_aligned.timestamps,
                     new_error_metrics.traj_est_aligned.positions_xyz[:, 2],
                     label="new")
        axes[2].legend()
        axes[2].set_title('z')
        axes[2].set_xlabel('t[us]')
        plt.tight_layout()
        # plt.savefig(os.path.join(output_folder, "trajectories.png"))

    except Exception as e:
        logger.warning(f"Failed to extract metrics on test: {test_name}!!")
    plt.close("all")
