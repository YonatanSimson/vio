import argparse
import logging
import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt
import pandas as pd
import os

from metrics.config import Config as ConfigMetrics
from metrics.metrics import Metrics as MetricsCalculator

# temporarily override some package settings
from evo.tools.settings import SETTINGS
from evo.tools import plot
from metrics.plot_errors import error_plotter

from utils.file_io import read_list


if __name__ == "__main__":
    logger = logging.getLogger("logging_tryout2")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='Arguments for running ROVIO metrics')
    parser.add_argument(
        "--benchmark_results_folder_ref",
        help="Folder for reference benchmark",
        type=str)

    parser.add_argument(
        "--benchmark_results_folder_new",
        help="Folder for new benchmark",
        type=str)

    parser.add_argument(
        "--ground_truth_folder",
        help="Folder for output VIO results",
        type=str)

    args = parser.parse_args()

    #############
    # config ####
    #############
    config = ConfigMetrics()
    SETTINGS.plot_usetex = False
    SETTINGS.plot_pose_correspondences = True

    benchmark_results_folder_ref = args.benchmark_results_folder_ref
    benchmark_results_folder_new = args.benchmark_results_folder_new

    gt_base_folder = args.ground_truth_folder

    ref_test_list = os.listdir(benchmark_results_folder_ref)
    ref_test_list = [x for x in ref_test_list if os.path.isfile(os.path.join(benchmark_results_folder_ref, x, "statistics/vio_errors.csv"))]
    new_test_list = os.listdir(benchmark_results_folder_new)
    new_test_list = [x for x in new_test_list if os.path.isfile(os.path.join(benchmark_results_folder_new, x, "statistics/vio_errors.csv"))]
    # tests added
    new_tests = list(set(new_test_list) - set(ref_test_list))
    logger.info(f"Added {len(new_tests)} new tests: {', '.join(new_tests)}")

    # tests lost
    lost_tests = list(set(ref_test_list) - set(new_test_list))
    if len(lost_tests) > 0:
        logger.warning(f"Lost {len(lost_tests)} tests: {', '.join(lost_tests)}")

    common_test = sorted(list(set(new_test_list)& set(ref_test_list)))

    # Run comparison
    df_average_ref = pd.DataFrame()
    df_average_new = pd.DataFrame()
    ref_dict = {}
    new_dict = {}
    for test in common_test:
        df_ref = pd.read_csv(os.path.join(benchmark_results_folder_ref, test, "statistics/vio_errors.csv"), index_col=[0])
        df_new = pd.read_csv(os.path.join(benchmark_results_folder_new, test, "statistics/vio_errors.csv"), index_col=[0])
        df_average_ref[test] = df_ref.unstack(0)
        df_average_new[test] = df_new.unstack(0)

    df_total = pd.DataFrame()
    df_total["ref"] = df_average_ref.mean(axis=1)
    df_total["new"] = df_average_new.mean(axis=1)

    df_total.to_csv(os.path.join(benchmark_results_folder_new, "benchmark_compare.csv"))
    df_total.to_html(os.path.join(benchmark_results_folder_new, "benchmark_compare.html"))

    print("")

