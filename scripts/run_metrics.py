import argparse
import logging
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import os

from metrics.config import Config as ConfigMetrics
from metrics.metrics import Metrics as MetricsCalculator


# temporarily override some package settings
from evo.tools.settings import SETTINGS
from metrics.plot_errors import error_plotter

from utils.file_io import read_list


if __name__ == "__main__":
    logger = logging.getLogger("logging_tryout2")
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    formatter = logging.Formatter("%(asctime)s;%(levelname)s;%(message)s",
                                  "%Y-%m-%d %H:%M:%S")
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)

    parser = argparse.ArgumentParser(description='Arguments for running ROVIO metrics')
    parser.add_argument(
        "--vio_results_folder",
        help="Folder for output VIO results",
        type=str)

    parser.add_argument(
        "--input_test_list",
        help="Test list",
        default=r"./euroc_mav_test_list.txt",
        type=str)
    parser.add_argument(
        "--ground_truth_folder",
        help="Folder for output VIO results",
        type=str)
    parser.add_argument(
        "--metrics_folder",
        help="Folder for output VIO results",
        type=str)

    args = parser.parse_args()

    #############
    # config ####
    #############
    config = ConfigMetrics()
    SETTINGS.plot_usetex = False
    SETTINGS.plot_pose_correspondences = True

    test_list = read_list(args.input_test_list)

    results_base_folder = args.vio_results_folder
    metrics_base_folder = args.metrics_folder
    reference_base_folder = args.ground_truth_folder

    # Run tests
    for test_name in test_list:
        logger.info(f"Running metrics: {test_name}")

        reference_folder = os.path.join(reference_base_folder, test_name, "mav0/state_groundtruth_estimate0/data.csv")
        vio_results_folder = os.path.join(results_base_folder, test_name, "WB.csv")
        metrics_output_folder = os.path.join(metrics_base_folder, test_name)

        statistics_folder = os.path.join(metrics_output_folder, "statistics")
        plots_folder = os.path.join(metrics_output_folder, "plots")

        if not os.path.isdir(statistics_folder):
            os.makedirs(statistics_folder)
        if not os.path.isdir(plots_folder):
            os.makedirs(plots_folder)

        try:
            ######################
            # Calculate metrics ##
            ######################
            error_metrics = MetricsCalculator(reference_folder, vio_results_folder, config)
            error_metrics.output_statistics_to_file(statistics_folder)

            #####################
            # Build plots #######
            #####################
            error_plotter(error_metrics, plots_folder)
        except Exception as e:
            logger.warning(f"Failed to extract metrics on test: {test_name}!!")
        plt.close("all")
